<?php

	// carrega configuracoes
    include("core/configurations.php");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta http-equiv="Cache-Control" content="no-cache, no-store">
		<meta http-equiv="Pragma" content="no-cache, no-store">
		<meta http-equiv="expires" content="Mon, 06 Jan 1990 00:00:01 GMT">
		<meta name="author" content="Heron Santos">

		<title><?= SYSTEM_TITLE ?></title>
	</head>

	<frameset>
		<frame src="<?= SYSTEM_PATH ?>/framework/" scrolling="auto" id="framework" name="framework">
		<noframes>
			<body>
				<p>Seu navegador não suporta navegação em frames...</p>
				<p>Atualize-o!</p>
			</body>
		</noframes>
	</frameset>
</html>
