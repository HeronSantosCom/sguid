﻿# SQL Manager 2007 for MySQL 4.3.4.1
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : sguid


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `sguid`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `sguid`;

#
# Structure for the `cbx_federacao` table : 
#

CREATE TABLE `cbx_federacao` (
  `idcbx_federacao` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `federacao` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_federacao`)
)ENGINE=InnoDB
AUTO_INCREMENT=227 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Federação Nacional / País; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_estado` table : 
#

CREATE TABLE `cbx_estado` (
  `idcbx_estado` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idcbx_federacao` INTEGER(11) NOT NULL DEFAULT '0' COMMENT 'Federação',
  `estado` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  `uf` CHAR(2) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Sigla da Unidade Federativa',
  PRIMARY KEY (`idcbx_estado`),
  KEY `fk_federacao` (`idcbx_federacao`),
  CONSTRAINT `estado_federacao` FOREIGN KEY (`idcbx_federacao`) REFERENCES `cbx_federacao` (`idcbx_federacao`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=28 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Estado / Província; InnoDB free: 9216 kB; (`idcbx_fe; InnoDB free: 81920';

#
# Structure for the `cbx_cidade` table : 
#

CREATE TABLE `cbx_cidade` (
  `idcbx_cidade` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idcbx_estado` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Estado',
  `cidade` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_cidade`),
  KEY `fk_estado` (`idcbx_estado`),
  CONSTRAINT `estado-cidade` FOREIGN KEY (`idcbx_estado`) REFERENCES `cbx_estado` (`idcbx_estado`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=28 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Cidade / Região; InnoDB free: 9216 kB; (`idcbx_estad; InnoDB free: 81920';

#
# Structure for the `cbx_conteudo` table : 
#

CREATE TABLE `cbx_conteudo` (
  `idcbx_conteudo` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `conteudo` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  `cargahoraria` INTEGER(11) NOT NULL DEFAULT '0' COMMENT 'Caga Horária (por hora)',
  `descricao` TEXT COLLATE utf8_general_ci COMMENT 'Descrição',
  PRIMARY KEY (`idcbx_conteudo`)
)ENGINE=InnoDB
AUTO_INCREMENT=24 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Conteudo; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_deficiencia` table : 
#

CREATE TABLE `cbx_deficiencia` (
  `idcbx_deficiencia` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deficiencia` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Deficiência',
  PRIMARY KEY (`idcbx_deficiencia`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Deficiencias; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_estadocivil` table : 
#

CREATE TABLE `cbx_estadocivil` (
  `idcbx_estadocivil` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `estadocivil` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_estadocivil`)
)ENGINE=InnoDB
AUTO_INCREMENT=6 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Estado Civil; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_etnia` table : 
#

CREATE TABLE `cbx_etnia` (
  `idcbx_etnia` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `etnia` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_etnia`)
)ENGINE=InnoDB
AUTO_INCREMENT=6 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Raça; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_grauescolar` table : 
#

CREATE TABLE `cbx_grauescolar` (
  `idcbx_grauescolar` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `grauescolar` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_grauescolar`)
)ENGINE=InnoDB
AUTO_INCREMENT=13 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Grau Escolar; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_levelaccess` table : 
#

CREATE TABLE `cbx_levelaccess` (
  `idcbx_levelaccess` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `levelaccess` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  `description` TINYTEXT COMMENT 'Descrição',
  PRIMARY KEY (`idcbx_levelaccess`)
)ENGINE=InnoDB
AUTO_INCREMENT=6 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Nível de Acesso; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_modulo` table : 
#

CREATE TABLE `cbx_modulo` (
  `idcbx_modulo` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `modulo` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  `descricao` TEXT COLLATE utf8_general_ci COMMENT 'Descrição',
  PRIMARY KEY (`idcbx_modulo`)
)ENGINE=InnoDB
AUTO_INCREMENT=12 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Módulo; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_modulo_has_cbx_conteudo` table : 
#

CREATE TABLE `cbx_modulo_has_cbx_conteudo` (
  `idcbx_modulo_has_cbx_conteudo` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `idcbx_modulo` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Módulo',
  `idcbx_conteudo` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Conteúdo',
  PRIMARY KEY (`idcbx_modulo_has_cbx_conteudo`),
  KEY `fk_modulo` (`idcbx_modulo`),
  KEY `fk_conteudo` (`idcbx_conteudo`),
  CONSTRAINT `conteudo-has-modulo` FOREIGN KEY (`idcbx_conteudo`) REFERENCES `cbx_conteudo` (`idcbx_conteudo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modulo-has-conteudo` FOREIGN KEY (`idcbx_modulo`) REFERENCES `cbx_modulo` (`idcbx_modulo`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=25 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Relacionamento: Módulo > Conteúdo; InnoDB free: 9216 kB; (`i; InnoDB free: 81920';

#
# Structure for the `type_curso` table : 
#

CREATE TABLE `type_curso` (
  `idtype_curso` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `curso` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Legenda',
  `descricao` TEXT COLLATE utf8_general_ci COMMENT 'Descrição',
  PRIMARY KEY (`idtype_curso`)
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Curso; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_modulo_has_type_curso` table : 
#

CREATE TABLE `cbx_modulo_has_type_curso` (
  `idcbx_modulo_has_type_curso` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `idcbx_modulo` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Módulo',
  `idtype_curso` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Curso',
  PRIMARY KEY (`idcbx_modulo_has_type_curso`),
  KEY `fk_modulo` (`idcbx_modulo`),
  KEY `fk_curso` (`idtype_curso`),
  CONSTRAINT `curso-has-modulo` FOREIGN KEY (`idtype_curso`) REFERENCES `type_curso` (`idtype_curso`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modulo-has-curso` FOREIGN KEY (`idcbx_modulo`) REFERENCES `cbx_modulo` (`idcbx_modulo`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=13 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Relacionamento: Módulo > Curso; InnoDB free: 9216 kB; (`idty; InnoDB free: 81920';

#
# Structure for the `cbx_nacionalidade` table : 
#

CREATE TABLE `cbx_nacionalidade` (
  `idcbx_nacionalidade` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nacionalidade` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_nacionalidade`)
)ENGINE=InnoDB
AUTO_INCREMENT=4 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Nacionalidade; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_serieescolar` table : 
#

CREATE TABLE `cbx_serieescolar` (
  `idcbx_serieescolar` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `serieescolar` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_serieescolar`)
)ENGINE=InnoDB
AUTO_INCREMENT=26 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Série Escolar; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_sexo` table : 
#

CREATE TABLE `cbx_sexo` (
  `idcbx_sexo` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sexo` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_sexo`)
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Sexo; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_situacaoprofissional` table : 
#

CREATE TABLE `cbx_situacaoprofissional` (
  `idcbx_situacaoprofissional` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `situacao` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_situacaoprofissional`)
)ENGINE=InnoDB
AUTO_INCREMENT=11 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Situação Profissional; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_status` table : 
#

CREATE TABLE `cbx_status` (
  `idcbx_status` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Legenda',
  PRIMARY KEY (`idcbx_status`)
)ENGINE=InnoDB
AUTO_INCREMENT=4 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Estado Atual do Registro; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_tipoescola` table : 
#

CREATE TABLE `cbx_tipoescola` (
  `idcbx_tipoescola` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipoescola` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome do Tipo Escola',
  PRIMARY KEY (`idcbx_tipoescola`)
)ENGINE=InnoDB
AUTO_INCREMENT=7 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Tipo Escola; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `cbx_turnoescolar` table : 
#

CREATE TABLE `cbx_turnoescolar` (
  `idcbx_turnoescolar` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `turnoescolar` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome do Turno Escolar',
  PRIMARY KEY (`idcbx_turnoescolar`)
)ENGINE=InnoDB
AUTO_INCREMENT=5 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Select: Turno Escolar; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `type_endereco` table : 
#

CREATE TABLE `type_endereco` (
  `idtype_endereco` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idcbx_cidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Cidade',
  `logradouro` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome do Logradouro',
  `bairro` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome do Bairro',
  `cep` CHAR(9) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'CEP',
  PRIMARY KEY (`idtype_endereco`),
  KEY `fk_cidade` (`idcbx_cidade`),
  CONSTRAINT `cidade-endereco` FOREIGN KEY (`idcbx_cidade`) REFERENCES `cbx_cidade` (`idcbx_cidade`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=8 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Endereço; InnoDB free: 9216 kB; (`idcbx_cidade`) R; InnoDB free: 81920';

#
# Structure for the `instituicao` table : 
#

CREATE TABLE `instituicao` (
  `idinstituicao` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomefantasia` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome Fantasia',
  `razaosocial` VARCHAR(155) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Razão Social',
  `CNPJ` CHAR(18) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'CNPJ',
  `idtype_endereco` INTEGER(11) UNSIGNED DEFAULT '0' COMMENT 'Endereço',
  PRIMARY KEY (`idinstituicao`),
  KEY `fk_endereco` (`idtype_endereco`),
  CONSTRAINT `endereco-insituicao` FOREIGN KEY (`idtype_endereco`) REFERENCES `type_endereco` (`idtype_endereco`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Insituição; InnoDB free: 9216 kB; (`idtype_endereco`) REFER ; InnoDB free: 81920';

#
# Structure for the `unidade` table : 
#

CREATE TABLE `unidade` (
  `idunidade` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idinstituicao` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Instituição',
  `nome` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome da Unidade',
  `idtype_endereco` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Endereço',
  PRIMARY KEY (`idunidade`),
  KEY `fk_instituicao` (`idinstituicao`),
  KEY `fk_endereco` (`idtype_endereco`),
  CONSTRAINT `endereco-unidade` FOREIGN KEY (`idtype_endereco`) REFERENCES `type_endereco` (`idtype_endereco`) ON UPDATE CASCADE,
  CONSTRAINT `instituicao-unidade` FOREIGN KEY (`idinstituicao`) REFERENCES `instituicao` (`idinstituicao`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Unidade de Inclusão Digital - Max: 999; InnoDB free: 9216 kB; InnoDB free: 81920';

#
# Structure for the `turma` table : 
#

CREATE TABLE `turma` (
  `idturma` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo` CHAR(12) COLLATE utf8_general_ci DEFAULT NULL,
  `idunidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Unidade',
  `idtype_curso` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Curso',
  `inicio` DATE NOT NULL DEFAULT '0000-00-00' COMMENT 'Data Inicial da Turma',
  `termino` DATE DEFAULT NULL COMMENT 'Data Estimada para Término da Turma',
  PRIMARY KEY (`idturma`),
  KEY `fk_curso` (`idtype_curso`),
  KEY `fk_unidade` (`idunidade`),
  CONSTRAINT `turma-curso` FOREIGN KEY (`idtype_curso`) REFERENCES `type_curso` (`idtype_curso`) ON UPDATE CASCADE,
  CONSTRAINT `turma-unidade` FOREIGN KEY (`idunidade`) REFERENCES `unidade` (`idunidade`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Turma; InnoDB free: 9216 kB; (`idtype_curso`) REFER `sguid/t; InnoDB free: 81920';

#
# Structure for the `type_emprego` table : 
#

CREATE TABLE `type_emprego` (
  `idtype_emprego` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idcbx_situacaoprofissional` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Situação Profissional',
  `instituicao` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome da Instituição',
  `admissao` DATE DEFAULT NULL COMMENT 'Data de Admissão',
  `ocupacao` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Ocupação',
  `remuneracao` FLOAT(9,2) NOT NULL COMMENT 'Remuneração Salarial',
  PRIMARY KEY (`idtype_emprego`),
  KEY `fk_situacaoprofissional` (`idcbx_situacaoprofissional`),
  CONSTRAINT `situacaoprofissional-emprego` FOREIGN KEY (`idcbx_situacaoprofissional`) REFERENCES `cbx_situacaoprofissional` (`idcbx_situacaoprofissional`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Situação Profissional; InnoDB free: 9216 kB; (`idc; InnoDB free: 81920';

#
# Structure for the `type_escolaridade` table : 
#

CREATE TABLE `type_escolaridade` (
  `idtype_escolaridade` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idcbx_grauescolar` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Grau Escolar',
  `idcbx_serieescolar` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Série Escolar',
  `idcbx_tipoescola` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Tipo da Escola',
  `instituicao` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome do Instituição',
  `curso` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome do Curso',
  `idcbx_turnoescolar` INTEGER(11) UNSIGNED DEFAULT '0' COMMENT 'Turno Escolar',
  PRIMARY KEY (`idtype_escolaridade`),
  KEY `fk_tipoescola` (`idcbx_tipoescola`),
  KEY `fk_grauescolar` (`idcbx_grauescolar`),
  KEY `fk_serieescolar` (`idcbx_serieescolar`),
  KEY `fk_turnoescolar` (`idcbx_turnoescolar`),
  CONSTRAINT `grauescolar-escolaridade` FOREIGN KEY (`idcbx_grauescolar`) REFERENCES `cbx_grauescolar` (`idcbx_grauescolar`) ON UPDATE CASCADE,
  CONSTRAINT `serieescolar-escolaridade` FOREIGN KEY (`idcbx_serieescolar`) REFERENCES `cbx_serieescolar` (`idcbx_serieescolar`) ON UPDATE CASCADE,
  CONSTRAINT `tipoescola-escolariade` FOREIGN KEY (`idcbx_tipoescola`) REFERENCES `cbx_tipoescola` (`idcbx_tipoescola`) ON UPDATE CASCADE,
  CONSTRAINT `turnoescolar-escolariade` FOREIGN KEY (`idcbx_turnoescolar`) REFERENCES `cbx_turnoescolar` (`idcbx_turnoescolar`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Escolaridade; InnoDB free: 9216 kB; (`idcbx_graues; InnoDB free: 81920';

#
# Structure for the `type_identidade` table : 
#

CREATE TABLE `type_identidade` (
  `idtype_identidade` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Número do RG',
  `orgaoemissor` CHAR(10) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Orgão Emissor',
  `dataemissao` DATE DEFAULT NULL COMMENT 'Data de Emissão',
  PRIMARY KEY (`idtype_identidade`)
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Identidade / Registro Geral; InnoDB free: 9216 kB; InnoDB free: 81920 ';

#
# Structure for the `type_nacionalidade` table : 
#

CREATE TABLE `type_nacionalidade` (
  `idtype_nacionalidade` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idcbx_nacionalidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Nacionalidade',
  `idcbx_federacao` INTEGER(11) DEFAULT '0' COMMENT 'Federação',
  PRIMARY KEY (`idtype_nacionalidade`),
  KEY `fk_nacionalidade` (`idcbx_nacionalidade`),
  KEY `fk_federacao` (`idcbx_federacao`),
  CONSTRAINT `federacao-nacionalidade` FOREIGN KEY (`idcbx_federacao`) REFERENCES `cbx_federacao` (`idcbx_federacao`) ON UPDATE CASCADE,
  CONSTRAINT `nacionalidade-nacionalidade` FOREIGN KEY (`idcbx_nacionalidade`) REFERENCES `cbx_nacionalidade` (`idcbx_nacionalidade`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=3 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Nacionalidade; InnoDB free: 9216 kB; (`idcbx_feder; InnoDB free: 81920';

#
# Structure for the `usuario` table : 
#

CREATE TABLE `usuario` (
  `idusuario` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idunidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Unidade de Cadastro',
  `inscricao` DATE NOT NULL COMMENT 'Data de Inscrição',
  `nome` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome',
  `nascimento` DATE NOT NULL COMMENT 'Data de Nascimento',
  `idcbx_sexo` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Sexo',
  `idcbx_estadocivil` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Estado Civil',
  `idcbx_etnia` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Etnia',
  `idtype_nacionalidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Nacionalidade',
  `idtype_endereco` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Endereço',
  `idtype_identidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Identidade',
  `cpf` CHAR(15) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'CPF',
  `idtype_escolaridade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Escolaridade',
  `idtype_emprego` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Situação Profissional',
  `idcbx_status` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Estado',
  PRIMARY KEY (`idusuario`),
  KEY `fk_nacionalidade` (`idtype_nacionalidade`),
  KEY `fk_etnia` (`idcbx_etnia`),
  KEY `fk_estadocivil` (`idcbx_estadocivil`),
  KEY `fk_sexo` (`idcbx_sexo`),
  KEY `fk_identidade` (`idtype_identidade`),
  KEY `fk_escolaridade` (`idtype_escolaridade`),
  KEY `fk_status` (`idcbx_status`),
  KEY `fk_unidade` (`idunidade`),
  KEY `fk_emprego` (`idtype_emprego`),
  KEY `fk_endereco` (`idtype_endereco`),
  CONSTRAINT `emprego-usuario` FOREIGN KEY (`idtype_emprego`) REFERENCES `type_emprego` (`idtype_emprego`) ON UPDATE CASCADE,
  CONSTRAINT `endereco-usuario` FOREIGN KEY (`idtype_endereco`) REFERENCES `type_endereco` (`idtype_endereco`) ON UPDATE CASCADE,
  CONSTRAINT `escolaridade-usuario` FOREIGN KEY (`idtype_escolaridade`) REFERENCES `type_escolaridade` (`idtype_escolaridade`) ON UPDATE CASCADE,
  CONSTRAINT `estadocivil-usuario` FOREIGN KEY (`idcbx_estadocivil`) REFERENCES `cbx_estadocivil` (`idcbx_estadocivil`) ON UPDATE CASCADE,
  CONSTRAINT `etnia-usuario` FOREIGN KEY (`idcbx_etnia`) REFERENCES `cbx_etnia` (`idcbx_etnia`) ON UPDATE CASCADE,
  CONSTRAINT `identidade-usuario` FOREIGN KEY (`idtype_identidade`) REFERENCES `type_identidade` (`idtype_identidade`) ON UPDATE CASCADE,
  CONSTRAINT `nacionalidade-usuario` FOREIGN KEY (`idtype_nacionalidade`) REFERENCES `type_nacionalidade` (`idtype_nacionalidade`) ON UPDATE CASCADE,
  CONSTRAINT `sexo-usuario` FOREIGN KEY (`idcbx_sexo`) REFERENCES `cbx_sexo` (`idcbx_sexo`) ON UPDATE CASCADE,
  CONSTRAINT `status-usuario` FOREIGN KEY (`idcbx_status`) REFERENCES `cbx_status` (`idcbx_status`) ON UPDATE CASCADE,
  CONSTRAINT `unidade-usuario` FOREIGN KEY (`idunidade`) REFERENCES `unidade` (`idunidade`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Usuário / Visitante do Telecentro; InnoDB free: 9216 kB; (`i; InnoDB free: 81920';

#
# Structure for the `diariodeclasse` table : 
#

CREATE TABLE `diariodeclasse` (
  `idturma` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Turma',
  `idusuario` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Usuário',
  `presenca` DATE DEFAULT NULL COMMENT 'Data de Presença',
  KEY `fk_turma` (`idturma`),
  KEY `fk_usuario` (`idusuario`),
  CONSTRAINT `diariodeclasse-turma` FOREIGN KEY (`idturma`) REFERENCES `turma` (`idturma`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `diariodeclasse-usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Diario de Classe (Relacionamento: Turma > Usuario); InnoDB f; InnoDB free: 81920';

#
# Structure for the `type_ctps` table : 
#

CREATE TABLE `type_ctps` (
  `idtype_ctps` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero` INTEGER(10) NOT NULL COMMENT 'Número do CTPS',
  `serie` CHAR(5) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Série',
  `dataemissao` DATE NOT NULL COMMENT 'Data de Emissão',
  `idcbx_estado` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Estado',
  PRIMARY KEY (`idtype_ctps`),
  KEY `fk_estado` (`idcbx_estado`),
  CONSTRAINT `estado-ctps` FOREIGN KEY (`idcbx_estado`) REFERENCES `cbx_estado` (`idcbx_estado`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Carteira de Trabalho (CTPS); InnoDB free: 9216 kB;; InnoDB free: 81920';

#
# Structure for the `type_tituloeleitor` table : 
#

CREATE TABLE `type_tituloeleitor` (
  `idtype_tituloeleitor` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero` CHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Número do Título de Eleitor',
  `zona` CHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Zona Eleitoral',
  `secao` CHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Seção',
  PRIMARY KEY (`idtype_tituloeleitor`)
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='DataType: Titulo Eleitor; InnoDB free: 9216 kB; InnoDB free: 81920 kB';

#
# Structure for the `monitor` table : 
#

CREATE TABLE `monitor` (
  `idmonitor` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admissao` DATE NOT NULL COMMENT 'Data Admissão',
  `nome` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome do Monitor',
  `nascimento` DATE NOT NULL DEFAULT '0000-00-00' COMMENT 'Data de Nascimento',
  `idcbx_sexo` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Sexo',
  `idcbx_estadocivil` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Estado Civil',
  `idcbx_etnia` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Etnia',
  `idtype_nacionalidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Nacionalidade',
  `nomemae` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome Completo da Mãe',
  `nomepai` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome Completo do Pai',
  `idtype_endereco` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Endereço',
  `idtype_identidade` INTEGER(11) UNSIGNED DEFAULT '0' COMMENT 'Identidade',
  `cpf` CHAR(14) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'CPF',
  `idtype_tituloeleitor` INTEGER(11) UNSIGNED DEFAULT '0' COMMENT 'Título Eleitor',
  `idtype_ctps` INTEGER(11) UNSIGNED DEFAULT '0' COMMENT 'CTPS',
  `idtype_escolaridade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Escolaridade',
  `experiencias` LONGTEXT COMMENT 'Experiencia Profissional',
  `atividades` LONGTEXT COMMENT 'Atividades Extra-Curriculares',
  `idcbx_status` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Estado do Registro',
  PRIMARY KEY (`idmonitor`),
  KEY `fk_sexo` (`idcbx_sexo`),
  KEY `fk_estadocivil` (`idcbx_estadocivil`),
  KEY `fk_endereco` (`idtype_endereco`),
  KEY `fk_identidade` (`idtype_identidade`),
  KEY `fk_ctps` (`idtype_ctps`),
  KEY `fk_tituloeleitor` (`idtype_tituloeleitor`),
  KEY `fk_escolaridade` (`idtype_escolaridade`),
  KEY `fk_status` (`idcbx_status`),
  KEY `fk_nacionalidade` (`idtype_nacionalidade`),
  KEY `fk_etina` (`idcbx_etnia`),
  CONSTRAINT `ctps-monitor` FOREIGN KEY (`idtype_ctps`) REFERENCES `type_ctps` (`idtype_ctps`) ON UPDATE CASCADE,
  CONSTRAINT `endereco-monitor` FOREIGN KEY (`idtype_endereco`) REFERENCES `type_endereco` (`idtype_endereco`) ON UPDATE CASCADE,
  CONSTRAINT `escolaridade-monitor` FOREIGN KEY (`idtype_escolaridade`) REFERENCES `type_escolaridade` (`idtype_escolaridade`) ON UPDATE CASCADE,
  CONSTRAINT `estadocivil-monitor` FOREIGN KEY (`idcbx_estadocivil`) REFERENCES `cbx_estadocivil` (`idcbx_estadocivil`) ON UPDATE CASCADE,
  CONSTRAINT `etnia-monitor` FOREIGN KEY (`idcbx_etnia`) REFERENCES `cbx_etnia` (`idcbx_etnia`) ON UPDATE CASCADE,
  CONSTRAINT `identidade-monitor` FOREIGN KEY (`idtype_identidade`) REFERENCES `type_identidade` (`idtype_identidade`) ON UPDATE CASCADE,
  CONSTRAINT `nacionalidade-monitor` FOREIGN KEY (`idtype_nacionalidade`) REFERENCES `type_nacionalidade` (`idtype_nacionalidade`) ON UPDATE CASCADE,
  CONSTRAINT `sexo-monitor` FOREIGN KEY (`idcbx_sexo`) REFERENCES `cbx_sexo` (`idcbx_sexo`) ON UPDATE CASCADE,
  CONSTRAINT `status-monitor` FOREIGN KEY (`idcbx_status`) REFERENCES `cbx_status` (`idcbx_status`) ON UPDATE CASCADE,
  CONSTRAINT `tituloeleitor-monitor` FOREIGN KEY (`idtype_tituloeleitor`) REFERENCES `type_tituloeleitor` (`idtype_tituloeleitor`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Monitor / Instrutor - Quantidade Máxima de Monitor: 999999; ; InnoDB free: 81920';

#
# Structure for the `sala` table : 
#

CREATE TABLE `sala` (
  `idsala` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idunidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Unidade',
  `sala` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome / Número da Sala',
  `quantidade_estacao` INTEGER(11) NOT NULL DEFAULT '0' COMMENT 'Quantidade de Estações',
  PRIMARY KEY (`idsala`),
  KEY `fk_unidade` (`idunidade`),
  CONSTRAINT `unidade-sala` FOREIGN KEY (`idunidade`) REFERENCES `unidade` (`idunidade`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Salas da Unidade; InnoDB free: 9216 kB; (`idunidade`) REFER ; InnoDB free: 81920';

#
# Structure for the `unidade_has_monitor` table : 
#

CREATE TABLE `unidade_has_monitor` (
  `idunidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Unidade',
  `idmonitor` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Monitor',
  KEY `fk_unidade` (`idunidade`),
  KEY `fk_monitor` (`idmonitor`),
  CONSTRAINT `monitor-has-unidade` FOREIGN KEY (`idmonitor`) REFERENCES `monitor` (`idmonitor`) ON UPDATE CASCADE,
  CONSTRAINT `unidade-has-monitor` FOREIGN KEY (`idunidade`) REFERENCES `unidade` (`idunidade`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB
CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Relacionamento: Unidade > Monitor; InnoDB free: 9216 kB; (`i; InnoDB free: 81920';

#
# Structure for the `useraccount` table : 
#

CREATE TABLE `useraccount` (
  `iduseraccount` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nome do Usuário',
  `username` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Usuário',
  `password` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Senha do Usuário',
  `idcbx_levelaccess` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Nível do Usuário',
  PRIMARY KEY (`iduseraccount`),
  KEY `fk_levelaccess` (`idcbx_levelaccess`),
  CONSTRAINT `levelaccess-useraccount` FOREIGN KEY (`idcbx_levelaccess`) REFERENCES `cbx_levelaccess` (`idcbx_levelaccess`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Contas de Usuário; InnoDB free: 9216 kB; (`idcbx_levelaccess; InnoDB free: 81920';

#
# Structure for the `usuario_has_turma` table : 
#

CREATE TABLE `usuario_has_turma` (
  `idusuario_has_turma` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `idusuario` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Usuário',
  `idturma` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Turma',
  PRIMARY KEY (`idusuario_has_turma`),
  KEY `fk_usuario` (`idusuario`),
  KEY `fk_turma` (`idturma`),
  CONSTRAINT `turma-has-usuario` FOREIGN KEY (`idturma`) REFERENCES `turma` (`idturma`) ON UPDATE CASCADE,
  CONSTRAINT `usuario-has-turma` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Relacionamento: Usuário > Turma; InnoDB free: 9216 kB; (`idt; InnoDB free: 81920';

#
# Structure for the `worktime` table : 
#

CREATE TABLE `worktime` (
  `idworktime` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `idunidade` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0',
  `dayweek` CHAR(1) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Dia da Semana',
  `dayname` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome do Dia',
  `start` TIME DEFAULT NULL COMMENT 'Hora Inicial',
  `end` TIME DEFAULT NULL COMMENT 'Hora Final',
  PRIMARY KEY (`idworktime`),
  KEY `fk_unidade` (`idunidade`),
  CONSTRAINT `unidade-worktime` FOREIGN KEY (`idunidade`) REFERENCES `unidade` (`idunidade`) ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=12 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Horário de Expediente; InnoDB free: 9216 kB; (`idunidade`) R; InnoDB free: 81920';

#
# Structure for the `worktime_has_turma` table : 
#

CREATE TABLE `worktime_has_turma` (
  `idworktime_has_turma` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `idworktime` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Horário de Expediente',
  `idturma` INTEGER(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Turma',
  `inicio` TIME NOT NULL DEFAULT '00:00:00' COMMENT 'Hora Inicial',
  `termino` TIME NOT NULL DEFAULT '00:00:00' COMMENT 'Hora Final',
  PRIMARY KEY (`idworktime_has_turma`),
  KEY `fk_worktime` (`idworktime`),
  KEY `fk_turma` (`idturma`),
  CONSTRAINT `turma-has-worktime` FOREIGN KEY (`idturma`) REFERENCES `turma` (`idturma`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `worktime-has-turma` FOREIGN KEY (`idworktime`) REFERENCES `worktime` (`idworktime`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Relacionamento: Horário de Expediente > Turma; InnoDB free: ; InnoDB free: 81920';

#
# Data for the `cbx_federacao` table  (LIMIT 0,500)
#

INSERT INTO `cbx_federacao` (`idcbx_federacao`, `federacao`) VALUES 
  (1,'&Atilde;frica Central'),
  (2,'&Atilde;frica do Sul'),
  (3,'&Atilde;ndia'),
  (4,'&Atilde;ustria'),
  (5,'Abec&Atilde;&iexcl;sia'),
  (6,'Afeganist&Atilde;&pound;o'),
  (7,'Alb&Atilde;&cent;nia'),
  (8,'Alemanha'),
  (9,'Alto Carabaque'),
  (10,'Andorra'),
  (11,'Angola'),
  (12,'Ant&Atilde;&shy;gua e Barbuda'),
  (13,'Ar&Atilde;&iexcl;bia Saudita'),
  (14,'Arg&Atilde;&copy;lia'),
  (15,'Argentina'),
  (16,'Arm&Atilde;&copy;nia (Arm&Atilde;&ordf;nia)'),
  (17,'Austr&Atilde;&iexcl;lia'),
  (18,'Azerbaij&Atilde;&pound;o'),
  (19,'B&Atilde;&copy;lgica'),
  (20,'B&Atilde;&sup3;snia e Herzegovina'),
  (21,'Baamas'),
  (22,'Bangladeche'),
  (23,'Bar&Atilde;&copy;m'),
  (24,'Barbados'),
  (25,'Belize'),
  (26,'Benim'),
  (27,'Bielorr&Atilde;&ordm;ssia'),
  (28,'Birm&Atilde;&cent;nia'),
  (29,'Bol&Atilde;&shy;via'),
  (30,'Botsuana'),
  (31,'Br&Atilde;&ordm;nei'),
  (32,'Brasil'),
  (33,'Bulg&Atilde;&iexcl;ria'),
  (34,'Bur&Atilde;&ordm;ndi'),
  (35,'Burquina Faso'),
  (36,'But&Atilde;&pound;o'),
  (37,'Cabo Verde'),
  (38,'Camar&Atilde;&micro;es'),
  (39,'Camboja'),
  (40,'Canad&Atilde;&iexcl;'),
  (41,'Catar'),
  (42,'Cazaquist&Atilde;&pound;o'),
  (43,'Ceil&Atilde;&pound;o'),
  (44,'Chade'),
  (45,'Chile'),
  (46,'Chipre'),
  (47,'Chipre do Norte'),
  (48,'Cidade do Vaticano'),
  (49,'Cingapura'),
  (50,'Col&Atilde;&acute;mbia'),
  (51,'Comores'),
  (52,'Coreia do Norte (Cor&Atilde;&copy;ia do Norte)'),
  (53,'Coreia do Sul (Cor&Atilde;&copy;ia do Sul)'),
  (54,'Cossovo'),
  (55,'Costa do Marfim'),
  (56,'Costa Rica'),
  (57,'Couaite (Coveite)'),
  (58,'Cro&Atilde;&iexcl;cia'),
  (59,'Cuba'),
  (60,'Dinamarca'),
  (61,'Djib&Atilde;&ordm;ti'),
  (62,'Dominica'),
  (63,'Egipto (Egito)'),
  (64,'El Salvador'),
  (65,'Emirados &Atilde;rabes Unidos'),
  (66,'Equador'),
  (67,'Eritreia (Eritr&Atilde;&copy;ia)'),
  (68,'Eslov&Atilde;&copy;nia (Eslov&Atilde;&ordf;nia)'),
  (69,'Eslov&Atilde;&iexcl;quia'),
  (70,'Espanha'),
  (71,'Est&Atilde;&sup3;nia (Est&Atilde;&acute;nia)'),
  (72,'Estados Federados da Micron&Atilde;&copy;sia'),
  (73,'Estados Unidos'),
  (74,'Eti&Atilde;&sup3;pia'),
  (75,'Fiji'),
  (76,'Filipinas'),
  (77,'Finl&Atilde;&cent;ndia'),
  (78,'Formosa'),
  (79,'Fran&Atilde;&sect;a'),
  (80,'G&Atilde;&cent;mbia'),
  (81,'Gab&Atilde;&pound;o'),
  (82,'Gana'),
  (83,'Ge&Atilde;&sup3;rgia'),
  (84,'Gr&Atilde;&copy;cia'),
  (85,'Gr&Atilde;&pound;-Bretanha'),
  (86,'Granada'),
  (87,'Guatemala'),
  (88,'Guiana'),
  (89,'Guin&Atilde;&copy;'),
  (90,'Guin&Atilde;&copy; Equatorial'),
  (91,'Guin&Atilde;&copy;-Bissau'),
  (92,'Haiti'),
  (93,'Holanda'),
  (94,'Honduras'),
  (95,'Hungria'),
  (96,'I&Atilde;&copy;men (I&Atilde;&ordf;men)'),
  (97,'Ilhas M&Atilde;&iexcl;rchal'),
  (98,'Ilhas Salom&Atilde;&pound;o'),
  (99,'Indon&Atilde;&copy;sia'),
  (100,'Inglaterra'),
  (101,'Ir&Atilde;&pound;o (Ir&Atilde;&pound;)'),
  (102,'Iraque'),
  (103,'Isl&Atilde;&cent;ndia'),
  (104,'Israel'),
  (105,'It&Atilde;&iexcl;lia'),
  (106,'Jamaica'),
  (107,'Jap&Atilde;&pound;o'),
  (108,'Jib&Atilde;&ordm;ti'),
  (109,'Jord&Atilde;&cent;nia'),
  (110,'Katar'),
  (111,'Kiribati'),
  (112,'Kosovo'),
  (113,'Kuwait'),
  (114,'L&Atilde;&shy;bano'),
  (115,'L&Atilde;&shy;bia'),
  (116,'Laus'),
  (117,'Lesoto'),
  (118,'Let&Atilde;&sup3;nia (Let&Atilde;&acute;nia)'),
  (119,'Lib&Atilde;&copy;ria'),
  (120,'Listenstaine'),
  (121,'Litu&Atilde;&cent;nia'),
  (122,'Luxemburgo'),
  (123,'M&Atilde;&copy;xico'),
  (124,'M&Atilde;&sup3;naco (M&Atilde;&acute;naco)'),
  (125,'Maced&Atilde;&sup3;nia (Maced&Atilde;&acute;nia)'),
  (126,'Madag&Atilde;&iexcl;scar (Madagascar)'),
  (127,'Mal&Atilde;&iexcl;sia'),
  (128,'Mal&Atilde;&iexcl;ui'),
  (129,'Maldivas'),
  (130,'Mali'),
  (131,'Malta'),
  (132,'Marrocos'),
  (133,'Maur&Atilde;&shy;cia'),
  (134,'Maurit&Atilde;&cent;nia'),
  (135,'Mianmar (Birm&Atilde;&cent;nia)'),
  (136,'Mo&Atilde;&sect;ambique'),
  (137,'Mold&Atilde;&iexcl;via'),
  (138,'Mong&Atilde;&sup3;lia'),
  (139,'Montenegro'),
  (140,'N&Atilde;&shy;ger'),
  (141,'Nagorno-Carabaque'),
  (142,'Nam&Atilde;&shy;bia'),
  (143,'Nauru'),
  (144,'Nepal'),
  (145,'Nicar&Atilde;&iexcl;gua'),
  (146,'Nig&Atilde;&copy;ria'),
  (147,'Noruega'),
  (148,'Nova Zel&Atilde;&cent;ndia'),
  (149,'Om&Atilde;&pound;'),
  (150,'Oss&Atilde;&copy;tia do Sul'),
  (151,'Pa&Atilde;&shy;ses Baixos'),
  (152,'Palau'),
  (153,'Palestina'),
  (154,'Panam&Atilde;&iexcl;'),
  (155,'Papua Nova Guin&Atilde;&copy;'),
  (156,'Paquist&Atilde;&pound;o'),
  (157,'Paraguai'),
  (158,'Peru'),
  (159,'Pol&Atilde;&sup3;nia (Pol&Atilde;&acute;nia)'),
  (160,'Portugal'),
  (161,'Pridnestr&Atilde;&sup3;via'),
  (162,'Qatar'),
  (163,'Qu&Atilde;&copy;nia (Qu&Atilde;&ordf;nia)'),
  (164,'Quaite'),
  (165,'Quirguist&Atilde;&pound;o'),
  (166,'Quirib&Atilde;&iexcl;ti'),
  (167,'R&Atilde;&ordm;ssia'),
  (168,'Reino Unido'),
  (169,'Rep&Atilde;&ordm;blica Centro-Africana'),
  (170,'Rep&Atilde;&ordm;blica Checa (Rep&Atilde;&ordm;blica Tcheca)'),
  (171,'Rep&Atilde;&ordm;blica da China'),
  (172,'Rep&Atilde;&ordm;blica da Irlanda'),
  (173,'Rep&Atilde;&ordm;blica Democr&Atilde;&iexcl;tica do Congo'),
  (174,'Rep&Atilde;&ordm;blica do Congo'),
  (175,'Rep&Atilde;&ordm;blica Dominicana'),
  (176,'Rep&Atilde;&ordm;blica Popular da China'),
  (177,'Rep&Atilde;&ordm;blica Tcheca'),
  (178,'Rom&Atilde;&copy;nia (Rom&Atilde;&ordf;nia)'),
  (179,'Ruanda'),
  (180,'S&Atilde;&copy;rvia'),
  (181,'S&Atilde;&pound;o Crist&Atilde;&sup3;v&Atilde;&pound;o e Neves'),
  (182,'S&Atilde;&pound;o Domingos'),
  (183,'S&Atilde;&pound;o Marinho'),
  (184,'S&Atilde;&pound;o Tom&Atilde;&copy; e Pr&Atilde;&shy;ncipe'),
  (185,'S&Atilde;&pound;o Vicente e Granadinas'),
  (186,'S&Atilde;&shy;ria'),
  (187,'Saara Ocidental'),
  (188,'Salvador'),
  (189,'Samoa'),
  (190,'Santa L&Atilde;&ordm;cia'),
  (191,'Seicheles'),
  (192,'Senegal'),
  (193,'Serra Leoa'),
  (194,'Singapura (Cingapura)'),
  (195,'Som&Atilde;&iexcl;lia'),
  (196,'Somalil&Atilde;&cent;ndia'),
  (197,'Sri Lanca'),
  (198,'Su&Atilde;&copy;cia'),
  (199,'Su&Atilde;&shy;&Atilde;&sect;a'),
  (200,'Suazil&Atilde;&cent;ndia'),
  (201,'Sud&Atilde;&pound;o'),
  (202,'Suriname'),
  (203,'Tail&Atilde;&cent;ndia'),
  (204,'Taiu&Atilde;&pound;'),
  (205,'Tajiquist&Atilde;&pound;o'),
  (206,'Tanz&Atilde;&cent;nia'),
  (207,'Tchade'),
  (208,'Timor-Leste'),
  (209,'Togo'),
  (210,'Tonga'),
  (211,'Transdni&Atilde;&copy;stria'),
  (212,'Trindade e Tobago (Trinidad e Tobago)'),
  (213,'Tun&Atilde;&shy;sia'),
  (214,'Turcomenist&Atilde;&pound;o'),
  (215,'Turquia'),
  (216,'Tuvalu'),
  (217,'Ucr&Atilde;&cent;nia'),
  (218,'Uganda'),
  (219,'Uruguai'),
  (220,'Usbequist&Atilde;&pound;o'),
  (221,'Vanuato'),
  (222,'Venezuela'),
  (223,'Vietname (Vietn&Atilde;&pound;)'),
  (224,'Y&Atilde;&copy;men (Y&Atilde;&ordf;men)'),
  (225,'Z&Atilde;&cent;mbia'),
  (226,'Zimb&Atilde;&iexcl;bue');
COMMIT;

#
# Data for the `cbx_estado` table  (LIMIT 0,500)
#

INSERT INTO `cbx_estado` (`idcbx_estado`, `idcbx_federacao`, `estado`, `uf`) VALUES 
  (1,32,'Acre','AC'),
  (2,32,'Alagoas','AL'),
  (3,32,'Amap&Atilde;&iexcl;','AP'),
  (4,32,'Amazonas','AM'),
  (5,32,'Bahia','BA'),
  (6,32,'Cear&Atilde;&iexcl;','CE'),
  (7,32,'Distrito Federal','DF'),
  (8,32,'Esp&Atilde;&shy;rito Santo','ES'),
  (9,32,'Goi&Atilde;&iexcl;s','GO'),
  (10,32,'Maranh&Atilde;&pound;o','MA'),
  (11,32,'Mato Grosso do Sul','MS'),
  (12,32,'Mato Grosso','MT'),
  (13,32,'Minas Gerais','MG'),
  (14,32,'Par&Atilde;&iexcl;','PA'),
  (15,32,'Para&Atilde;&shy;ba','PB'),
  (16,32,'Paran&Atilde;&iexcl;','PR'),
  (17,32,'Pernambuco','PE'),
  (18,32,'Piau&Atilde;&shy;','PI'),
  (19,32,'Rio de Janeiro','RJ'),
  (20,32,'Rio Grande do Norte','RN'),
  (21,32,'Rio Grande do Sul','RS'),
  (22,32,'Rond&Atilde;&acute;nia','RO'),
  (23,32,'Roraima','RR'),
  (24,32,'S&Atilde;&pound;o Paulo','SP'),
  (25,32,'Santa Catarina','SC'),
  (26,32,'Sergipe','SE'),
  (27,32,'Tocantins','TO');
COMMIT;

#
# Data for the `cbx_cidade` table  (LIMIT 0,500)
#

INSERT INTO `cbx_cidade` (`idcbx_cidade`, `idcbx_estado`, `cidade`) VALUES 
  (1,1,'Rio Branco'),
  (2,2,'Macei&Atilde;&sup3;'),
  (3,3,'Macap&Atilde;&iexcl;'),
  (4,4,'Manaus'),
  (5,5,'Salvador'),
  (6,6,'Fortaleza'),
  (7,7,'Bras&Atilde;&shy;lia'),
  (8,8,'Vit&Atilde;&sup3;ria'),
  (9,9,'Goi&Atilde;&cent;nia'),
  (10,10,'S&Atilde;&pound;o Lu&Atilde;&shy;s'),
  (11,11,'Cuiab&Atilde;&iexcl;'),
  (12,12,'Campo Grande'),
  (13,13,'Belo Horizonte'),
  (14,14,'Bel&Atilde;&copy;m'),
  (15,15,'Jo&Atilde;&pound;o Pessoa'),
  (16,16,'Curitiba'),
  (17,17,'Recife'),
  (18,18,'Teresina'),
  (19,19,'Rio de Janeiro'),
  (20,20,'Natal'),
  (21,21,'Porto Alegre'),
  (22,22,'Porto Velho'),
  (23,23,'Boa Vista'),
  (24,24,'Florian&Atilde;&sup3;polis'),
  (25,25,'S&Atilde;&pound;o Paulo'),
  (26,26,'Aracaju'),
  (27,27,'Palmas');
COMMIT;

#
# Data for the `cbx_conteudo` table  (LIMIT 0,500)
#

INSERT INTO `cbx_conteudo` (`idcbx_conteudo`, `conteudo`, `cargahoraria`, `descricao`) VALUES 
  (1,'O que &Atilde;&copy; Linux?',1,'Uma abordagem ao linux...'),
  (2,'Licen&Atilde;&sect;as, patentes',1,''),
  (3,'Camadas',1,''),
  (4,'Compara&Atilde;&sect;&Atilde;&pound;o software livre X software propriet&Atilde;&iexcl;rio',1,''),
  (5,'O que &Atilde;&copy; uma distribui&Atilde;&sect;&Atilde;&pound;o',1,''),
  (6,'Distribui&Atilde;&sect;&Atilde;&micro;es mais comuns',1,''),
  (7,'Pol&Atilde;&shy;tica de usu&Atilde;&iexcl;rios e grupos',2,''),
  (8,'Parti&Atilde;&sect;&Atilde;&micro;es',1,''),
  (9,'Sistemas de arquivos',1,''),
  (10,'Comandos mais usados',1,''),
  (11,'Servidor X',1,''),
  (12,'Gerenciadores de janelas mais comuns: GNOME, KDE, IceWm, WindowMaker',1,''),
  (13,'Internet',1,''),
  (14,'Escrit&Atilde;&sup3;rio',1,''),
  (15,'Multim&Atilde;&shy;dia',1,''),
  (16,'Desenvolvimento',1,''),
  (17,'Compilando dos fontes',1,''),
  (18,'Instalando bin&Atilde;&iexcl;rios pr&Atilde;&copy;-compilados para sua distribui&Atilde;&sect;&Atilde;&pound;o (sistemas de pacotes)',1,''),
  (19,'Scripts instaladores',1,''),
  (20,'Adaptando o software para suas necessidades',1,''),
  (21,'Otimiza&Atilde;&sect;&Atilde;&pound;o por compila&Atilde;&sect;&Atilde;&pound;o',1,''),
  (22,'Feedback para os desenvolvedores',3,''),
  (23,'Comandos Comuns',1,'');
COMMIT;

#
# Data for the `cbx_estadocivil` table  (LIMIT 0,500)
#

INSERT INTO `cbx_estadocivil` (`idcbx_estadocivil`, `estadocivil`) VALUES 
  (1,'Separado'),
  (2,'Solteiro'),
  (3,'Casado'),
  (4,'Divorciado'),
  (5,'Vi&Atilde;&ordm;vo');
COMMIT;

#
# Data for the `cbx_etnia` table  (LIMIT 0,500)
#

INSERT INTO `cbx_etnia` (`idcbx_etnia`, `etnia`) VALUES 
  (1,'Branco'),
  (2,'Negro'),
  (3,'Pardo'),
  (4,'Amarelo'),
  (5,'Ind&Atilde;&shy;gena');
COMMIT;

#
# Data for the `cbx_grauescolar` table  (LIMIT 0,500)
#

INSERT INTO `cbx_grauescolar` (`idcbx_grauescolar`, `grauescolar`) VALUES 
  (1,'Analfabeto'),
  (2,'At&Atilde;&copy; 4&Acirc;&ordf; s&Atilde;&copy;rie'),
  (3,'Com 4&Acirc;&ordf; s&Atilde;&copy;rie completa do ensino fundamental'),
  (4,'De 5&Acirc;&ordf; a 8&Acirc;&ordf; s&Atilde;&copy;rie completa do ensino fundamental'),
  (5,'Ensino fundamental completo'),
  (6,'Ensino m&Atilde;&copy;dio inompleto'),
  (7,'Ensino m&Atilde;&copy;dio completo'),
  (8,'Especializa&Atilde;&sect;&Atilde;&pound;o'),
  (9,'Ensino superior incompleto'),
  (10,'Ensino superior completo'),
  (11,'Mestrado'),
  (12,'Doutorado');
COMMIT;

#
# Data for the `cbx_levelaccess` table  (LIMIT 0,500)
#

INSERT INTO `cbx_levelaccess` (`idcbx_levelaccess`, `levelaccess`, `description`) VALUES 
  (1,'Super-Usu&Atilde;&iexcl;rio','Administrador Geral do Sistema'),
  (2,'Gerente','Gerente de Projetos: responsável por fazer a análise de desempenho das unidades, dando um posicionamento sobre o futuro do projeto, elaborados pelos coordenadores operacionais de área.'),
  (3,'Coordenador','Coordenador Operacional de Área: responsável pela manutenção dos cursos, contratação de monitores para as unidades, além de fazer a instalação e extinção das unidades.'),
  (4,'Supervisor','Supervisor de Unidade: responsável pela manutenção operacional da unidade, isto é, manter íntegra as turmas instaladas nas unidades, cadastro de usuários em dia, planos de aula e a organização física da unidade.'),
  (5,'Monitor','Monitor: responsável por instruir os alunos durante o curso prestado e auxiliar no desenvolver das atividades propostas pelo projeto.');
COMMIT;

#
# Data for the `cbx_modulo` table  (LIMIT 0,500)
#

INSERT INTO `cbx_modulo` (`idcbx_modulo`, `modulo`, `descricao`) VALUES 
  (1,'Introdu&Atilde;&sect;&Atilde;&pound;o ao Linux','Sobre Linux'),
  (2,'Defini&Atilde;&sect;&Atilde;&pound;o de Software Livre',''),
  (3,'O sistema operacional',''),
  (4,'Distribui&Atilde;&sect;&Atilde;&micro;es GNU/Linux',''),
  (5,'Caracter&Atilde;&shy;sticas gerais de um sistema compat&Atilde;&shy;vel com UNIX',''),
  (6,'O Shell',''),
  (7,'Ambiente gr&Atilde;&iexcl;fico',''),
  (8,'Aplicativos',''),
  (9,'Instala&Atilde;&sect;&Atilde;&pound;o de programas',''),
  (10,'Diferen&Atilde;&sect;as entre plataformas livres e propriet&Atilde;&iexcl;rias',''),
  (11,'Alguns comandos comuns.','');
COMMIT;

#
# Data for the `cbx_modulo_has_cbx_conteudo` table  (LIMIT 0,500)
#

INSERT INTO `cbx_modulo_has_cbx_conteudo` (`idcbx_modulo_has_cbx_conteudo`, `idcbx_modulo`, `idcbx_conteudo`) VALUES 
  (2,2,2),
  (3,3,3),
  (4,3,4),
  (5,4,5),
  (6,4,6),
  (7,5,7),
  (8,5,8),
  (9,5,9),
  (10,6,10),
  (11,7,11),
  (12,7,12),
  (13,8,13),
  (14,8,14),
  (15,8,15),
  (16,8,16),
  (17,9,17),
  (18,9,18),
  (19,9,19),
  (20,10,20),
  (21,10,21),
  (22,10,22),
  (24,11,23);
COMMIT;

#
# Data for the `type_curso` table  (LIMIT 0,500)
#

INSERT INTO `type_curso` (`idtype_curso`, `curso`, `descricao`) VALUES 
  (1,'Inform&Atilde;&iexcl;tica B&Atilde;&iexcl;sica','Blah'),
  (2,'Curso B&Atilde;&iexcl;sico de GNU/Linux','O GNU/Linux &Atilde;&copy; um sistema operacional livre compat&Atilde;&shy;vel com UNIX. Criado no in&Atilde;&shy;cio dos anos 90 pela jun&Atilde;&sect;&Atilde;&pound;o do kernel Linux com o sistema GNU, o GNU/Linux vem sendo popularizado e est&Atilde;&pound;o dispon&Atilde;&shy;veis aplica&Atilde;&sect;&Atilde;&micro;es para as mais diversas &Atilde;&iexcl;reas de atua&Atilde;&sect;&Atilde;&pound;o. Este curso, voltado para usu&Atilde;&iexcl;rios t&Atilde;&copy;cnicos, tem o objetivo de introduzir os conceitos filos&Atilde;&sup3;ficos sobre software livre e t&Atilde;&copy;cnicos sobre a opera&Atilde;&sect;&Atilde;&pound;o de um sistema GNU/Linux.');
COMMIT;

#
# Data for the `cbx_modulo_has_type_curso` table  (LIMIT 0,500)
#

INSERT INTO `cbx_modulo_has_type_curso` (`idcbx_modulo_has_type_curso`, `idcbx_modulo`, `idtype_curso`) VALUES 
  (1,1,1),
  (3,2,2),
  (4,3,2),
  (5,4,2),
  (6,5,2),
  (7,6,2),
  (8,7,2),
  (9,8,2),
  (10,9,2),
  (11,10,2),
  (12,11,2);
COMMIT;

#
# Data for the `cbx_nacionalidade` table  (LIMIT 0,500)
#

INSERT INTO `cbx_nacionalidade` (`idcbx_nacionalidade`, `nacionalidade`) VALUES 
  (1,'Brasileiro'),
  (2,'Brasileito naturalizado'),
  (3,'Estrangeiro');
COMMIT;

#
# Data for the `cbx_serieescolar` table  (LIMIT 0,500)
#

INSERT INTO `cbx_serieescolar` (`idcbx_serieescolar`, `serieescolar`) VALUES 
  (1,'Maternal I'),
  (2,'Maternal II'),
  (3,'Maternal II'),
  (4,'Jardim I'),
  (5,'Jardim II'),
  (6,'Jardim II'),
  (7,'CA (alfabetiza&Atilde;&sect;&Atilde;&pound;o)'),
  (8,'1&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (9,'2&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (10,'3&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (11,'4&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (12,'5&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (13,'6&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (14,'7&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (15,'8&Acirc;&ordf; s&Atilde;&copy;rie do ensino fundamental'),
  (16,'1&Acirc;&ordf; s&Atilde;&copy;rie do ensino m&Atilde;&copy;dio'),
  (17,'2&Acirc;&ordf; s&Atilde;&copy;rie do ensino m&Atilde;&copy;dio'),
  (18,'3&Acirc;&ordf; s&Atilde;&copy;rie do ensino m&Atilde;&copy;dio'),
  (19,'1&Acirc;&ordf; ano do ensino superior'),
  (20,'2&Acirc;&ordf; ano do ensino superior'),
  (21,'3&Acirc;&ordf; ano do ensino superior'),
  (22,'4&Acirc;&ordf; ano do ensino superior'),
  (23,'5&Acirc;&ordf; ano do ensino superior'),
  (24,'6&Acirc;&ordf; ano do ensino superior'),
  (25,'7&Acirc;&ordf; ano do ensino superior');
COMMIT;

#
# Data for the `cbx_sexo` table  (LIMIT 0,500)
#

INSERT INTO `cbx_sexo` (`idcbx_sexo`, `sexo`) VALUES 
  (1,'Feminino'),
  (2,'Masculino');
COMMIT;

#
# Data for the `cbx_situacaoprofissional` table  (LIMIT 0,500)
#

INSERT INTO `cbx_situacaoprofissional` (`idcbx_situacaoprofissional`, `situacao`) VALUES 
  (1,'Empregador'),
  (2,'Assalariado com carteira de trabalho'),
  (3,'Assalariado sem carteira de trabalho'),
  (4,'Aut&Atilde;&acute;nomo com previd&Atilde;&ordf;ncia social'),
  (5,'Aut&Atilde;&acute;nomo sem previd&Atilde;&ordf;ncia social'),
  (6,'Aposentado / Pensionista'),
  (7,'Trabalhador rual'),
  (8,'Empregador Rural'),
  (9,'N&Atilde;&pound;o trabalha'),
  (10,'Outra');
COMMIT;

#
# Data for the `cbx_status` table  (LIMIT 0,500)
#

INSERT INTO `cbx_status` (`idcbx_status`, `status`) VALUES 
  (1,'Ativo'),
  (2,'Inativo'),
  (3,'Em espera');
COMMIT;

#
# Data for the `cbx_tipoescola` table  (LIMIT 0,500)
#

INSERT INTO `cbx_tipoescola` (`idcbx_tipoescola`, `tipoescola`) VALUES 
  (1,'P&Atilde;&ordm;blica municipal'),
  (2,'P&Atilde;&ordm;blica estadual'),
  (3,'P&Atilde;&ordm;blica federal'),
  (4,'Particular'),
  (5,'Outra'),
  (6,'N&Atilde;&pound;o freq&Atilde;&frac14;enta');
COMMIT;

#
# Data for the `cbx_turnoescolar` table  (LIMIT 0,500)
#

INSERT INTO `cbx_turnoescolar` (`idcbx_turnoescolar`, `turnoescolar`) VALUES 
  (1,'Manh&Atilde;&pound;'),
  (2,'Tarde'),
  (3,'Noite'),
  (4,'Especial');
COMMIT;

#
# Data for the `type_endereco` table  (LIMIT 0,500)
#

INSERT INTO `type_endereco` (`idtype_endereco`, `idcbx_cidade`, `logradouro`, `bairro`, `cep`) VALUES 
  (1,19,'Rua Ata&Atilde;&shy;de Pimenta de Moraes','Centro','26210-190'),
  (2,19,'Rua Sebasti&Atilde;&pound;o de Melo, 384. Casas 01 e 02','Jardim Nova Era','26272-040'),
  (3,19,'R Dr. Getulio Vargas N&Acirc;&ordm;51','Centro','20010- 00'),
  (4,19,'Rua Sebasti&Atilde;&pound;o de Melo, 384. Casas 01 e 02','Jardim Nova Era','21350-180'),
  (5,19,'Rua Surucuas, 233','Miguel Coulto','26200-100'),
  (6,19,'Rua Luiz Tomaz, 582','Luz','26256-100'),
  (7,19,'Rua Luiz Tomaz, 582','Luz','26256-100');
COMMIT;

#
# Data for the `instituicao` table  (LIMIT 0,500)
#

INSERT INTO `instituicao` (`idinstituicao`, `nomefantasia`, `razaosocial`, `CNPJ`, `idtype_endereco`) VALUES 
  (1,'Prefeitura de Nova Igua&Atilde;&sect;u','Prefeitura Municipal de Nova Igua&Atilde;&sect;u','29.138.278/0001-01',1),
  (2,'CISANE','Centro de Integra&Atilde;&sect;&Atilde;&pound;o Social Amigos do Nova Era','03.230.355/0001-65',2);
COMMIT;

#
# Data for the `unidade` table  (LIMIT 0,500)
#

INSERT INTO `unidade` (`idunidade`, `idinstituicao`, `nome`, `idtype_endereco`) VALUES 
  (1,1,'Espa&Atilde;&sect;o Cultural Sylvio Monteiro',3),
  (2,2,'Centro de Integra&Atilde;&sect;&Atilde;&pound;o Social Amigos do Nova Era',4);
COMMIT;

#
# Data for the `turma` table  (LIMIT 0,500)
#

INSERT INTO `turma` (`idturma`, `codigo`, `idunidade`, `idtype_curso`, `inicio`, `termino`) VALUES 
  (1,'001.00001/08',1,2,'2008-12-14','2009-06-01');
COMMIT;

#
# Data for the `type_emprego` table  (LIMIT 0,500)
#

INSERT INTO `type_emprego` (`idtype_emprego`, `idcbx_situacaoprofissional`, `instituicao`, `admissao`, `ocupacao`, `remuneracao`) VALUES 
  (1,6,'','2008-12-14','',0.00),
  (2,6,'','2008-12-14','',750.00);
COMMIT;

#
# Data for the `type_escolaridade` table  (LIMIT 0,500)
#

INSERT INTO `type_escolaridade` (`idtype_escolaridade`, `idcbx_grauescolar`, `idcbx_serieescolar`, `idcbx_tipoescola`, `instituicao`, `curso`, `idcbx_turnoescolar`) VALUES 
  (1,9,22,4,'Universidade Igua&Atilde;&sect;u','Ci&Atilde;&ordf;ncias Biologicas',3),
  (2,5,15,1,'Blah','Blah',3);
COMMIT;

#
# Data for the `type_identidade` table  (LIMIT 0,500)
#

INSERT INTO `type_identidade` (`idtype_identidade`, `numero`, `orgaoemissor`, `dataemissao`) VALUES 
  (1,'000000-00','detran','2008-12-14'),
  (2,'0000000','00000','2008-12-14');
COMMIT;

#
# Data for the `type_nacionalidade` table  (LIMIT 0,500)
#

INSERT INTO `type_nacionalidade` (`idtype_nacionalidade`, `idcbx_nacionalidade`, `idcbx_federacao`) VALUES 
  (1,1,32),
  (2,1,32);
COMMIT;

#
# Data for the `usuario` table  (LIMIT 0,500)
#

INSERT INTO `usuario` (`idusuario`, `idunidade`, `inscricao`, `nome`, `nascimento`, `idcbx_sexo`, `idcbx_estadocivil`, `idcbx_etnia`, `idtype_nacionalidade`, `idtype_endereco`, `idtype_identidade`, `cpf`, `idtype_escolaridade`, `idtype_emprego`, `idcbx_status`) VALUES 
  (1,1,'2008-12-14','Tania Christine Teixeira Reis dos Santos','1958-01-05',1,3,3,2,7,2,'693.722.502-44',2,2,1);
COMMIT;

#
# Data for the `type_ctps` table  (LIMIT 0,500)
#

INSERT INTO `type_ctps` (`idtype_ctps`, `numero`, `serie`, `dataemissao`, `idcbx_estado`) VALUES 
  (1,0,'00000','2008-12-14',19);
COMMIT;

#
# Data for the `type_tituloeleitor` table  (LIMIT 0,500)
#

INSERT INTO `type_tituloeleitor` (`idtype_tituloeleitor`, `numero`, `zona`, `secao`) VALUES 
  (1,'0000.0000.0000.0000','0000','000');
COMMIT;

#
# Data for the `monitor` table  (LIMIT 0,500)
#

INSERT INTO `monitor` (`idmonitor`, `admissao`, `nome`, `nascimento`, `idcbx_sexo`, `idcbx_estadocivil`, `idcbx_etnia`, `idtype_nacionalidade`, `nomemae`, `nomepai`, `idtype_endereco`, `idtype_identidade`, `cpf`, `idtype_tituloeleitor`, `idtype_ctps`, `idtype_escolaridade`, `experiencias`, `atividades`, `idcbx_status`) VALUES 
  (1,'2008-12-14','Monique Siqueira Br&Atilde;&iexcl;s','1986-05-05',1,2,1,1,'Ivonete Siqueira','In&Atilde;&iexcl;cio Br&Atilde;&iexcl;s',5,1,'000.000.000-00',1,1,1,'Blah','Blah',1);
COMMIT;

#
# Data for the `unidade_has_monitor` table  (LIMIT 0,500)
#

INSERT INTO `unidade_has_monitor` (`idunidade`, `idmonitor`) VALUES 
  (1,1);
COMMIT;

#
# Data for the `useraccount` table  (LIMIT 0,500)
#

INSERT INTO `useraccount` (`iduseraccount`, `name`, `username`, `password`, `idcbx_levelaccess`) VALUES 
  (1,'Adminstrador','YWRtaW4.','MTIzNA..',1);
COMMIT;

#
# Data for the `usuario_has_turma` table  (LIMIT 0,500)
#

INSERT INTO `usuario_has_turma` (`idusuario_has_turma`, `idusuario`, `idturma`) VALUES 
  (1,1,1);
COMMIT;

#
# Data for the `worktime` table  (LIMIT 0,500)
#

INSERT INTO `worktime` (`idworktime`, `idunidade`, `dayweek`, `dayname`, `start`, `end`) VALUES 
  (1,1,'2','Segunda - Feira','09:00:00','18:00:00'),
  (2,1,'3','Ter&Atilde;&sect;a-feira','09:00:00','18:00:00'),
  (3,1,'4','Quarta-feira','09:00:00','18:00:00'),
  (4,1,'5','Quinta-feira','09:00:00','18:00:00'),
  (5,1,'6','Sexta-feira','09:00:00','18:00:00'),
  (6,1,'7','S&Atilde;&iexcl;bado','09:00:00','13:00:00'),
  (7,2,'2','Segunda-feira','09:00:00','19:00:00'),
  (8,2,'3','Ter&Atilde;&sect;a-feira','09:00:00','19:00:00'),
  (9,2,'4','Quarta-feira','09:00:00','19:00:00'),
  (10,2,'5','Quinta-feira','09:00:00','19:00:00'),
  (11,2,'6','Sexta-feira','09:00:00','19:00:00');
COMMIT;

#
# Data for the `worktime_has_turma` table  (LIMIT 0,500)
#

INSERT INTO `worktime_has_turma` (`idworktime_has_turma`, `idworktime`, `idturma`, `inicio`, `termino`) VALUES 
  (1,1,1,'09:00:00','10:00:00');
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;