<?php


    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("string.php", "$url_check")) {
      header ("Location: /index.php");
    }
    
    /**
     * @author Heron Santos
     * @copyright 2008
     */

    /*
     * @class Manipulação de String
     */
    class String {
        /** construtor */
        public function __construct() { }

        /** remove campos html de formularios */
        public function _UNHTMLENTITIES($valor) {
            $this->valor = trim($valor);
            $trans_tbl = get_html_translation_table(HTML_ENTITIES);
            $trans_tbl = array_flip($trans_tbl);
            return strtr($this->valor,$trans_tbl);
        }

        /** converte os textos para o banco de dados */
        public function db2str($valor) {
            if (!is_array($valor) and isset($valor) and !empty($valor)) {
                $this->valor = trim($valor);
                $this->valor = stripslashes($this->valor);
                $this->valor = html_entity_decode($this->valor);
                return $this->valor;
            }
        }

        /** converte os textos para o banco de dados */
        public function str2db($valor) {
            if (!is_array($valor) and isset($valor) and !empty($valor)) {
                $this->valor = $valor;
                // remove palavras que contenham sintaxe sql
                $this->valor = preg_replace("/(from|alter table|select|insert|delete|update|where|drop table|show tables|#|\*|--|\\\\)/i","",$this->valor);
                //limpa espaços vazio
                $this->valor = trim($this->valor);
                //converte tags html e php
                $this->valor = htmlentities($this->valor);
                //Adiciona barras invertidas a uma string
                $this->valor = addslashes($this->valor);
                return $this->valor;
            }
        }

        /** metodo que retorna a data formatada
		 * parametros:data a ser formatada, formato da data atual, formato posterior */
        public function toDate($data,$formato_atual,$formato_novo){
            $formato_atual = strtoupper($formato_atual);
            $formato_novo = strtoupper($formato_novo);
            $dia = substr($data,strpos($formato_atual,"DD"),2);
            $mes = substr($data,strpos($formato_atual,"MM"),2);
            $ano = substr($data,strpos($formato_atual,"YYYY"),4);
            $data_nova = str_replace("DD",$dia,$formato_novo);
            $data_nova = str_replace("MM",$mes,$data_nova);
            $data_nova = str_replace("YYYY",$ano,$data_nova);
            return $data_nova;
        }

        /** criptografa caracteres na base 64 */ 
        public function crypTo($value){
            $this->value = base64_encode($value);
            $this->value = str_replace(array('+','/','='),array('-','_','.'),$this->value);
            return $this->value;
        } 

        /** criptografa caracteres na base 64 */ 
        public function deCrypt($value){
            $this->value = str_replace(array('-','_','.'),array('+','/','='),$value);
            $this->mod4 = strlen($this->value) % 4;
            if ($this->mod4) {
                $this->value .= substr('====', $this->mod4);
            }
            return base64_decode($this->value);
        }  

        /** segundos para horas */
        public function second2hour($seconds){
            $hours = round($seconds / 3600);
            $minutes = round(($seconds % 3600) / 60);
            $seconds = round(($seconds % 3600) % 60);
            if (strlen($minutes) < 2) $minutes = "0".$minutes;
            if (strlen($seconds) < 2) $seconds = "0".$seconds;

            return $hours.":".$minutes.":".$seconds;
        }

        /** metodo que retorna a data acrescida
		 * parametros: data a ser acrescida, dias, meses, anos */
        public function sumDate($data, $dias = 0, $meses = 0, $ano = 0) {
			$data = explode("/", $data);
			$newData = date("d/m/Y", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano));
			return $newData;
        }

        /** metodo que retorna o dia da semana
		 * parametros: data */
        public function getDayWeek($data) {
			$data = explode("/", $data);
			$dayweek = date("w", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano)) + 1;
			return $dayweek;
        }
    }
?>