<?php


    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("form.php", "$url_check")) {
      header ("Location: /index.php");
    }
	
	/**
	* @author Heron Santos
	* @copyright 2008
	*/
	
	/**
	 * Construtor
	 * @param $active [ true | false ]
	 * @param $action [ string ]
	 * @param $method [ post, get ]
	 */
	class Form {

		function Form($active = false, $action = NULL, $method = "GET", $other = NULL) {
			$this->active = $active;
			
			if (is_null($action) or empty($action))
				$this->action = " action=\"$action\"";
			else
				$this->action = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;
			
			$openform = "<form".$this->action." method=\"$method\"".$this->other.">";
			
			$this->active?$this->print[] = $openform:NULL;
		}
		
		/**
		 * Cria Caixa de Texto
		 * @param $type [ text | date | password | hidden ]
		 * @param $name [ string ]
		 * @param $caption [ string ]
		 * @param $obrigatory [ true | false ]
		 * @param $value [ string ]
		 * @param $maxlength [ int ]
		 * @param $disabled [ true | false ]
		 * @param $other [ string ]
		 */
		function Input($type, $name, $caption, $obrigatory = false, $value = NULL, $maxlength = NULL, $disabled = false, $other = NULL) {
			if (!empty($type))
				$this->type = $type;
			else
				$this->type = NULL;
			
			if (!empty($name))
				$this->name = $name;
			else
				$this->name = NULL;
			
			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;
			
			if ($obrigatory)
				$this->obrigatory = " * ";
			else
				$this->obrigatory = NULL;

			if (!empty($value))
				$this->value = $value;
			elseif ($this->type == "date")
				$this->value = date("d/m/Y");
			else
				$this->value = NULL;
			
			if (!empty($maxlength))
				$this->maxlength = $maxlength;
			else
				$this->maxlength = "255";
			
			if ($disabled)
				$this->disabled = " disabled=\"$disabled\"";
			else
				$this->disabled = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;
			
			if ($this->type == "hidden") {
				$this->print[] = "\t<input type=\"hidden\" name=\"".$this->name."\" value=\"".$this->value."\">";

			} elseif ($this->type == "date") {

				$this->print[] = "\t<script type=\"text/javascript\">";
				$this->print[] = "\t\twindow.addEvent('domready', function() {";
				$this->print[] = "\t\t\tnew vlaDatePicker(";
				$this->print[] = "\t\t\t\t'".$this->name."', {";
				
				if (!is_null($this->value)) {
					list($dd, $mm, $yy) = explode("/", $this->value);
					$this->print[] = "\t\t\t\tprefillDate: { day: ".$dd.", month: ".$mm.", year: ".$yy." }, ";
				}

				$this->print[] = "\t\t\t\tstyle: 'adobe_cs3',";
				$this->print[] = "\t\t\t\toffset: { y: 1 },";
				$this->print[] = "\t\t\t\tformat: 'd/m/y',";
				$this->print[] = "\t\t\t\tieTransitionColor: ''}";
				$this->print[] = "\t\t\t)";
				$this->print[] = "\t\t\t});";
				$this->print[] = "\t</script>";

				if(!is_null($this->caption))
					$this->print[] = "\t<div class=\"div-label\"><label for=\"".$this->name."\" class=\"label-date\">".$this->caption.": ".$this->obrigatory."</label></div>";
					
				$this->print[] = "\t<div class=\"div-input-date\"><input type=\"text\" class=\"input-date\" name=\"".$this->name."\" id=\"".$this->name."\" maxlength=\"10\"".$this->disabled.$this->other."></div>";

			} else {
				
				if(!is_null($this->caption))
					$this->print[] = "\t<div class=\"div-label\"><label for=\"".$this->name."\" class=\"label\">".$this->caption.": ".$this->obrigatory."</label></div>";

				$this->print[] = "\t<div class=\"div-input\"><input type=\"".$this->type."\" class=\"input\" value=\"$value\" name=\"".$this->name."\" id=\"".$this->name."\" maxlength=\"".$this->maxlength."\"".$this->disabled.$this->other."></div>";

			}
		}
		

		
		/**
		 * Cria CheckBox
		 * @param $type [ checkbox ]
		 * @param $name [ string ]
		 * @param $caption [ string ]
		 * @param $obrigatory [ true | false ]
		 * @param $value [ string ]
		 * @param $maxlength [ int ]
		 * @param $disabled [ true | false ]
		 * @param $other [ string ]
		 */
		function Checkbox($name, $caption, $obrigatory = false, $value = NULL, $checked = false, $disabled = false, $other = NULL) {
			if (!empty($name))
				$this->name = $name;
			else
				$this->name = NULL;
			
			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;
			
			if ($obrigatory)
				$this->obrigatory = " * ";
			else
				$this->obrigatory = NULL;
			
			if (!empty($value))
				$this->value = $value;
			else
				$this->value = NULL;
			
			if (!empty($maxlength))
				$this->maxlength = $maxlength;
			else
				$this->maxlength = "255";
			
			if ($disabled)
				$this->disabled = " disabled=\"$disabled\"";
			else
				$this->disabled = NULL;
			
			if ($checked)
				$this->checked = " checked=\"$checked\"";
			else
				$this->checked = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;

			$this->print[] = "\t<div class=\"div-checkbox\">\n";
			$this->print[] = "\t<input type=\"checkbox\" class=\"checkbox\" value=\"".$this->value."\" name=\"".$this->name."\" id=\"".$this->name."\" ".$this->checked.$this->disabled.$this->other.">\n";

			if(!is_null($this->caption))
				$this->print[] = "\t<label for=\"".$this->name."\">".$this->caption."</label>";

			$this->print[] = "\t</div>\n";
		}

		/**
		 * Cria Radiobox
		 * @param $type [ checkbox ]
		 * @param $name [ string ]
		 * @param $caption [ string ]
		 * @param $obrigatory [ true | false ]
		 * @param $value [ string ]
		 * @param $maxlength [ int ]
		 * @param $disabled [ true | false ]
		 * @param $other [ string ]
		 */
		function Radiobox($name, $caption, $value = NULL, $checked = false, $disabled = false, $other = NULL) {
			if (!empty($name))
				$this->name = $name;
			else
				$this->name = NULL;

			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;

			if ($obrigatory)
				$this->obrigatory = " * ";
			else
				$this->obrigatory = NULL;

			if (!empty($value))
				$this->value = $value;
			else
				$this->value = NULL;

			if (!empty($maxlength))
				$this->maxlength = $maxlength;
			else
				$this->maxlength = "255";

			if ($disabled)
				$this->disabled = " disabled=\"$disabled\"";
			else
				$this->disabled = NULL;

			if ($checked)
				$this->checked = " checked=\"$checked\"";
			else
				$this->checked = NULL;

			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;

			$this->print[] = "\t<div class=\"div-radio\">\n";
			$this->print[] = "\t<input type=\"radio\" class=\"radio\" value=\"".$this->value."\" name=\"".$this->name."\" id=\"".$this->name."\" ".$this->checked.$this->disabled.$this->other.">\n";

			if(!is_null($this->caption))
				$this->print[] = "\t<label for=\"".$this->name."\">".$this->caption."</label>";

			$this->print[] = "\t</div>\n";

		}
		
		/**
		 * Cria Botao
		 * @param $id [ string ]
		 * @param $caption [ string ]
		 * @param $submit [ button | submit ]
		 * @param $onclick [ string ]
		 * @param $disabled [ true | false ]
		 * @param $other [ string ]
		 */
		function Button($id, $caption, $submit = "button", $onclick = NULL, $disabled = false, $modal = false, $other = NULL) {
			global $formbutton;
			
			if (!empty($id))
				$this->id = " id=\"".$id."\"";
			else
				$this->id = NULL;
			
			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;
			
			if (!empty($onclick))
				$this->onclick = " onclick=\"".$onclick."\"";
			else
				$this->onclick = NULL;
				
			if ($disabled)
				$this->disabled = " disabled=\"$disabled\"";
			else
				$this->disabled = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;
				
			$print = "\t<button type=\"$submit\"".$this->id.$this->onclick.$this->disabled.$this->other.">".$this->caption."</button>";

			if ($modal)
				$formbutton .= "\n$print";
			else {
				$this->print[] = $print;
			}
		}
		
		/**
		 * Cria Caixa de Memo
		 * @param $name [ string ]
		 * @param $caption [ string ]
		 * @param $obrigatory [ true | false ]
		 * @param $value [ string ]
		 * @param $cols [ int ]
		 * @param $disabled [ true | false ]
		 * @param $other [ string ]
		 */
		function Textarea($name, $caption, $obrigatory = false, $value = NULL, $rows = NULL, $disabled = false, $other = NULL) {
			if (!empty($name))
				$this->name = $name;
			else
				$this->name = NULL;
			
			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;
			
			if ($obrigatory)
				$this->obrigatory = " * ";
			else
				$this->obrigatory = NULL;
			
			if (!empty($value))
				$this->value = "$value";
			else
				$this->value = NULL;
			
			if (!empty($rows))
				$this->rows = "$rows";
			else
				$this->rows = "3";
			
			if ($disabled)
				$this->disabled = "disabled=\"$disabled\"";
			else
				$this->disabled = NULL;
			
			if (!empty($other))
				$this->other = $other;
			else
				$this->other = NULL;
				
			$this->print[] = "\t<div class=\"div-label\"><label class=\"label\" for=\"".$this->name."\">".$this->caption.": ".$this->obrigatory."</label></div>";
			$this->print[] = "\t<div class=\"div-textarea\"><textarea class=\"textarea\" type=\"".$this->type."\" name=\"".$this->name."\" id=\"".$this->name."\" rows=\"".$this->rows."\"".$this->disabled.$this->other.">".$this->value."</textarea></div>";

		}
		
		/**
		 * Cria Select
		 * @param $name [ string ]
		 * @param $caption [ string ]
		 * @param $array [ array ]: deve ser atribuido ('id' => 'caption')
		 * @param $compare [ 'string', 'array', '!string', '!array' ]
		 * @param $compare_value [ string ]
		 * @param $obrigatory [ 'true' | 'false' ]
		 * @param $maxlines [ int ]
		 * @param $multiple [ 'true' | 'false' ]
		 * @param $onchange [ string ]
		 * @param $disabled [ 'true' | 'false' ]
		 * @param $other [ string ]
		 * @param $boolean [ 'true' | 'false' ]
		 */
		function Select($name, $caption, $array, $compare = NULL, $compare_value = NULL, $obrigatory = false, $maxlines = NULL, $multiple = false, $onchange = NULL, $disabled = false, $other = NULL, $boolean = false) {
			if (!empty($name)) {
				$this->name = $name;
				$this->id = $name;
			} else {
				$this->name = NULL;
				$this->id = NULL;
			}
			
			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;
			
			if (!empty($array))
				$this->array = $array;
			else
				$this->array = NULL;
			
			if (!empty($compare))
				$this->compare = $compare;
			else
				$this->compare = NULL;
			
			if (!empty($compare_value)) {
				$this->compare_value = $compare_value;
			} else
				$this->compare_value = NULL;
			
			if ($obrigatory)
				$this->obrigatory = " * ";
			else
				$this->obrigatory = NULL;
			
			if (!empty($maxlines))
				$this->maxlines = " size=\"$maxlines\"";
			else
				$this->maxlines = NULL;
			
			if ($multiple) {
				$this->multiple = " multiple=\"multiple\"";
				$this->name .= "[]";
			} else
				$this->multiple = NULL;
			
			if ($onchange)
				$this->onchange = " onchange=\"".$onchange."\"";
			else
				$this->onchange = NULL;
			
			if ($disabled)
				$this->disabled = " disabled=\"disabled\"";
			else
				$this->disabled = NULL;
			
			if (!empty($other))
				$this->other = $other;
			else
				$this->other = NULL;



			if ($multiple) {
				if(!is_null($this->caption))
					$this->print[] = "\t<div class=\"div-label-multiple\"><label class=\"label\" for=\"".$this->id."\">".$this->caption.": ".$this->obrigatory."</label></div>";
					$this->print[] = "\t<div class=\"div-select-multiple\"><select class=\"select-multiple\" name=\"".$this->name."\" id=\"".$this->id."\"".$this->maxlines.$this->onchange.$this->multiple.$this->disabled.$this->other.">";
			} else {
				if(!is_null($this->caption))
					$this->print[] = "\t<div class=\"div-label\"><label class=\"label\" for=\"".$this->id."\">".$this->caption.": ".$this->obrigatory."</label></div>";
					$this->print[] = "\t<div class=\"div-select\"><select class=\"select\" name=\"".$this->name."\" id=\"".$this->id."\"".$this->maxlines.$this->onchange.$this->multiple.$this->disabled.$this->other.">";
			}

			if (is_array($array)) {

				if (!$boolean) {
					if (!$multiple)
						$this->print[] = "\t\t<option value=''>Selecione...</option>";
				}

                foreach($array as $tblArray) {
                	if (!is_null($this->compare_value)) {
						switch ($this->compare) {
							case "!string":
								if ($tblArray['id'] == $this->compare_value) {
									if (!$multiple) $this->print[] = "\t\t<option value=\"".$tblArray['id']."\" selected>".$tblArray['caption']."</option>";
								} else {
									$this->print[] = "\t\t<option value=\"".$tblArray['id']."\">".$tblArray['caption']."</option>";
								}
							break;
							
							case "string":
								if ($tblArray['id'] == $this->compare_value) {
									if (!$multiple) $this->print[] = "\t\t<option value=\"".$tblArray['id']."\" selected>".$tblArray['caption']."</option>";
								} else {
									$this->print[] = "\t\t<option value=\"".$tblArray['id']."\">".$tblArray['caption']."</option>";
								}
							break;

							case "array":
								if (in_array($tblArray['id'], $compare_value)) {
									if (!$multiple) $this->print[] = "\t\t<option value=\"".$tblArray['id']."\" selected>".$tblArray['caption']."</option>";
								} else {
									$this->print[] = "\t\t<option value=\"".$tblArray['id']."\">".$tblArray['caption']."</option>";
								}
							break;

							case "!array":
								if (!in_array($tblArray['id'], $compare_value)) {
									if (!$multiple) $this->print[] = "\t\t<option value=\"".$tblArray['id']."\" selected>".$tblArray['caption']."</option>";
								} else {
									$this->print[] = "\t\t<option value=\"".$tblArray['id']."\">".$tblArray['caption']."</option>";
								}
							break;
						}
					} else {
						$this->print[] = "\t\t<option value=\"".$tblArray['id']."\">".$tblArray['caption']."</option>";
					}
				}
			} else {
				if (!$multiple) $this->print[] = "\t\t<option>Nenhum valor retornado...</option>";
			}
			
			$this->print[] = "\t</select></div>";
		}
		
		/**
		 * Cria Grupo de Campos
		 * @param $id [ string ]
		 * @param $caption [ string ]
		 * @param $other [ string ]
		 */
		function Fieldset($id, $caption, $image = NULL, $other = NULL) {
			
			if (!empty($id))
				$this->id = $id;
			else
				$this->id = NULL;
			
			if (!empty($caption))
				$this->caption = $caption;
			else
				$this->caption = NULL;

			if (!empty($image))
				$this->image = " <img src=\"".getImage($image)."\" title=\"$this->caption\" align=\"absmiddle\"> ";
			else
				$this->image = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;
			
			$this->print[] = "\t<fieldset class=\"fieldset\" id=\"".$this->id."\"".$this->other.">";
			$this->print[] = "\t\t<legend class=\"legend\" id=\"leg_".$this->id."\">".$this->image.$this->caption."</legend>";
		}
		
		/**
		 * Finaliza Grupo de Campos
		 */
		function EndFieldset() {
			$this->print[] = "\t</fieldset>";
		}
		
		/**
		 * Cria Grupo de Campos
		 * @param $id [ string ]
		 * @param $other [ string ]
		 */
		function Div($id, $other = NULL) {
			
			if (!empty($id))
				$this->id = $id;
			else
				$this->id = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;
			
			$this->print[] = "\t<span id=\"".$this->id."\"".$this->other.">";
		}
		
		/**
		 * Finaliza Grupo de Campos
		 */
		function EndDiv() {
			$this->print[] = "\t</span>";
		}
		
		/**
		 * Cria Grupo de Campos
		 * @param $id [ string ]
		 * @param $context [ string ]
		 * @param $other [ string ]
		 */
		function Object($id, $context = NULL, $other = NULL) {
			
			if (!empty($id))
				$this->id = $id;
			else
				$this->id = NULL;
			
			if (!empty($other))
				$this->other = " $other";
			else
				$this->other = NULL;
			
			if (!empty($context))
				$this->context = "$context";
			else
				$this->context = NULL;
			
			$this->print[] = "\t<div id=\"".$this->id."\"".$this->other.">".$this->context."</div>";
		}
		
		/**
		 * Cria Grupo de Campos
		 * @param $id [ string ]
		 * @param $context [ string ]
		 * @param $other [ string ]
		 */
		function Null($context = NULL) {
			
			if (!empty($context))
				$this->context = "$context";
			else
				$this->context = NULL;
			
			$this->print[] = $this->context;
		}

		/**
		 * Cria Table Switch e Exibe a coluna esquerda
		 */
		function SwitchLeft($lenght = "45%") {
				
			$this->print[] = "\t<div class=\"div-table\"><table align=\"center\" class=\"switch-select\" cellpadding=\"0\" cellspacing=\"0\">\n";
			$this->print[] = "\t\t<tr>\n";
			$this->print[] = "\t\t\t<td class=\"switch-select-td\" style=\"width: $lenght;\">\n";
		}

		/**
		 * Exibe a coluna do meio da Table Switch
		 */
		function SwitchMiddle($lenght = "10%") {
			$this->print[] = "\t\t\t</td>\n";
			$this->print[] = "\t\t\t<td class=\"switch-select-middle\" style=\"width: $lenght; text-align: center;\">\n";
		}

		/**
		 * Exibe a coluna direita  da Table Switch
		 */
		function SwitchRight($lenght = "45%") {
			$this->print[] = "\t\t\t</td>\n";
			$this->print[] = "\t\t\t<td class=\"switch-select-td\" style=\"width: $lenght;\">\n";
		}

		/**
		 * Finaliza Table Switch
		 */
		function SwitchEnd() {
			$this->print[] = "\t\t\t</td>\n";
			$this->print[] = "\t\t</tr>\n";
			$this->print[] = "\t</table></div>\n";
		}

		/**
		 * Cria Grupo de Campos
		 * @param $id [ string ]
		 * @param $context [ string ]
		 * @param $other [ string ]
		 */
		function OpenInclude($file = NULL) {
			global $forminclude;

			if (!is_null($file))
				$forminclude = $file;
		}
		
		/**
		 * Finaliza Form e Imprime
		 */
		function Close() {
			global $formbutton;
			global $formclose;
			
			$closeform = "</form>";
			
			if (!is_null($formbutton)) {
				if ($this->active)
					$formclose = "\n$closeform";
					
			} else
				$this->active?$this->print[] = $closeform:NULL;
			
			if (is_array($this->print)) $print = join("\n",$this->print);
			if (isset($print))
				print $print;
		}
	}
	
?>