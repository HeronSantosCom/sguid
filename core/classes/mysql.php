<?php
    

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("mysql.php", "$url_check")) {
      header ("Location: /index.php");
    }
    
    /**
     * @author Heron Santos
     * @copyright 2008
     */

    /*
     * @class Conexão com Banco de Dados 
     */
    class MySQL {
        /** construtor */
        public function __construct() { }

/**
 * Example of documenting multiple possible datatypes for a given parameter
 * @param bool|string $foo sometimes a boolean, sometimes a string (or, could have just used "mixed")
 * @param bool|int $bar sometimes a boolean, sometimes an int (again, could have just used "mixed")
 */

		/**
		 * Conector
		 * @param string $hostname
		 * @param int $port
		 * @param string $username
		 * @param string $password
		 * @param string $name
		 */
        public function connect($hostname,$port,$username,$password,$name){
            $this->hostname = trim($hostname);
            $this->port = trim($port);
            $this->username = trim($username);
            $this->password = trim($password);
            $this->name = trim($name);
        }

        /** abre conexão com banco de dados */
        public function _OPEN() {
            $this->connection = mysql_connect("$this->hostname:$this->port",$this->username,$this->password);
            if (!$this->connection) {
                echo mysql_error();
                die();
            } elseif (!mysql_select_db($this->name,$this->connection)) {
                echo mysql_error();
                die();
            }
        }

        /** fecha conexão com banco de dados */
        public function _CLOSE() {
          return mysql_close($this->connection);
        }

        /** executa rotina em sql */
        public function getQuery($query) {
			debug_mysql("SQL: ".$query);
			
			$aux = explode(" ", $query);

            $this->_OPEN();
            if ($result = mysql_query($query)) {
				if (is_array($aux))
					if ($aux[0] == "insert") {
						$retornoid = mysql_insert_id();
						$this->_CLOSE();
						if ($retornoid) {
							return $retornoid;
							debug_mysql("INSERT: ". $retornoid);
						} else {
							return $result;
							debug_mysql("INSERT OK");
						}
					} else {
						$this->_CLOSE();
						return $result;
					}
            } else {
                echo mysql_error();
                die();
            }
        }

        /** executa rotina em sql */
        public function getFields($tabela) {
			debug_mysql("LIST: ".$tabela);
            $this->_OPEN();
            if ($result = mysql_list_fields($this->name, $tabela)) {
            	if (($fields = mysql_num_fields($result)) > 0) {
					for ($iCount = 0; $iCount <= $fields-1; $iCount++) {
						$tmp_return = mysql_field_name($result, $iCount);
						$return[] = $tmp_return;
					}
	                $this->_CLOSE();
					return $return;
            	} else {
	                $this->_CLOSE();
	                return false;
            	}
            } else {
                echo mysql_error();
                die();
            }
        }
        
        /** seleciona dados na tabela */
        public function querySelect($tabela,$colunas = " * ",$condicao = null,$ordenacao = null,$outros = null) {
            if (empty($colunas)) $colunas = "*";
            if (!empty($condicao)) $condicao = " where " . $condicao;
            if (!empty($ordenacao)) $ordenacao = " order by " . $ordenacao;
            if (!empty($outros)) $outros = " " . $outros;

            $sql = "select " . $colunas . " from " . $tabela . $condicao . $ordenacao . $outros;
            
			$fifo = $this->getQuery($sql);
            while ($result = mysql_fetch_array($fifo, MYSQL_ASSOC)) {
                $array[] = $result;
            }
            if (isset($array))
				return $array;
        	else 
        		return false;
        }
        
        /** insere dados na tabela */
        public function queryInsert($tabela,$valores,$outros = null) {
            if (!empty($outros)) $outros = " " . $outros;
            
            $sql = "insert into " . $tabela . " set " . $valores . $outros;
            
            $fifo = $this->getQuery($sql);
            if ($fifo) {
				return $fifo;
            } else
				return false;
        }
        
        /** atualiza dados na tabela */
        public function queryUpdate($tabela,$valores,$condicao = null,$outros = null) {
            if (!empty($condicao)) $condicao = " where " . $condicao;
            if (!empty($outros)) $outros = " " . $outros;
            
            $sql = "update " . $tabela . " set " . $valores . $condicao . $outros;
            
            $fifo = $this->getQuery($sql);
            if ($fifo)
				return true;
			else
				return false;
        }
        
        /** deleta dados na tabela */
        public function queryDelete($tabela,$condicao = null,$outros = null) {
            if (!empty($condicao)) $condicao = " where ".$condicao;
            if (!empty($outros)) $outros = " ".$outros;
            
            $sql = "delete from " . $tabela . $condicao . $outros;
			$sql_2 = "OPTIMIZE TABLE $tabela";

            
            $fifo = $this->getQuery($sql);
            if ($fifo) {
				$this->getQuery($sql_2);
				return true;
			} else {
				return false;
			}
        }

        /** retorna o total de registros */
        public function dbCount($tabela,$condicao = null) {
            if (!empty($condicao)) $condicao = " where ".$condicao;

			$sql = "select count(*) as total from " . $tabela . $condicao;
            
            $fifo = $this->getQuery($sql);
            $value = mysql_result($fifo, 0, "total");
            
            return $value;
        }

        /** retorna o total de registros */
        public function dbCheck($tabela,$condicao = null) {
            if (!empty($condicao)) $condicao = " where ".$condicao;

			$sql = "select count(*) as total from " . $tabela . $condicao;
            
            $fifo = $this->getQuery($sql);
            $value = mysql_result($fifo, 0, "total");
            
            if ($value == 0)
				return false;
			else
				return true;
        }

        /** retorna o total acumulativo */
        public function dbSum($tabela,$coluna,$condicao = null) {
            if (!empty($condicao)) $condicao = " where ".$condicao;
            
            $fifo = $this->getQuery("select sum(".$coluna.") as total from ".$tabela.$condicao);
            
			return mysql_result($fifo, 0, "total");
        }

        /** retorna o total acumulativo */
        public function dbMax($tabela,$coluna,$condicao = null) {
            if (!empty($condicao)) $condicao = " where ".$condicao;
            
            $fifo = $this->getQuery("select max(".$coluna.") as maximo from ".$tabela.$condicao);
            
			return mysql_result($fifo, 0, "maximo");
        }

        /** retorna o total acumulativo */
        public function dbAvg($tabela,$coluna,$condicao = null) {
            if (!empty($condicao)) $condicao = " where ".$condicao;
            
            $fifo = $this->getQuery("select avg(".$coluna.") as media from ".$tabela.$condicao);
            
			return mysql_result($fifo, 0, "media");
        }

        /** retorna o total de registros */
        public function getValue($tabela,$coluna,$condicao = null,$ordenacao = null,$outros = null) {

            $fifo = $this->querySelect($tabela,$coluna,$condicao,$ordenacao,$outros);
            if (is_array($fifo)) {
            	foreach ($fifo as $result) {
            		$value = $result["$coluna"];
            		return $value;
            	}
            } else
				return true;
				
        }
    }
    
?>