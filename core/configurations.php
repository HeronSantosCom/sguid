<?php
    $PATH = pathinfo(__file__);

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("configurations.php", "$url_check")) {
      header ("Location: /index.php");
    }

    /**
    * @author Heron Santos
    * @copyright 2008
    */

    session_start();
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
    header('Cache-Control: no-cache, must-revalidate');
    header('Pragma: no-cache');
    header("Content-Type: text/html; charset=utf-8",true);

    // variaveis globais
    define("SYSTEM_NAME", "SGUID Suite");
    define("SYSTEM_SYMBOL", "SGUID");
    define("SYSTEM_VERSION", "1");
    define("SYSTEM_FINALIZATION", "0");
    define("SYSTEM_CONTRUCTION", "0");
    define("SYSTEM_ORGANIZATION", "TELOSONLINE CONSULTORIA");
    define("SYSTEM_TITLE", SYSTEM_NAME . " v" . SYSTEM_VERSION . "." . SYSTEM_FINALIZATION . "." . SYSTEM_CONTRUCTION);
    define("SYSTEM_PATH", "/sguid");
    define("DEBUG_MYSQL", false);
    define("DEBUG_FRAMEWORK", false);

    define("FEDERACAO", 32);

    define("MYSQL_HOST", "localhost");
    define("MYSQL_PORT", 3306);
    define("MYSQL_USERNAME", "root");
    define("MYSQL_PASSWORD", "");
    define("MYSQL_DATABASENAME", "sguid");

    if(empty($_SERVER["DOCUMENT_ROOT"])) {
        $tmp_path = str_replace("/core", "", $PATH["dirname"]);
        define("SYSTEM_PATH_FILE", $tmp_path);
        unset($tmp_path);
    } else {
        define("SYSTEM_PATH_FILE", $_SERVER["DOCUMENT_ROOT"] . SYSTEM_PATH);
    }

    if(!empty($_SERVER["HTTP_HOST"])) {
        define("SYSTEM_PATH_FULL", "http://" . $_SERVER["HTTP_HOST"] . SYSTEM_PATH);
    
		define("FRAMEWORK_PATH_REFERENCES", "references/");
		define("FRAMEWORK_PATH_INCLUDES", "includes/");
		define("THEME_PATH_INCLUDES", SYSTEM_PATH . "/core/themes/");
		define("THEME_PATH_FILE_INCLUDES", SYSTEM_PATH_FILE . "/core/themes/");
		define("THEME_PATH_FULL_INCLUDES", SYSTEM_PATH_FULL . "/core/themes/");
		define("AUTHENTICATION_PATH", SYSTEM_PATH . "/framework/authentication.php?login=");
		
    }

    if(empty($_SERVER["REQUEST_URI"])){
        define("SYSTEM_PATH_CONTAINER", $PATH["dirname"]);
    } else {
        define("SYSTEM_PATH_CONTAINER", $_SERVER["REQUEST_URI"]);
    }

	// definidor de endereco do core
	include(SYSTEM_PATH_FILE . "/core/includes/getfiles.php");

	// debug
    include(getInclude("debug.php","file"));

    // debug
    include(getInclude("debug_mysql.php","file"));

    // arrays padrao
    $BOOL_STATUS[] = array("id" => 0, "caption" => "Desabilitado");
    $BOOL_STATUS[] = array("id" => 1, "caption" => "Habilitado");

    $BOOL_QUESTION[] = array("id" => 0, "caption" => "N&atilde;o");
    $BOOL_QUESTION[] = array("id" => 1, "caption" => "Sim");

    $BOOL_AFFIRMATIVE[] = array("id" => 0, "caption" => "Falso");
    $BOOL_AFFIRMATIVE[] = array("id" => 1, "caption" => "Verdadeiro");

	$TYPE_ALLOW[] = array("id" => "ulaw", "caption" => "ulaw");
	$TYPE_ALLOW[] = array("id" => "alaw", "caption" => "alaw");
	$TYPE_ALLOW[] = array("id" => "g729", "caption" => "g729");
	$TYPE_ALLOW[] = array("id" => "ilbc", "caption" => "ilbc");
	$TYPE_ALLOW[] = array("id" => "gsm", "caption" => "gsm");

	$TYPE_FORWARD[] = array("id" => "group", "caption" => "Grupo");
	$TYPE_FORWARD[] = array("id" => "local", "caption" => "Ramal");
	$TYPE_FORWARD[] = array("id" => "voicemail", "caption" => "Correio de Voz");
	$TYPE_FORWARD[] = array("id" => "queue", "caption" => "Fila");

	$TYPE_FORMAT_ADD_CHAN[] = array("id" => "unik", "caption" => "Individual");
	$TYPE_FORMAT_ADD_CHAN[] = array("id" => "sequence", "caption" => "Sequencial");

	$TYPE_DISPOSITION[] = array("id" => "ANSWERED", "caption" => "Atendida");
	$TYPE_DISPOSITION[] = array("id" => "BUSY", "caption" => "Ocupado");
	$TYPE_DISPOSITION[] = array("id" => "NO ANSWER", "caption" => "Não Atendida");
	$TYPE_DISPOSITION[] = array("id" => "FAILED", "caption" => "Falhou");

	$TYPE_LIMIT[] = array("id" => "20", "caption" => "20 registros");
	$TYPE_LIMIT[] = array("id" => "40", "caption" => "40 registros");
	$TYPE_LIMIT[] = array("id" => "60", "caption" => "60 registros");
	$TYPE_LIMIT[] = array("id" => "80", "caption" => "80 registros");
	$TYPE_LIMIT[] = array("id" => "100", "caption" => "100 registros");

	$TYPE_DAYWEEK[] = array("id" => "1", "caption" => "Domingo");
	$TYPE_DAYWEEK[] = array("id" => "2", "caption" => "Segunda");
	$TYPE_DAYWEEK[] = array("id" => "3", "caption" => "Terça");
	$TYPE_DAYWEEK[] = array("id" => "4", "caption" => "Quarta");
	$TYPE_DAYWEEK[] = array("id" => "5", "caption" => "Quinta");
	$TYPE_DAYWEEK[] = array("id" => "6", "caption" => "Sexta");
	$TYPE_DAYWEEK[] = array("id" => "7", "caption" => "Sábado");

    $MONTHS_YEAR = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');

    // instancia classe mysql
    include(getClasses("mysql.php","file"));
    $MYSQL = new MySQL();
    $MYSQL->connect(MYSQL_HOST,MYSQL_PORT,MYSQL_USERNAME,MYSQL_PASSWORD,MYSQL_DATABASENAME);

    // instancia classe nula
    class NullObject{}
	
    // instancia classe string
    include(getClasses("string.php","file"));
    $STRING = new String();

    // instancia classe file
    include(getClasses("file.php","file"));
	$FILE = new File();

    // carrega classe form
    include(getClasses("form.php","file"));

    // create dialog
    include(getInclude("createdialog.php","file"));

    // check macaddress
    include(getInclude("is_date.php","file"));

	// define variaveis do GET
    $GET = new NullObject();
    foreach ($_GET as $k => $v){
    	if (is_array($k))
      		$GET->$k = array($v);
    	else
      		$GET->$k = $STRING->str2db($v);
    }

	// define variaveis do POST
    $POST = new NullObject();
    foreach ($_POST as $k => $v){
    	if (is_array($k))
      		$POST->$k = array($v);
    	else
      		$POST->$k = $STRING->str2db($v);
    }

	// variaveis complexadas TOKEN
	if (isset($GET->token)) {
		$token = explode("|", $GET->token); //op: resgata token e desmembra variavel vindo por GET
	} elseif (isset($POST->token)) { //op1
		$token = explode("|", $POST->token); //op: resgata token e desmembra variavel vindo por POST
	} //op1

	//op2: se token existir
	if (isset($token)) {
		$TOKEN = new NullObject(); //op: instancia objeto nulo
		//op3: lista todos as variaves do token
		foreach ($token as $request) {
			unset($tmptoken); //op: remove temp
			$tmptoken = explode("/", $request); //op: separa indice do valor
			//op: se for array, descriptografa e insere na variavel
			if (is_array($tmptoken[0]))
				$TOKEN->$tmptoken[0] = $STRING->deCrypt(array($tmptoken[1]));
			else
				$TOKEN->$tmptoken[0] = $STRING->deCrypt($tmptoken[1]);
		}
	}

	/**
	 * Autenticador
	 */
    // declara variaveis
    if (!isset($GET->authentication)) $GET->authentication = NULL;
    if (!isset($GET->login)) $GET->login = NULL;
    if (!isset($POST->action)) $POST->action = NULL;
    if (!isset($authenticate_alert)) $authenticate_alert = NULL;

	if ($GET->login == md5("logoff")) {
    	debug("efetuando logoff do usuario");
        unset($_SESSION["username"]);
        unset($_SESSION["password"]);
        $GET->authentication = "!authenticated";
		$authenticate_alert = "logoff";
	} elseif ($GET->login == md5("expired")) {
    	debug("sessao do usuario expirado");
        unset($_SESSION["username"]);
        unset($_SESSION["password"]);
        $GET->authentication = "!authenticated";
		$authenticate_alert = "expired";
	}

    if($POST->action == md5("login")) {
        debug("commit do usuario");
        $pstAuthusername = $STRING->crypTo($POST->authusername);
        $pstAuthpassword = $STRING->crypTo($POST->authpassword);

        $qryUseraccount = $MYSQL->querySelect("`useraccount`",0,"username = '$pstAuthusername' and password = '$pstAuthpassword'");
        if (is_array($qryUseraccount)){
            debug("usuario encontrado");
            foreach($qryUseraccount as $tblUseraccount) {
                debug("session gerado");
                $_SESSION["username"] = $STRING->db2str($tblUseraccount["username"]);
                $_SESSION["password"] = $STRING->db2str($tblUseraccount["password"]);
                $authenticate_alert = "authenticated";
            }
        } else {
        	debug("usuario nao localizado");
            unset($_SESSION["username"]);
            unset($_SESSION["password"]);
	        $GET->authentication = "!authenticated";
			$authenticate_alert = "notfound";
        }
    }

    if (isset($_SESSION["username"]) && isset($_SESSION["password"])) {
        debug("session com usuario registrado");
        $pstAuthusername = $_SESSION["username"];
        $pstAuthpassword = $_SESSION["password"];
        $qryUseraccount = $MYSQL->querySelect("`useraccount`",0,"username = '$pstAuthusername' and password = '$pstAuthpassword'");
        if (is_array($qryUseraccount)){
            debug("usuario encontrado");
            foreach($qryUseraccount as $tblUseraccount) {
                debug("declarando seus dados");
                define("USER_ID", $STRING->db2str($tblUseraccount["iduseraccount"]));
                define("USER_LEVELACCESS", $STRING->db2str($tblUseraccount["idcbx_levelaccess"]));
                define("USER_CAPTION", $STRING->db2str($tblUseraccount["name"]));
                define("USER_USERNAME", $STRING->db2str($tblUseraccount["username"]));
                define("USER_PASSWORD", $STRING->db2str($tblUseraccount["password"]));
            	$GET->authentication = "authenticated";
            }
        } else {
        	debug("nenhum usuario encontrado");
            unset($_SESSION["username"]);
            unset($_SESSION["password"]);
	        $GET->authentication = "!authenticated";
			$authenticate_alert = "notfound";
        }
    } else {
    	debug("session nao retornou usuario");
        $GET->authentication = "!authenticated";
    }

    if ($GET->authentication == "!authenticated")
    	define("USER_CAPTION", "N&atilde;o Autenticado...");
  
?>