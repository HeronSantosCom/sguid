// insere na lista fifo
function toggle_checkbox(theId, theList, rtnList, thisObjId) {
	//alert("estou aki: " + theId + " - " + theList);
	
	// define campos a trabalhar
	var aId = "a_" + theId;
	var aObj = document.getElementById(aId);
	
	var trId = "tr_" + theId;
	var trObj = document.getElementById(trId);
	
	var chkId = "chk_" + theId;
	var chkObj = document.getElementById(chkId);
	
	// declara lista e lista tamanho
	var listObj = document.getElementById(theList);
	var listLen = listObj.length;
		
	// altera o estado do checkbox
	if (thisObjId != chkId)
		if (chkObj.checked == false)
			chkObj.checked = true;
		else
			chkObj.checked = false;

	if (chkObj.checked == true) { // se o objeto checkbox nao estiver marcado
		//alert("estou aki: " + theId + " - " + theList);
		var ListNotFound = false;
		var iCount = 0;
		
		if (listLen > 0) { // se conter item na lista
		
			while (iCount < listLen) { // percorre toda lista
				if (rtnList == "text") {
					var listValue = listObj.options[iCount].text;
				} else {
					var listValue = listObj.options[iCount].value;
				}

				if (listValue != chkObj.value) { // se o valor do item da lista atual for igual ao checkbox
					ListNotFound = true; // option nao encontrado
				} else {
					ListNotFound = false; // option encontrado
					break; // encerra varredura
				}
				
				iCount++; // incrementa contador da lista
				
			}
			
		} else { // nao conter item na lista
			ListNotFound = true; // option nao encontrado
		}

		if (ListNotFound == true) { // se option nao encontrado
			addList(listObj, theId, chkObj.value); // acrescentar na lista
		}
		
		trObj.className = "hover";
	} else { // se o objeto checkbox estiver marcado
		var ListFound = true;
		var iCount = 0;
		var listIndex = null;
		
		if (listLen > 0) { // se conter item na lista
		
			while (iCount < listLen) { // percorre toda lista
				if (rtnList == "text") {
					var listValue = listObj.options[iCount].text;
				} else {
					var listValue = listObj.options[iCount].value;
				}

				if (listValue != chkObj.value) { // se o valor do item da lista atual for igual ao checkbox
					ListFound = false; // option nao encontrado
				} else {
					ListFound = true; // option encontrado
					var listIndex = iCount; // identifica o option a ser removido
					break; // encerra varredura
				}
				
				iCount++; // incrementa contador da lista
			}
			
		}

		if (ListFound == true) { // se option for encontrado
			remList(listObj, listIndex);
		}
		
		trObj.className = "";
	}
	
}

// manipula todos os itens
function toggle_all_checkbox(theList, rtnList, thisId) {
	// define campos a trabalhar
	var chkallObj = document.getElementById('chkall');
	var chkElemName = document.getElementsByName('chk');

	if (thisId == 'chkall') {
		if (chkallObj.checked == true) { // se o checkbox all estiver marcado
			chkallObj.checked = false; // seleciona todos os checkbox
		} else {
			chkallObj.checked = true; // desmarca todos os checkbox
		}
	}

	if (chkallObj.checked == true) { // se o checkbox all estiver marcado
		checkedAllChk(chkElemName, theList, rtnList); // seleciona todos os checkbox
	} else {
		uncheckedAllChk(chkElemName, theList, rtnList); // desmarca todos os checkbox
	}
}

// insere na lista
function addList(theObj, theText, theValue) {
	var newOpt = new Option(theValue, theText);
	var selectLen = theObj.length;
	theObj.options[selectLen] = newOpt;
}

// remove da lista
function remList(theObj, theIndex) {
	var selectLen = theObj.length;
	if (selectLen > 0)
		theObj.options[theIndex] = null;
}

// marca todos os itens
function checkedAllChk(chkElemName, theList, rtnList) {
	for (iCount = 0; iCount < chkElemName.length; iCount++) { // percorre todos os checkbox`s
		var xCount = iCount + 1; //incrementa o contador para equilibar
		
		var chkId = "chk_" + xCount; //define o id do checkbox
		var chkObj = document.getElementById(chkId); // define o objeto do checkbox
		var theId = chkObj.id.replace(/chk_/, "");
		
		chkObj.checked = 0; // desmarca o checkbox
		//alert("checkedAllChk: " + theId + " - lista: - " + theList + " - retorno: - " + rtnList);
		
		toggle_checkbox(theId, theList, rtnList);
	}
}

// desmarca todos os itens
function uncheckedAllChk(chkElemName, theList, rtnList) {
	for (iCount = 0; iCount < chkElemName.length; iCount++) { // percorre todos os checkbox`s
		var xCount = iCount + 1; //incrementa o contador para equilibar
		
		var chkId = "chk_" + xCount; //define o id do checkbox
		var chkObj = document.getElementById(chkId); // define o objeto do checkbox
		var theId = chkObj.id.replace(/chk_/, "");
		
		chkObj.checked = 1; // desmarca o checkbox
		//alert("uncheckedAllChk: " + theId + " - lista: - " + theList + " - retorno: - " + rtnList);
		
		toggle_checkbox(theId, theList, rtnList);
	}
	
}