var NS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) < 5);

function toggle_option(SelFrom, SelTo, SelTotal) {
	var SelFromObj = SelFrom;
	var SelToObj = SelTo;

	var SelFromLength = SelFromObj.length;
	var SelToLength = SelToObj.length;

	var SelectedText = new Array();
	var SelectedValues = new Array();
	var SelectedCount = 0;
	var iCount;

	// Find the selected Options in reverse order
	// and delete them from the 'from' Select.
	for (iCount = SelFromLength-1; iCount >= 0; iCount--) {
		if (SelFromObj.options[iCount].selected) {
			SelectedText[SelectedCount] = SelFromObj.options[iCount].text;
			SelectedValues[SelectedCount] = SelFromObj.options[iCount].value;
			deleteOption(SelFromObj, iCount);
			SelectedCount++;
		}
	}

	// Add the selected text/values in reverse order.
	// This will add the Options to the 'to' Select
	// in the same order as they were in the 'from' Select.
	for (iCount = SelectedCount-1; iCount >= 0; iCount--) {
		if (SelTotal > 0) {
			if (SelToLength < SelTotal) {
				addOption(SelToObj, SelectedText[iCount], SelectedValues[iCount]);
				SelToLength++;
			} else
				alert("Lista Cheia!");
		} else
			addOption(SelToObj, SelectedText[iCount], SelectedValues[iCount]);
	}

	if(NS4) history.go(0);
}

function toggle_all_option(theName) {
	theObj = document.getElementsByName(theName);
	for (var iCount = 0; iCount < theObj[0].length; iCount++) {
		theObj[0].options[iCount].selected = true;
	}
}

function addOption(theSel, theText, theValue) {
	var newOpt = new Option(theText, theValue);
	var selLength = theSel.length;
	theSel.options[selLength] = newOpt;
}

function deleteOption(theSel, theIndex) { 
	var selLength = theSel.length;
	if (selLength>0) {
		theSel.options[theIndex] = null;
	}
}