/***********************************************
* Dynamic Ajax Content- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
var bustcachevar = 1; //bust potential caching of external pages after initial request? (1=yes, 0=no)
var loadedobjects = "";
var rootdomain = "http://" + window.location.hostname;
var bustcacheparameter = "";

function ajaxpage(url, containerid, display_status) {
	var page_request = false;
	if (window.XMLHttpRequest) // if Mozilla, Safari etc
		page_request = new XMLHttpRequest();
	else if (window.ActiveXObject) { // if IE
		try {
			page_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				page_request = new ActiveXObject("Microsoft.XMLHTTP");
			}
				catch (e) {}
		}
	} else                                         
		return false;
	
	page_request.onreadystatechange = function() {
		loadpage(page_request, containerid, display_status);
	}

	if (bustcachevar) //if bust caching of external page
		bustcacheparameter = (url.indexOf("?")!=-1) ? "&"+new Date().getTime() : "?" + new Date().getTime();
	page_request.open('GET', url + bustcacheparameter, true);
	page_request.send(null);
}                                             

function loadpage(page_request, theId, display_status) {
	var httpStatus = page_request.readyState;

	if (httpStatus == 4 && (page_request.status == 200 || window.location.href.indexOf("http") == -1)) {
		var src = page_request.responseText;
		var src_js = extract_js(src);
		var bytecode = src;
		if (src_js) {
			bytecode = bytecode + src_js;
		}
		document.getElementById(theId).innerHTML = bytecode;
	} else {
		if (display_status == false) {
			document.getElementById(theId).innerHTML = "";
		} else {
			switch (httpStatus) {
				case 0:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>Erro desconhecido de javascript...</div>";
				break;
				case 1:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"hourglass.png' align='absmiddle'>Carregando...</div>";
				break;
				case 400:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>400: Solicita��o incompreens�vel...</div>";
				break;
				case 404:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>404: N�o foi encontrada a URL solicitada...</div>";
				break;
				case 405:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>405: O servidor n�o suporta o m�todo solicitado...</div>";
				break;
				case 500:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>500: Erro desconhecido de natureza do servidor...</div>";
				break;
				case 503:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>503: Capacidade m�xima do servidor alcan�ada...</div>";
				break;
				/*default:
					document.getElementById(theId).innerHTML = "<div id='boxmessage'><img src='"+ ImagePath +"error.png' align='absmiddle'>Erro " + httpStatus + ".<br>Mais informa��es em http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html</div>";
				break;*/
			}
		}
	}
}


function extract_js(texto) {
	var ini = 0;
	while (ini!=-1) { // loop enquanto achar um script
		ini = texto.indexOf('<script', ini); // procura uma tag de script
		if (ini >=0) { // se encontrar
			ini = texto.indexOf('>', ini) + 1; // define o inicio para depois do fechamento dessa tag
			var fim = texto.indexOf('</script>', ini); // procura o final do script
			codigo = texto.substring(ini,fim); // extrai apenas o script
			novo = document.createElement("script")
			novo.text = codigo;
			document.body.appendChild(novo);
		}
	}
}