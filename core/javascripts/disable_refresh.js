var msg = 'Função Desativada!';
var asciiF5 = 116;
var asciiALT = 18;
var asciiLeftArrow = 37;
var asciiRightArrow = 39;

if (document.all) { //ie has to block in the key down
	document.onkeydown = onKeyPress;
} else if (document.layers || document.getElementById) { //NS and mozilla have to block in the key press
	document.onkeypress = onKeyPress;
}

function onKeyPress(evt) {
	window.status = '';
	var oEvent = (window.event) ? window.event : evt;
	var nKeyCode = oEvent.keyCode ? oEvent.keyCode : oEvent.which ? oEvent.which : void 0;
	var bIsFunctionKey = false;
	if (oEvent.charCode == null || oEvent.charCode == 0) {
		bIsFunctionKey = (nKeyCode == asciiF5)||(nKeyCode == asciiALT)
	}
	var sChar = String.fromCharCode(nKeyCode).toUpperCase();
	var oTarget = (oEvent.target) ? oEvent.target : oEvent.srcElement;
	var sTag = oTarget.tagName.toLowerCase();
	var sTagType = oTarget.getAttribute("type");
	var bAltPressed = (oEvent.altKey) ? oEvent.altKey : oEvent.modifiers & 1 > 0;
	var bRet = true;
	if(sTagType != null) {
		sTagType = sTagType.toLowerCase();
	}
	if (bIsFunctionKey) {
		bRet = false;
	} else if (bAltPressed && (nKeyCode == asciiLeftArrow || nKeyCode == asciiRightArrow)) {
		bRet = false;
	}
	if (!bRet) {
		try {
			oEvent.returnValue = false;
			oEvent.cancelBubble = true;
			if (document.all) {
				oEvent.keyCode = 0;
			} else {
				oEvent.preventDefault();
				oEvent.stopPropagation();
			}
		} catch(ex) { }
	}
	return bRet;
}