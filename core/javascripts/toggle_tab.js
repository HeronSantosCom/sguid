function toggle_tab(nome_aba, id_aba, n_aba){
	// manipula aba
	var AbaSelObj = document.getElementById(id_aba);
	for (var iCount = '1'; iCount <= n_aba; iCount++) {
		var AbaHideId = nome_aba + '-aba' + iCount;
		document.getElementById(AbaHideId).className = 'input-aba-hide';
	}
	AbaSelObj.className = "input-aba";

	// manipula div
	var TextShowId = 'text-' + id_aba;
	var TextSelObj = document.getElementById(TextShowId);
	for (var uCount = '1'; uCount <= n_aba; uCount++) {
		var TextHideId = 'text-' + nome_aba + '-aba' + uCount;
		document.getElementById(TextHideId).style.display = "none";
	}
	TextSelObj.style.display = "block";
}