<?php

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("is_ipaddress.php", "$url_check")) {
      header ("Location: /index.php");
    }

	/*
	* Checa IP
	*/
	function is_ipaddress($checkip, $jolly_char='')
	{
		if ($jolly_char=='.')        // dot ins't allowed as jolly char
			$jolly_char = '';

		if ($jolly_char!='') {
			$checkip = str_replace($jolly_char, '*', $checkip);        // replace the jolly char with an asterisc
			$my_reg_expr = "^[0-9\*]{1,3}\.[0-9\*]{1,3}\.[0-9\*]{1,3}\.[0-9\*]{1,3}$";
			$jolly_char = '*';
		}
		else
			$my_reg_expr = "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$";

		if (eregi($my_reg_expr, $checkip)) {
			for ($i = 1; $i <= 3; $i++) {
				if (!(substr($checkip, 0, strpos($checkip, ".")) >= "0" && substr($checkip, 0, strpos($checkip, ".")) <= "255")) {
					if ($jolly_char!='') {            // if exists, check for the jolly char
						if (substr($checkip, 0, strpos($checkip, "."))!=$jolly_char)
							return false;
					}
					else
						return false;
				}

				$checkip = substr($checkip, strpos($checkip, ".") + 1);
			}

			if (!($checkip >= "0" && $checkip <= "255")) {        // class D
				if ($jolly_char!='') {            // if exists, check for the jolly char
					if ($checkip!=$jolly_char)
						return false;
				}
				else
					return false;
			}
		}
		else
			return false;

		return true;
	}

?>