<?php

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("createdialog.php", "$url_check")) {
      header ("Location: /index.php");
    }

    /**
     * @author Heron Santos e Marcus Coelho
     * @copyright 2008
     */

	/**
	 * Cria caixa de dialogo
	 * @param $method modal, alert
	 * @param $type tipo de dialogo (warning, information, nonpemission, question)
	 * @param $title titulo do dialogo <string>
	 * @param $message mensagem do dialogo <string>
	 * @param $return retorno (null = exit, default = fecha modulo)
	 * @param $return_aux retorno auxiliar
	 */
	function CreateDialog($method, $title, $message, $return = NULL, $return_aux = NULL, $target = "container") {
		$errSintaxe = false;
		empty($title)?$errSintaxe == true:NULL;
		empty($message)?$errSintaxe == true:NULL;
		
		if (!is_null($return) or !is_null($return_aux))
			$tempDialog[] = "<script type=\"text/javascript\" src=\"".getJavaScript("get_open.js")."\"></script>";
		
		// metodo modal
		if ($method == "modal" and !empty($return)) {
			$tempDialog[] = '<script type="text/javascript">';
			
			$tempDialog[] = "\t" . 'agree = confirm("'.$title.'\n'.$message.'");';
			$tempDialog[] = "\t" . 'if (agree) {';
			if (!empty($return)) {
				$tempDialog[] = "\t\t" . 'getOpen("'.$return.'","'.$target.'");';
			}
			if (!empty($return_aux)) {
				$tempDialog[] = "\t" . '} else {';
				$tempDialog[] = "\t\t" . 'getOpen("'.$return_aux.'","'.$target.'");';
			}
			$tempDialog[] = "\t" . '}';
			$tempDialog[] = '</script>';
		// metodo alert
		} elseif ($method == "alert") {
			$tempDialog[] = '<script type="text/javascript">';
			$tempDialog[] = "\t" . 'alert("'.$title.'\n'.$message.'");';
			
			if (!empty($return)) {
				$tempDialog[] = "\t" . 'getOpen("'.$return.'","'.$target.'");';
			}

			$tempDialog[] = '</script>';
		// erro
		} else
			$errSintaxe = true;
		
		if (is_array($tempDialog)) $dialog = join("\n",$tempDialog);
		
		// erro de sintaxe
		if ($errSintaxe) {
			print("\n<h1>ERRO AO CRIAR CAIXA DE DIALOGO</h1>");
			exit;
		} else
			return print $dialog;
	}
?>