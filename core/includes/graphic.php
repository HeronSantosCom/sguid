<?php

    /**
    * @author Heron Santos
    * @copyright 2008
    */
    include("../configurations.php");
    
    /**
    * Carrega Criador de Graficos
    */
    include(SYSTEM_PATH_FILE."/core/plugins/phplot/phplot.php");
    
    // declara variaveis
    $calldate_hour = NULL;
    $tmp_calldate = NULL;

	if (isset($GET->where)) {
		$qry_where = $STRING->deCrypt($GET->where);
	} else {
		$qry_where = NULL;
	}

	if (isset($GET->resolucao)) {
		$img_resolucao = $GET->resolucao;
	} else {
		$img_resolucao = NULL;
	}

	if (!is_null($img_resolucao)) {
		debug("Resolucao: $img_resolucao");
		$img_resolucao = explode("x", $img_resolucao);
		
		$img_resolucao_x = $img_resolucao[0];
		$img_resolucao_y = $img_resolucao[1];
		
		//print $img_resolucao_x - ($img_resolucao_x*0.01);
		//print $img_resolucao_y - ($img_resolucao_y*0.01);

		$img_resolucao_x = $img_resolucao_x - ($img_resolucao_x*0.02);
		$img_resolucao_y = $img_resolucao_y - ($img_resolucao_y*0.03);

		debug("Resolucao X: $img_resolucao_x");
		debug("Resolucao Y: $img_resolucao_y");
		
		//print_r($img_resolucao);
		switch ($GET->type) {

			/*
			 * Grafico de chamadas por hora
			 */
			case "com-call2hour":
				$xNum = -1;
				$uHora = 0;

				$qry_cdr = $MYSQL->querySelect("`cdr`","date(calldate) as calldate_day","$qry_where","","group by calldate_day");
				foreach($qry_cdr as $tbl_cdr) {
					$tmp_calldate_day = explode('-',$tbl_cdr["calldate_day"]);
					$calldate_day[] = date('d/m/Y', mktime(0, 0, 0, $tmp_calldate_day[1], $tmp_calldate_day[2], $tmp_calldate_day[0]));
				}

				//print_r($qry_where);

				$qry_cdr = $MYSQL->querySelect("`cdr`","date(calldate) as calldate_day, hour(calldate) as calldate_hour, count(calldate) as calldate_count","$qry_where","","group by calldate_hour, calldate_day");
				foreach($qry_cdr as $tbl_cdr) {
					$calldate_day_2 = strtotime($tbl_cdr["calldate_day"]);
					$count_calldate = $tbl_cdr["calldate_count"];

					if ($uHora != $tbl_cdr["calldate_hour"]) {
						$uHora = $calldate_hour = $tbl_cdr["calldate_hour"];
						$tmp_calldate[$calldate_hour][] = date('H', mktime($calldate_hour, 0, 0, 0, 0, 0)) . ":00";
						$xNum++;
					}

					(!empty($calldate_hour) and $xNum >= 0)?$tmp_calldate[$calldate_hour][$calldate_day_2] = $count_calldate:NULL;

					is_array($tmp_calldate[$calldate_hour])?$calldate[$xNum] = $tmp_calldate[$calldate_hour]:NULL;
				}
				//print_r($calldate);

				//Define o objeto
				$graph = new PHPlot($img_resolucao_x, $img_resolucao_y);
				$graph->SetDataType('text-data');

				$graph->SetTitle("Chamadas / Hora");

				$graph->SetLegend($calldate_day);
				$graph->SetDataValues($calldate);

				$graph->SetDataColors(array('DarkGreen','DimGrey','PeachPuff','SkyBlue','SlateBlue','YellowGreen','aquamarine1','azure1','beige','black','blue','brown','cyan','gold','gray','green','grey','ivory','lavender','magenta','maroon','navy','orange','orchid','peru','pink','plum','purple','red','salmon','snow','tan','violet','wheat','yellow'));
				//$graph->SetRGBArray('large');

				$graph->SetXTickLabelPos('none');
				$graph->SetXTickPos('none');

				$graph->SetDrawXGrid(True);
				$graph->SetDrawYGrid(True);

				$graph->SetPlotAreaWorld(NULL, 0, NULL, NULL);

				$graph->SetYLabelType('data');
				$graph->SetPrecisionY(0);

				# Y Axis title:
				$graph->SetYTitle("Chamadas");

				$graph->DrawGraph();
			break;

			/*
			 * Grafico de minutos por hora
			 */
			case "com-min2hour":
				$xNum = -1;
				$uHora = 0;

				$qry_cdr = $MYSQL->querySelect("`cdr`","date(calldate) as calldate_day","$qry_where","","group by calldate_day");
				foreach($qry_cdr as $tbl_cdr) {
					$tmp_calldate_day = explode('-',$tbl_cdr["calldate_day"]);
					$calldate_day[] = date('d/m/Y', mktime(0, 0, 0, $tmp_calldate_day[1], $tmp_calldate_day[2], $tmp_calldate_day[0]));
				}

				//print_r($qry_where);

				$qry_cdr = $MYSQL->querySelect("`cdr`","date(calldate) as calldate_day, hour(calldate) as calldate_hour, sum(time(billsec)) as billsec","$qry_where","","group by calldate_hour, calldate_day");
				foreach($qry_cdr as $tbl_cdr) {
					$calldate_day_2 = strtotime($tbl_cdr["calldate_day"]);
					$billsec = sprintf("%02d",intval($tbl_cdr["billsec"]/60));

					if ($uHora != $tbl_cdr["calldate_hour"]) {
						$uHora = $calldate_hour = $tbl_cdr["calldate_hour"];
						$tmp_calldate[$calldate_hour][] = date('H', mktime($calldate_hour, 0, 0, 0, 0, 0)) . ":00";
						$xNum++;
					}

					(!empty($calldate_hour) and $xNum >= 0)?$tmp_calldate[$calldate_hour][$calldate_day_2] = $billsec:NULL;

					is_array($tmp_calldate[$calldate_hour])?$calldate[$xNum] = $tmp_calldate[$calldate_hour]:NULL;
				}
				//print_r($calldate);

				//Define o objeto
				$graph = new PHPlot($img_resolucao_x, $img_resolucao_y);
				$graph->SetDataType('text-data');

				$graph->SetTitle("Duracao / Hora");

				$graph->SetLegend($calldate_day);
				$graph->SetDataValues($calldate);

				$graph->SetDataColors(array('DarkGreen','DimGrey','PeachPuff','SkyBlue','SlateBlue','YellowGreen','aquamarine1','azure1','beige','black','blue','brown','cyan','gold','gray','green','grey','ivory','lavender','magenta','maroon','navy','orange','orchid','peru','pink','plum','purple','red','salmon','snow','tan','violet','wheat','yellow'));

				$graph->SetXTickLabelPos('none');
				$graph->SetXTickPos('none');

				$graph->SetDrawXGrid(True);
				$graph->SetDrawYGrid(True);

				$graph->SetPlotAreaWorld(NULL, 0, NULL, NULL);

				$graph->SetYLabelType('data');
				$graph->SetPrecisionY(0);

				# Y Axis title:
				$graph->SetYTitle("Duracao");

				$graph->DrawGraph();
			break;

			/*
			 * Grafico de Carga Diaria
			 */
			case "carga":
				/*$qry_cdr = $MYSQL->querySelect("`cdr`","date(calldate) as calldate_day","$qry_where","","group by calldate_day");
				foreach($qry_cdr as $tbl_cdr) {
					$tmp_calldate_day = explode('-',$tbl_cdr["calldate_day"]);
					$calldate_day[] = date('d/m/Y', mktime(0, 0, 0, $tmp_calldate_day[1], $tmp_calldate_day[2], $tmp_calldate_day[0]));
				}*/

				/*for ($xHour=0;$xHour<=24;$xHour++) {
					$calldate_hour = $xHour;
					$tmp_calldate[$calldate_hour][] = date('H', mktime($calldate_hour, 0, 0, 0, 0, 0)) . ":00";
					$tmp_calldate[$calldate_hour][] = $MYSQL->dbCount("cdr", "$qry_where and hour(calldate) = $calldate_hour");

					//$MYSQL->getValue("`cdr`","count(calldate)","$qry_where and hour(calldate) = $calldate_hour");

					$calldate[] = $tmp_calldate[$calldate_hour];
				}*/

				$calldate = $MYSQL->querySelect("`cdr`","hour(calldate)as calldate_hour, count(calldate) as calldate_count",$qry_where,"","group by calldate_hour order by calldate_hour asc");

				//print_r($calldate);

				//Define o objeto
				$graph = new PHPlot($img_resolucao_x, $img_resolucao_y);
				$graph->SetDataType('text-data');

				$graph->SetTitle("Carga Diaria");

				//$graph->SetLegend($calldate_day);
				$graph->SetDataValues($calldate);

				$graph->SetPlotType('bars');
				$graph->SetShading(0);
				$graph->SetYDataLabelPos('plotin');

				//$graph->SetDataColors(array('DarkGreen','DimGrey','PeachPuff','SkyBlue','SlateBlue','YellowGreen','aquamarine1','azure1','beige','black','blue','brown','cyan','gold','gray','green','grey','ivory','lavender','magenta','maroon','navy','orange','orchid','peru','pink','plum','purple','red','salmon','snow','tan','violet','wheat','yellow'));

				$graph->SetXTickLabelPos('none');
				$graph->SetXTickPos('none');

				$graph->SetDrawXGrid(False);
				$graph->SetDrawYGrid(True);

				$graph->SetPlotAreaWorld(NULL, 0, NULL, NULL);

				$graph->SetYLabelType('data');
				$graph->SetPrecisionY(0);

				# Y Axis title:
				$graph->SetYTitle("Chamadas");

				$graph->DrawGraph();
			break;

			/*
			 * Grafico de Trafego
			 */
			case "trafego":
				$qry_cdr = $MYSQL->querySelect("`cdr`","year(calldate) as calldateYear, month(calldate) as calldateMonth, count(calldate) as calldate_count","$qry_where","","group by calldateYear, calldateMonth");
				foreach($qry_cdr as $tbl_cdr) {
					$calldate_day = $MONTHS_YEAR[$tbl_cdr["calldateMonth"]]."/".$tbl_cdr["calldateYear"];
					$calldate[] = array($calldate_day, $tbl_cdr["calldate_count"]);
				}
				//print_r($calldate);

				//Define o objeto
				$graph = new PHPlot($img_resolucao_x, $img_resolucao_y);
				$graph->SetPlotType('pie');
				$graph->SetDataType('text-data-single');

				$graph->SetTitle("Trafego Mensal\nChamadas / Mes");

				foreach ($calldate as $row) $graph->SetLegend(implode(': ', $row));
				$graph->SetDataValues($calldate);

				$graph->SetDataColors(array('DarkGreen','DimGrey','PeachPuff','SkyBlue','SlateBlue','YellowGreen','aquamarine1','azure1','beige','black','blue','brown','cyan','gold','gray','green','grey','ivory','lavender','magenta','maroon','navy','orange','orchid','peru','pink','plum','purple','red','salmon','snow','tan','violet','wheat','yellow'));

				$graph->SetXTickLabelPos('none');
				$graph->SetXTickPos('none');

				$graph->SetPlotAreaWorld(NULL, 0, NULL, NULL);

				$graph->SetYLabelType('data');
				$graph->SetPrecisionY(0);

				$graph->DrawGraph();
			break;
		}
	}

?>