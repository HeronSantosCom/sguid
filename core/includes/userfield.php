<?php

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("userfield.php", "$url_check")) {
      header ("Location: /index.php");
    }

	/**
	 * @author Marcus Vinícius
	 * @copyright 2008
	 */

	/*
	* Vefifica se a chamada veio de um encaminhamento
	*/
	function is_forward($userfield) {
		if (!empty($userfield)) {
			$data = explode("|", $userfield);
			if (is_array($data)) {
				foreach ($data as $tmp_data) {
					$aux_data = explode("=", $tmp_data);
					if (!empty($aux_data[0]) and !empty($aux_data[1])) {
						switch ($aux_data[0]) {
							case 'CF':
								return $aux_data[1];
								break;
							case 'CFF':
								return $aux_data[1];
								break;
							case 'CFU':
								return $aux_data[1];
								break;
							case 'CFB':
								return $aux_data[1];
								break;
						}
					} else
						return false;
				}
			} else
				return false;
		} else
			return false;
	}


	/*
	* Vefifica se a chamada foi monitorada
	*/
	function is_audio($userfield)
	{
		if (!empty($userfield)) {
			$data = explode("|", $userfield);
			if (is_array($data)) {
				foreach ($data as $tmp_data) {
					$aux_data = explode("=", $tmp_data);
					if (!empty($aux_data[0]) and !empty($aux_data[1])) {
						switch ($aux_data[0]) {
							case 'MONITOR':
								return $aux_data[1];
								break;
						}
					} else
						return false;
				}
			} else
				return false;
		} else
			return false;
	}

?>