<?php

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("getfiles.php", "$url_check")) {
      header ("Location: /index.php");
    }

    /**
    * @author Heron Santos
    * @copyright 2008
    */

	/**
	 * Funcao de Carregar Imagem
	 * @var path is (file, full, null)
	 */
    function getImage($file, $path = null) {
    	if (is_null($file))
			$file = "spacer.gif";
    	else
    		$file = "$file";
    	
    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;
    	
    	$file = $path . "/core/imagens/" . $file;
    	
    	return $file;
    }

	/**
	 * Funcao de Carregar Estilo
	 * @var path is (file, full, null)
	 */
    function getStyle($file, $path = null) {
    	if (!empty($file))
    		$file = "$file";
    	
    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;
    	
    	$file = $path . "/core/styles/" . $file;
    	
    	if (empty($file))
    		return false;
    	else
			return $file;
    }

	/**
	 * Funcao de Carregar Javascript
	 * @var path is (file, full, null)
	 */
    function getJavaScript($file, $path = null) {
    	if (!empty($file))
    		$file = "$file";
    	
    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;
    	
    	$file = $path . "/core/javascripts/" . $file;
    	
    	if (empty($file))
    		return false;
    	else
			return $file;
    }

	/**
	 * Funcao de Carregar Plugin
	 * @var path is (file, full, null)
	 */
    function getPlugin($file, $path = null) {
    	if (!empty($file))
    		$file = "$file";
    	
    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;
    	
    	$file = $path . "/core/plugins/" . $file;
    	
    	if (empty($file))
    		return false;
    	else
			return $file;
    }

	/**
	 * Funcao de Carregar Include
	 * @var path is (file, full, null)
	 */
    function getInclude($file, $path = null) {
    	if (!empty($file))
    		$file = "$file";
    	
    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;
    	
    	$file = $path . "/core/includes/" . $file;
    	
    	if (empty($file))
    		return false;
    	else
			return $file;
    }

	/**
	 * Funcao de Carregar Classe
	 * @var path is (file, full, null)
	 */
    function getClasses($file, $path = null) {
    	if (!empty($file))
    		$file = "$file";

    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;

    	$file = $path . "/core/classes/" . $file;

    	if (empty($file))
    		return false;
    	else
			return $file;
    }

	/**
	 * Funcao de Carregar Estilo
	 * @var path is (file, full, null)
	 */
    function getTheme($file, $path = null) {
    	if (!empty($file))
    		$file = "$file";

    	if ($path == "full")
    		$path = SYSTEM_PATH_FULL;
    	elseif ($path == "file")
    		$path = SYSTEM_PATH_FILE;
    	else
    		$path = SYSTEM_PATH;

    	$file = $path . "/core/themes/" . $GLOBALS->THEME_SCHEMA . "/". $file;

    	if (empty($file))
    		return false;
    	else
			return $file;
    }
    
?>