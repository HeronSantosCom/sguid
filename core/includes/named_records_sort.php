<?php

    // bloqueador de acesso externo
    $url_check = $_SERVER["PHP_SELF"];
    if (eregi("named_records_sort.php", "$url_check")) {
      header ("Location: /index.php");
    }

	// create 1-dimensional named array with just
	function named_records_sort($named_recs, $order_by, $rev=false, $flags=0) {
		// sortfield (in stead of record) values
		$named_hash = array();
		foreach($named_recs as $key=>$fields)
			$named_hash["$key"] = $fields[$order_by];
	
		// Order 1-dimensional array,
		// maintaining key-value relations  
		if($rev) arsort($named_hash,$flags=0) ;
			else asort($named_hash, $flags=0);
	
		// Create copy of named records array
		// in order of sortarray 
		$sorted_records = array();
		foreach($named_hash as $key=>$val)
			$sorted_records["$key"]= $named_recs[$key];
	
		return $sorted_records;
	} // named_recs_sort()

?>