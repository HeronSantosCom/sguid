<?php

    /**
     * @author Heron Santos
     * @copyright 2008
     */

	// carrega configuracoes
    include("../core/configurations.php");
    
    // variavel
	$formbutton = null;
	$formclose = null;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta http-equiv="Cache-Control" content="no-cache, no-store">
	<meta http-equiv="Pragma" content="no-cache, no-store">
	<meta http-equiv="expires" content="Mon, 06 Jan 1990 00:00:01 GMT">
	<meta name="author" content="Heron Santos">

	<title><?= SYSTEM_TITLE ?> - Autenticação</title>
	
	<!-- Load Style -->
	<link href="<?= getStyle("general.css") ?>" rel="stylesheet" type="text/css">
	<link href="<?= getStyle("framework.css") ?>" rel="stylesheet" type="text/css">
	
	<!-- Define Diretorio Padrao -->
	<script type="text/javascript">
	    var SystemPath = '<?= SYSTEM_PATH ?>';
	    
	    var ImagePath = '<?= getImage("") ?>';
	</script>
	
	<!-- Load Fix PNG -->
	<style type="text/css">
		img, div, a, input, button {
			behavior: url("<?= getPlugin("iepngfix/iepngfix.htc") ?>");
		}
	</style>
	<script type="text/javascript" src="<?= getPlugin("iepngfix/iepngfix_tilebg.js") ?>"></script>

	<!-- Load JS -->
	<script type="text/javascript" src="<?= getJavaScript("navresized.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("get_open.js") ?>"></script>
    <script type="text/javascript" src="<?= getJavaScript("disable_context_menu.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("disable_refresh.js") ?>"></script>

	<script type="text/javascript">
		//hide loading box
		function hide_boxmessage_box() {
			document.getElementById('boxmessage').style.display = "none";
		}

	</script>

</head>

<body id="body-authentication">

<?php
	if ($authenticate_alert == "authenticated") {
		print "\t\t\t<script type=\"text/javascript\">";
		print "\t\t\t\tgetOpen('" . SYSTEM_PATH . "/framework/index.php', '_self');";
		print "\t\t\t</script>";
		exit;
	}
?>

<div id="background">
	<div id="folder"><img src="<?= getImage("logotype.folder.png") ?>" align="absmiddle"></div>
	<div id="form">
		<div id="container">
			<div id="title-container"><?= SYSTEM_TITLE ?> - Autenticação</div>
			<div id="content-container">
			<?php

				$form = new Form(true,NULL,"POST");
				$form->Input("hidden","action",NULL,NULL,md5("login"),255);
				$form->Input("text","authusername","Usuário",false);
				$form->Input("password","authpassword","Senha",false);
				$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL,false,true);
				$form->Close();

				if (isset($authenticate_alert)) {
					if ($authenticate_alert == "notfound") {
						print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Usuário não encontrado ou senha inválida...</div>";
					} elseif ($authenticate_alert == "expired") {
						print "<div id=\"boxmessage\"><img src=\"".getImage("alert.png")."\" align=\"absmiddle\">Sessão expirou...</div>";
					} elseif ($authenticate_alert == "logoff") {
						print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Sessão encerrada com sucesso...</div>";
					}
					print "<script type=\"text/javascript\"> \n";
					print "\t setTimeout(hide_boxmessage_box,3000); \n";
					print "</script> \n";
				}

			?>
			</div>
			<div id="footer-container">
				<p align="right">
					<?php
						if (!is_null($formbutton)) print $formbutton;
					?>
				</p>
			</div>
		</div>
	</div>
	<?php
		if (!is_null($formclose)) print $formclose;
	?>
</div>


</body>
</html>