<?php

    /**
    * @author Heron Santos
    * @copyright 2008
    */

	// carrega configuracoes
    include("../core/configurations.php");


	if ($GET->authentication == "authenticated") {

// carrega ordenador de array
		include(getInclude("named_records_sort.php","file"));

		//op1: verifica se a pasta exibe
		if (is_dir(FRAMEWORK_PATH_REFERENCES)) { // true
			$fifoList = scandir(FRAMEWORK_PATH_REFERENCES); //op: carrega a lista de arquivos
			//op2: verifica se o valor de list e um array
			if (is_array($fifoList)) { // true
				//op3: retorna o array da lista
				foreach ($fifoList as $fifoFile) {
					$fifoNameFile = explode(".",$fifoFile); $fifoNameFile = $fifoNameFile[0]; //op: quebra a variavel / e define o nome do arquivo
					$fifoPathFile = FRAMEWORK_PATH_REFERENCES . $fifoFile; //op: full path do arquivo
					//op4: verifica se o arquivo realmente existe
					if (is_file($fifoPathFile)) {
						$xml = simplexml_load_file($fifoPathFile); //op: carrega o xml atual
						//op5: verifica se o include e o principal, com o null
						if ($xml->reference == "null") {
							//op: resgata valores do xml
							if (isset($xml->reference)) $fifoReference = $xml->reference;
							if (isset($xml->caption)) $fifoCaption = $xml->caption;
							if (isset($xml->index)) $fifoIndex = $xml->index;
							if (isset($xml->level)) $fifoLevel = $xml->level;
							if (isset($xml->priority)) $fifoPriority = $xml->priority;
							if (isset($xml->description)) $fifoDescription = $xml->description;
							if (isset($xml->status)) $fifoStatus = $xml->status;
							if ($fifoStatus == "true") {
								//op: verifica se o status e ativo
								$main[] = array("id" => "$fifoNameFile", "caption" => "$fifoCaption", "index" => "$fifoIndex", "level" => "$fifoLevel", "priority" => "$fifoPriority", "description" => "$fifoDescription"); //op: monta o array do main
							}
						} else { //op5
							//op: resgata valores do xml
							unset($fifoIncludes);
							if (isset($xml->reference)) $fifoReference = $xml->reference;
							if (isset($xml->caption)) $fifoCaption = $xml->caption;
							if (isset($xml->index)) $fifoIndex = $xml->index;
							if (isset($xml->level)) $fifoLevel = $xml->level;
							if (isset($xml->priority)) $fifoPriority = $xml->priority;
							if (isset($xml->description)) $fifoDescription = $xml->description;
							if (isset($xml->status)) $fifoStatus = $xml->status;

							//op: verifica se o status e ativo
							if ($fifoStatus == "true") { //true
								if ($fifoIndex == "null" and isset($xml->includes)) //op: verifica se existe includes
										//op6: lista os includes
										foreach ($xml->includes->include as $includes) {
											//op: resgata valores do xml
											if (isset($includes->name)) $fifoName = $includes->name;
											if (isset($includes->fullname)) $fifoFullname = $includes->fullname;
											if (isset($includes->path)) $fifoPath = $includes->path;
											if (isset($includes->level)) $fifoLevel = $includes->level;
											if (isset($includes->status)) $fifoStatus = $xml->status;
											//op: verifica se o status e ativo
											if ($fifoStatus) {
												//op: monta o array do include
												$tempIncludes["name"] = "$fifoName";
												$tempIncludes["fullname"] = "$fifoFullname";
												$tempIncludes["path"] = "$fifoPath";
												$tempIncludes["level"] = "$fifoLevel";
												$fifoIncludes[] = $tempIncludes;
											}
										} //op6
								//op: monta o array do import
								$tempImport["id"] = "$fifoNameFile";
								$tempImport["caption"] = "$fifoCaption";
								$tempImport["index"] = "$fifoIndex";
								$tempImport["level"] = "$fifoLevel";
								$tempImport["priority"] = "$fifoPriority";
								$tempImport["description"] = "$fifoDescription";
								isset($fifoIncludes)?$tempImport["include"] = $fifoIncludes:null;
								$import["$fifoReference"][] = $tempImport;
							} //op
						} //op5
					} //op4
				} //op3
			} //op2
		} //op1

	}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta http-equiv="Cache-Control" content="no-cache, no-store">
	<meta http-equiv="Pragma" content="no-cache, no-store">
	<meta http-equiv="expires" content="Mon, 06 Jan 1990 00:00:01 GMT">
	<meta name="author" content="Heron Santos">

	<title><?= SYSTEM_TITLE ?></title>

	
	<!-- Load Style -->
	<link href="<?= getStyle("general.css") ?>" rel="stylesheet" type="text/css">
	<link href="<?= getStyle("framework.css") ?>" rel="stylesheet" type="text/css">
	
	<!-- Define Diretorio Padrao -->
	<script type="text/javascript">
	    var SystemPath = '<?= SYSTEM_PATH ?>';
	</script>

	<!-- Load Fix PNG -->
	<!--[if lte IE 6.5]>
		<style type="text/css">
			img, div, a, input, button {
				behavior: url("<?= getPlugin("iepngfix/iepngfix.htc") ?>");
			}
		</style>
		<script type="text/javascript" src="<?= getPlugin("iepngfix/iepngfix_tilebg.js") ?>"></script>
	<![endif]-->


	<!-- Load JS -->
	<script type="text/javascript" src="<?= getJavaScript("navresized.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("get_open.js") ?>"></script>
    <script type="text/javascript" src="<?= getJavaScript("disable_context_menu.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("disable_refresh.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("onclose.js") ?>"></script>

	<!-- Load MENU -->
	<link rel="stylesheet" type="text/css" href="<?= getPlugin("yuimenu/yui/reset-fonts-grids.css") ?>">
	<link href="<?= getStyle("menu/menu.css") ?>" rel="stylesheet" type="text/css">
	<style>
		#productsandservices {

			position: static;

		}

		/*
			For IE 6: trigger "haslayout" for the anchor elements in the root Menu by
			setting the "zoom" property to 1.  This ensures that the selected state of
			MenuItems doesn't get dropped when the user mouses off of the text node of
			the anchor element that represents a MenuItem's text label.
		*/
		#productsandservices .yuimenuitemlabel {

			_zoom: 1;

		}
		#productsandservices .yuimenu .yuimenuitemlabel {

			_zoom: normal;

		}
	</style>
	<script type="text/javascript" src="<?= getPlugin("yuimenu/yui/yahoo-dom-event.js") ?>"></script>
	<script type="text/javascript" src="<?= getPlugin("yuimenu/yui/container_core.js") ?>"></script>
	<script type="text/javascript" src="<?= getPlugin("yuimenu/yui/menu.js") ?>"></script>
	<script type="text/javascript">
		/*
			 Initialize and render the Menu when its elements are ready
			 to be scripted.
		*/
		YAHOO.util.Event.onContentReady("productsandservices", function () {
			/*
				 Instantiate a Menu:  The first argument passed to the
				 constructor is the id of the element in the page
				 representing the Menu; the second is an object literal
				 of configuration properties.
			*/
			var oMenu = new YAHOO.widget.Menu("productsandservices", {
													position: "static",
													hidedelay:  750,
													lazyload: true });
			/*
				 Call the "render" method with no arguments since the
				 markup for this Menu instance is already exists in the page.
			*/
			oMenu.render();
		});

	</script>

</head>

<?php
	if ($GET->authentication != "authenticated") {
		if (is_null($GET->login)) {
			$GET->login = md5("logon");
		}
?>

<body id="body-index" onselectstart="return false">
<script type="text/javascript">
	getOpen('<?= "authentication.php?login=" . md5("logon") ?>','_self');
</script>

<?php
	} else {
?>
<body class="yui-skin-sam" id="body-index" onload="javascript:init(); show_clock();" onunload="javascript:confirma_sair();" onselectstart="return false">

	<table cellpadding="0" cellspacing="0" width="100%" style="height: 100%">
		<tr>
			<td id="header">
				<table cellpadding="0" cellspacing="0" width="100%" style="height: 100%">
					<tr>
						<td id="logotype"><img src="<?= getImage("logotype.png") ?>" align="absmiddle"></td>
						<td id="information" align="right">
							<span class="field-parent">
								<span class="field" id="nameuser">Bem-vindo, <b><?= USER_CAPTION ?></b> [ <a title="Encerrar Sessão do Sistema" onclick="sair();">SAIR</a> ]</span>
								<span class="field" id="ipremoteaddr"><b>IP:</b> [ <?= $_SERVER["REMOTE_ADDR"] ?> ] </span>
								<span class="field" id="ipserveraddr"><b>Servidor:</b> [ <?= $_SERVER["SERVER_ADDR"] ?> ] </span>
								<script type="text/javascript" src="<?= getPlugin("liveclock/liveclock_lite.js") ?>"></script>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="100%">
				<table cellpadding="0" cellspacing="0" width="100%" style="height: 100%">
					<tr>
						<td width="20%" id="tdmenu">

						<?php

							$menu = named_records_sort($main, "priority", false); //op: ordena o main
							$xMain = 0; //op: inicia contador menu

							echo "<div id='productsandservices' class='yuimenu'>";
							echo "<div class='bd'>";
							echo "<ul class='first-of-type'> \n";
							foreach($menu as $name=>$fields) {
								foreach($fields as $name_field=>$val) {
									$field = "main_" . strtolower($name_field); //op: monta os campos
									$$field = $val; //op: atribui valores
								}

								echo "<li class='yuimenuitem".($xMain == 0?' first-of-type':'')."'> \n";
								if ($main_index <> "null") { //op: verifica se o index e nulo
									echo "<a class='yuimenuitemlabel' title='$main_description' href='javascript:void(0);' onclick=\"getOpen('container.php?token=MAIN/".$STRING->crypTo($main_id)."', 'container');\">$main_caption</a> \n";
								} else {
									echo "<a class='yuimenuitemlabel' title='$main_description' href='#'>$main_caption</a> \n";
									$last_main_index = FRAMEWORK_PATH_INCLUDES . $main_id . "/";
								}

								if (isset($import[$main_id]) and $main_index == "null") {//op: verifica se existe import
									if (is_array($import[$main_id])) { //true
										$module = named_records_sort($import[$main_id], "priority", false); //op: ordena o import
										$xModule = 0; //op: inicia contador modulo

										echo "<div id='module$xMain' class='yuimenu'> \n";
										echo "<div class='bd'> \n";
										echo "<ul ".($xMain == 0?'class="first-of-type"':'')."> \n";
										foreach($module as $name=>$fields) {
											foreach($fields as $name_field=>$val) {
												$field = "module_" . strtolower($name_field); //op: monta os campos
												$$field = $val; //op: atribui valores
											}

											echo "<li class='yuimenuitem'> \n";
											if ($module_index <> "null") { //op: verifica se o index e nulo
												echo "<a class='yuimenuitemlabel' title='$module_description' href='javascript:void(0);' onclick=\"getOpen('container.php?token=MAIN/". $STRING->crypTo($main_id) ."|MODULE/". $STRING->crypTo($module_id) ."', 'container');\">$module_caption</a> \n";
											} else { //op: se nulo indexmain e o id mais /
												echo "<a class='yuimenuitemlabel' title='$module_description' href='#'>$module_caption</a> \n";
												$last_module_index = $last_main_index . $module_id . "/";
											}

											if (isset($module_include) and $module_index == "null") {
												$xInclude = 0; //op: inicia contador do include

												echo "<div id='include$xModule' class='yuimenu'> \n";
												echo "<div class='bd'> \n";
												echo "<ul".($xInclude == 0?' class="first-of-type"':'')."> \n";
												foreach($module_include as $name=>$fields) {
													foreach($fields as $name_field=>$val) {
														$field = "include_" . strtolower($name_field); //op: monta os campos
														$$field = $val; //op: atribui valores
													}

													if ($include_path <> "null") { //op: verifica se o index e nulo
														echo "<li class='yuimenuitem'> \n";
														echo "<a class='yuimenuitemlabel' title='$include_fullname' href='javascript:void(0);' onclick=\"getOpen('container.php?token=MAIN/". $STRING->crypTo($main_id) ."|MODULE/". $STRING->crypTo($module_id) ."|INCLUDE/". $STRING->crypTo($include_path) ."', 'container');\">$include_name</a> \n";
														echo "</li> \n";
													}

													$xInclude++; //op: incrementa contador do include
													echo "</li> \n";
												}
												echo "</ul> \n";
												echo "</div> \n";
												echo "</div> \n";

											} //op12
											$xModule++; //op: incrementa contador do module
											echo "</li> \n";

										} //op10
										echo "</ul> \n";
										echo "</div> \n";
										echo "</div> \n";

									} //op9
								}
								$xMain++; //op: incrementa contador do menu
								echo "</li> \n";

							} //op7
							echo "</ul> \n";
							echo "</div> \n";
							echo "</div> \n";

						?>
						</td>
						<td width="80%" id="container"><iframe name="container" src="container.php" frameborder="0" marginwidth="0" marginheight="0" scrolling="auto" width="100%" height="100%" style="z-index: 1;"></iframe></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td id="footer">
				<span class="version"><b>Versão:</b> [ <?= (SYSTEM_VERSION . "." . SYSTEM_FINALIZATION . "." . SYSTEM_CONTRUCTION) ?> ]</span>
				<span>Copytight &copy; <?= date("Y") ?> TelosOnline.info</span>
			</td>
		</tr>
	</table>
<?php
	}
?>

</body>
</html>