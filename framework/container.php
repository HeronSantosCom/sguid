<?php

    /**
     * @author Heron Santos
     * @copyright 2008
     */

	// carrega configuracoes
    include("../core/configurations.php");
    
    // verifica se autenticado, senao volta para autenticacao
    if ($GET->authentication == "!authenticated") {
    	//header("Location: " . AUTHENTICATION_PATH . md5("expired") . "");
	$link = SYSTEM_PATH . "/framework/authentication.php?login=" . md5("expired");
    	CreateDialog("alert", "Erro", 'Sessão Expirada!\nEfetue login novamente...', $link, NULL, "_parent");
	    exit;
	}
	
	// declara variaveis
	$is_main = false;
	$is_module = false;
	$is_include = false;
	$err_display = false;
	$non_permission = false;
	$formbutton = null;
	$formclose = null;
	$forminclude = null;
		
	//op3: verifica se é principal
	if (isset($TOKEN->MAIN)) {
		$file = FRAMEWORK_PATH_REFERENCES . $TOKEN->MAIN . ".xml";
		if (is_file($file)) { //op7: verifica se é arquivo
			$main_xml = simplexml_load_file($file); //op: carrega o xml atual
			$main_index = $main_xml->index;
			$last_index = FRAMEWORK_PATH_INCLUDES . $TOKEN->MAIN . "/";
			$open_container = $last_index;
			
			if (USER_LEVELACCESS > $main_xml->level)
				$non_permission = true;
			
			if ($main_index <> "null" and !isset($TOKEN->INCLUDE)) {
				$open_container .= $main_index;
				debug("carregar: $open_container");
				if (is_file($open_container)){
					$token_title = $main_xml->description;
					$is_main = true;
					$err_display = false;
					//print("load: $open_container <br>");
				} else {
					$err_display = true;
					//print("erro load main: $open_container <br>");
				}
			}
		} else {
			$err_display = false;
			//print("erro load reference main: $file <br>");
		}
	}
	
	//op4: verifica se é modulo
	if (isset($TOKEN->MODULE)) {
		$file = FRAMEWORK_PATH_REFERENCES . $TOKEN->MODULE . ".xml";
		debug("carregando modulo: $file");
		if (is_file($file)) { //op7: verifica se é arquivo
			$module_xml = simplexml_load_file($file); //op: carrega o xml atual
			$module_index = $module_xml->index;
			$last_index .= $TOKEN->MODULE . "/";
			$open_container = $last_index;
			
			if (USER_LEVELACCESS > $module_xml->level)
				$non_permission = true;
			
			if ($module_index <> "null" and (isset($TOKEN->MODULE) and !isset($TOKEN->INCLUDE))) {
				$open_container .= $module_index;
				debug("carregar: $open_container");
				if (is_file($open_container)){
					$token_title = $module_xml->description;
					$is_module = true;
					$err_display = false;
					//print("load: $open_container <br>");
				} else {
					$err_display = true;
					//print("erro load module: $open_container <br>");
				}
			}
		} else {
			$err_display = true;
			//print("erro load reference module: $file <br>");
		}
	}
	
	//op5: verifica se é modo(include)
	if (isset($TOKEN->INCLUDE)) {
		if (isset($TOKEN->MODULE)) {
		    $file = FRAMEWORK_PATH_REFERENCES . $TOKEN->MODULE . ".xml";
		} elseif (isset($TOKEN->MAIN)) {
		    $file = FRAMEWORK_PATH_REFERENCES . $TOKEN->MAIN . ".xml";
		} else {
		    $err_display = true;
		}
		
		if (is_file($file) and !$err_display) { //op7: verifica se é arquivo
			$include_xml = simplexml_load_file($file); //op: carrega o xml atual
			if (isset($include_xml->includes)) //op: verifica se existe includes
				foreach ($include_xml->includes->include as $includes) { //op6: lista os includes
					if ($includes->path == $TOKEN->INCLUDE) { //op: verifica se o status é ativo
						$open_container .= $includes->path;
						debug("carregar: $open_container");
						
						if (USER_LEVELACCESS > $includes->level)
							$non_permission = true;
						
						if (is_file($open_container)){
							$token_title = $includes->fullname;
							$err_display = false;
							
							//print("load: $open_container <br>");
						} else {
							$err_display = true;
							//print("erro load include: $open_container <br>");
						}
						$is_include = true;
					}
				} //op6
		} else {
			$err_display = true;
			//print("erro load reference include: $file <br>");
		}
	}
	
	if (($is_include or $is_module or $is_main) and !$err_display) {
		define("TOKEN_TITLE", "$token_title");
		debug("Título: ". TOKEN_TITLE);
		define("TOKEN_PATH", "$open_container");
		debug("Caminho: ". TOKEN_PATH);
		define("INCLUDE_PATH_PAST", SYSTEM_PATH . "/framework/" . $last_index);
		debug("Caminho Include: ". INCLUDE_PATH_PAST);
		define("IMPORT_PATH_PAST", SYSTEM_PATH_FILE . "/framework/" . $last_index);
		debug("Caminho Import: ". IMPORT_PATH_PAST);
		define("LOADED_CONTAINER", $STRING->crypTo(SYSTEM_PATH_CONTAINER));
		debug("Container Aberto: ". LOADED_CONTAINER);
		
		if (isset($GET->id))
			$ID = $STRING->deCrypt($GET->id);
		elseif (isset($POST->id))
			$ID = $STRING->deCrypt($POST->id);
		else
			$ID = NULL;
			
		$is_null = false;
		
		
		
	} else {
		$is_null = true;
	}
		
	if (isset($GET->return))
		define("REFERRER", $STRING->deCrypt($GET->return));
	elseif (isset($POST->return))
		define("REFERRER", $STRING->deCrypt($POST->return));
	else
		define("REFERRER", SYSTEM_PATH . "/framework/container.php");

	//op9: se nao consegui carregar modulo exibe mensagem
	if ($err_display) {
		CreateDialog("alert","Erro",'Não foi possível carregar o módulo!\nEntre em contato com o administrador do sistema...',REFERRER);
		exit;
	}
	//op: sem permissao
	if ($non_permission) {
		CreateDialog("alert","Erro",'Permissão negada!\nEntre em contato com o administrador do sistema...',REFERRER);
		exit;
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store">
	<meta http-equiv="Pragma" content="no-cache, no-store">
	<meta http-equiv="expires" content="Mon, 06 Jan 1990 00:00:01 GMT">
	<meta name="author" content="Heron Santos">

	<title><?= TOKEN_TITLE ?></title>
	
	<!-- Load Style -->
	<link href="<?= getStyle("general.css") ?>" rel="stylesheet" type="text/css">
	<link href="<?= getStyle("framework.css") ?>" rel="stylesheet" type="text/css">
	
	<!-- Define Diretorio Padrao -->
	<script type="text/javascript">
	    var SystemPath = '<?= SYSTEM_PATH ?>';
	    var vlaCalPath = '<?= SYSTEM_PATH ?>/core/plugins/vlacalendar/';
	    
	    var ImagePath = '<?= getImage("") ?>';
	</script>
	
	<!-- Load Fix PNG -->
	<!--[if lte IE 6.5]>
		<style type="text/css">
			img, div, a, input, button {
				behavior: url("<?= getPlugin("iepngfix/iepngfix.htc") ?>");
			}
		</style>
		<script type="text/javascript" src="<?= getPlugin("iepngfix/iepngfix_tilebg.js") ?>"></script>
	<![endif]-->

	<!-- Load Vista-like Ajax Calendar version 2 -->
	<link type="text/css" media="screen" href="<?= getPlugin("vlacalendar/styles/vlaCal-v2.1.css") ?>" rel="stylesheet" />
	<link type="text/css" media="screen" href="<?= getPlugin("vlacalendar/styles/vlaCal-v2.1-adobe_cs3.css") ?>" rel="stylesheet" />
	<link type="text/css" media="screen" href="<?= getPlugin("vlacalendar/styles/vlaCal-v2.1-apple_widget.css") ?>" rel="stylesheet" />
	<script type="text/javascript" src="<?= getPlugin("vlacalendar/jslib/mootools-1.2-core.js") ?>"></script>
	<script type="text/javascript" src="<?= getPlugin("vlacalendar/jslib/vlaCal-v2.1.js") ?>"></script>

	<!-- Load JS -->
	<script type="text/javascript" src="<?= getJavaScript("navresized.js") ?>"></script>
	<!--<script type="text/javascript" src="<?= getJavaScript("disable_context_menu.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("disable_refresh.js") ?>"></script>-->
	<script type="text/javascript" src="<?= getJavaScript("get_open.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("get_popup.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("ajax.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("toggle_option.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("toggle_checkbox.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("toggle_tab.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("sleep.js") ?>"></script>
	<script type="text/javascript" src="<?= getJavaScript("replace_all.js") ?>"></script>

	<script type="text/javascript">
		//hide loading box
		function hide_boxmessage_box() {
			document.getElementById('boxmessage').style.display = "none";
		}

	</script>
</head>

<body id="body-container" onselectstart="return false">
<a name="top"></a>
	<?php
		//op10: se conter modulo a ser carregado
		if (!$is_null) {
	?>
	<div id="container">

		<div id="title-container"><button type="button" title="Fechar janela" id="close-container-top" onclick="getOpen('<?= REFERRER ?>','container');"><img src="<?= getImage("cancel.png") ?>" align="absmiddle"></button><img src="<?= getImage("application.png") ?>" align="absmiddle"> <?= TOKEN_TITLE ?></div>

		<div id="content-container">
			<?php
				//op10: se conter modulo a ser carregado
				if (isset($POST->action)) {
			?>
			<div id="boxmessage">
				<img src="<?= getImage("hourglass.png") ?>" align="absmiddle"> Efetuando Operações...
			</div>
			<script type="text/javascript">
				setTimeout(hide_boxmessage_box,1000);
			</script>
			<?php
				}
			?>
			<?php include(TOKEN_PATH) ?>
		</div>

		<!--<hr>-->
		<div id="footer-container">
			<p align="right">
				<?php
					if (!is_null($formbutton))
						print $formbutton;
				?>
				<button title="Fechar janela" type="button" id="close-container" onclick="getOpen('<?= REFERRER ?>','container');"><img src="<?= getImage("cancel.png") ?>" align="absmiddle"> Cancelar</button>
				<button title="Ir para o topo da página..." type="button" id="page_top" onclick="getOpen('#top', '_self');"><img src="<?= getImage("page_top.png") ?>" align="absmiddle"></button>
			</p>
		</div>
	</div>
	<?php
		if (!is_null($formclose))
			print $formclose;
	?>
	<script type="text/javascript" src="<?= getPlugin("event-listener.js") ?>"></script>
	<script type="text/javascript" src="<?= getPlugin("restrict.js") ?>"></script>
	<script type="text/javascript" src="<?= getPlugin("masked-input.js") ?>"></script>
	<script type="text/javascript" src="<?= getPlugin("format-currency.js") ?>"></script>
	<?php
		if (!is_null($forminclude))
			include($last_index . $forminclude);
	?>
	<br clear="all">
	<?php
	    } //op10
	?>
</body>
</html>