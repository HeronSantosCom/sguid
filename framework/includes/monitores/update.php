<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Monitor não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_monitor = $MYSQL->getFields("monitor");
	if (is_array($qry_monitor)) {
		foreach($qry_monitor as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados
	if(isset($_POST["idunidade"])) {
		$idunidade = $STRING->str2db($_POST["idunidade"]);
	} else {
		$idunidade = NULL;
	}
	debug("idunidade - ". $idunidade);

	// variaveis da tabela
	$qry_type_endereco = $MYSQL->getFields("type_endereco");
	if (is_array($qry_type_endereco)) {
		foreach($qry_type_endereco as $field) { //op: lista os campos da tabela
			$field = "endereco_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_ctps = $MYSQL->getFields("type_ctps");
	if (is_array($qry_type_ctps)) {
		foreach($qry_type_ctps as $field) { //op: lista os campos da tabela
			$field = "ctps_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_escolaridade = $MYSQL->getFields("type_escolaridade");
	if (is_array($qry_type_escolaridade)) {
		foreach($qry_type_escolaridade as $field) { //op: lista os campos da tabela
			$field = "escolaridade_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_identidade = $MYSQL->getFields("type_identidade");
	if (is_array($qry_type_identidade)) {
		foreach($qry_type_identidade as $field) { //op: lista os campos da tabela
			$field = "identidade_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_nacionalidade = $MYSQL->getFields("type_nacionalidade");
	if (is_array($qry_type_nacionalidade)) {
		foreach($qry_type_nacionalidade as $field) { //op: lista os campos da tabela
			$field = "nacionalidade_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_tituloeleitor = $MYSQL->getFields("type_tituloeleitor");
	if (is_array($qry_type_tituloeleitor)) {
		foreach($qry_type_tituloeleitor as $field) { //op: lista os campos da tabela
			$field = "tituloeleitor_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_nome"])) {
		$old_nome = $STRING->str2db($STRING->deCrypt($_POST["old_nome"]));
	} else {
		$old_nome = NULL;
	}
	debug("old_nome - ". $old_nome);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($endereco_idcbx_cidade) or empty($endereco_logradouro) or empty($endereco_bairro) or empty($endereco_cep)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// verifica se a cidade foi especificada
		} elseif (empty($ctps_numero) or empty($ctps_serie) or empty($ctps_dataemissao) or empty($ctps_idcbx_estado)) {
			CreateDialog("alert","Erro",'Falta informações sobre o CTPS...');

		// senao
		} elseif (empty($escolaridade_idcbx_grauescolar) or empty($escolaridade_idcbx_serieescolar) or empty($escolaridade_idcbx_tipoescola) or empty($escolaridade_instituicao) or empty($escolaridade_curso) or empty($escolaridade_idcbx_turnoescolar)) {
			CreateDialog("alert","Erro",'Falta informações sobre o escolaridade...');

		// senao
		} elseif (empty($identidade_numero) or empty($identidade_orgaoemissor) or empty($identidade_dataemissao)) {
			CreateDialog("alert","Erro",'Falta informações sobre o identidade......');

		// senao
		} elseif (!empty($nacionalidade_idcbx_nacionalidade) and ($nacionalidade_idcbx_nacionalidade > 1 and empty($nacionalidade_idcbx_federacao))) {
			CreateDialog("alert","Erro",'Falta informações sobre o nacionalidade...');

		// senao
		} elseif (empty($tituloeleitor_numero) or empty($tituloeleitor_zona) or empty($tituloeleitor_secao)) {
			CreateDialog("alert","Erro",'Falta informações sobre o titulo de eleitor......');

		// senao
		} elseif (empty($nome) or empty($nascimento) or empty($idcbx_sexo) or empty($idcbx_estadocivil) or empty($idcbx_etnia) or empty($nomemae) or empty($nomepai) or empty($cpf) or empty($experiencias) or empty($atividades)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_type_endereco[] = "idcbx_cidade = \"". $endereco_idcbx_cidade ."\"";
			$sql_type_endereco[] = "logradouro = \"". $endereco_logradouro ."\"";
			$sql_type_endereco[] = "bairro = \"". $endereco_bairro ."\"";
			$sql_type_endereco[] = "cep = \"". $endereco_cep ."\"";
			$sql_type_endereco = join(", ",$sql_type_endereco); // une a query
			debug("POSTA: ". $sql_type_endereco);
			$MYSQL->queryUpdate("`type_endereco`",$sql_type_endereco,"idtype_endereco = $idtype_endereco"); // commit

			//monta value do query
			$sql_type_ctps[] = "numero = \"". $ctps_numero ."\"";
			$sql_type_ctps[] = "serie = \"". $ctps_serie ."\"";
			$sql_type_ctps[] = "dataemissao = \"". $STRING->toDate($ctps_dataemissao, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_type_ctps[] = "idcbx_estado = \"". $ctps_idcbx_estado ."\"";
			$sql_type_ctps = join(", ",$sql_type_ctps); // une a query
			debug("POSTA: ". $sql_type_ctps);
			$MYSQL->queryUpdate("`type_ctps`",$sql_type_ctps,"idtype_ctps = $idtype_ctps"); // commit

			//monta value do query
			$sql_type_escolaridade[] = "idcbx_grauescolar = \"". $escolaridade_idcbx_grauescolar ."\"";
			$sql_type_escolaridade[] = "idcbx_serieescolar = \"". $escolaridade_idcbx_serieescolar ."\"";
			$sql_type_escolaridade[] = "idcbx_tipoescola = \"". $escolaridade_idcbx_tipoescola ."\"";
			$sql_type_escolaridade[] = "instituicao = \"". $escolaridade_instituicao ."\"";
			$sql_type_escolaridade[] = "curso = \"". $escolaridade_curso ."\"";
			$sql_type_escolaridade[] = "idcbx_turnoescolar = \"". $escolaridade_idcbx_turnoescolar ."\"";
			$sql_type_escolaridade = join(", ",$sql_type_escolaridade); // une a query
			debug("POSTA: ". $sql_type_escolaridade);
			$idtype_escolaridade = $MYSQL->queryUpdate("`type_escolaridade`",$sql_type_escolaridade,"idtype_escolaridade = $idtype_escolaridade"); // commit

			//monta value do query
			$sql_type_identidade[] = "numero = \"". $identidade_numero ."\"";
			$sql_type_identidade[] = "orgaoemissor = \"". $identidade_orgaoemissor ."\"";
			$sql_type_identidade[] = "dataemissao = \"". $STRING->toDate($identidade_dataemissao, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_type_identidade = join(", ",$sql_type_identidade); // une a query
			debug("POSTA: ". $sql_type_identidade);
			$MYSQL->queryUpdate("`type_identidade`",$sql_type_identidade,"idtype_identidade = $idtype_identidade"); // commit

			//monta value do query
			$sql_type_nacionalidade[] = "idcbx_nacionalidade = \"". $nacionalidade_idcbx_nacionalidade ."\"";
			$sql_type_nacionalidade[] = "idcbx_federacao = \"". ($nacionalidade_idcbx_nacionalidade>1?$nacionalidade_idcbx_federacao:FEDERACAO) ."\"";
			$sql_type_nacionalidade = join(", ",$sql_type_nacionalidade); // une a query
			debug("POSTA: ". $sql_type_nacionalidade);
			$MYSQL->queryUpdate("`type_nacionalidade`",$sql_type_nacionalidade,"idtype_nacionalidade = $idtype_nacionalidade"); // commit

			//monta value do query
			$sql_type_tituloeleitor[] = "numero = \"". $tituloeleitor_numero ."\"";
			$sql_type_tituloeleitor[] = "zona = \"". $tituloeleitor_zona ."\"";
			$sql_type_tituloeleitor[] = "secao = \"". $tituloeleitor_secao ."\"";
			$sql_type_tituloeleitor = join(", ",$sql_type_tituloeleitor); // une a query
			debug("POSTA: ". $sql_type_tituloeleitor);
			$MYSQL->queryUpdate("`type_tituloeleitor`",$sql_type_tituloeleitor,"idtype_tituloeleitor = $idtype_tituloeleitor"); // commit

			//monta value do query
			$sql_unidade_has_monitor[] = "idunidade = \"". $idunidade ."\"";
			$sql_unidade_has_monitor[] = "idmonitor = \"". $ID ."\"";
			$sql_unidade_has_monitor = join(", ",$sql_unidade_has_monitor); // une a query
			debug("POSTA: ". $sql_unidade_has_monitor);
			$MYSQL->queryUpdate("`unidade_has_monitor`",$sql_unidade_has_monitor,"idmonitor = $ID"); // commit

			//monta value do query
			$sql_monitor[] = "nome = \"". $nome ."\"";
			$sql_monitor[] = "nascimento = \"". $STRING->toDate($nascimento, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_monitor[] = "idcbx_sexo = \"". $idcbx_sexo ."\"";
			$sql_monitor[] = "idcbx_estadocivil = \"". $idcbx_estadocivil ."\"";
			$sql_monitor[] = "idcbx_etnia = \"". $idcbx_etnia ."\"";
			$sql_monitor[] = "idtype_nacionalidade = \"". $idtype_nacionalidade ."\"";
			$sql_monitor[] = "nomemae = \"". $nomemae ."\"";
			$sql_monitor[] = "nomepai = \"". $nomepai ."\"";
			$sql_monitor[] = "idtype_endereco = \"". $idtype_endereco ."\"";
			$sql_monitor[] = "idtype_identidade = \"". $idtype_identidade ."\"";
			$sql_monitor[] = "cpf = \"". $cpf ."\"";
			$sql_monitor[] = "idtype_tituloeleitor = \"". $idtype_tituloeleitor ."\"";
			$sql_monitor[] = "idtype_ctps = \"". $idtype_ctps ."\"";
			$sql_monitor[] = "idtype_escolaridade = \"". $idtype_escolaridade ."\"";
			$sql_monitor[] = "experiencias = \"". $experiencias ."\"";
			$sql_monitor[] = "atividades = \"". $atividades ."\"";
			// une a query
			$sql_monitor = join(", ",$sql_monitor);
			debug("POSTA: ". $sql_monitor);

			// se nao tiver alterado e nao tiver erro
			if ($nome == $old_nome and !$action_error) {
				debug("CONTINUA nome: ". $old_nome);
				// commit
				if ($MYSQL->queryUpdate("`monitor`",$sql_monitor,"idmonitor = \"$ID\"")) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// se tiver alterado e nao tiver erro
			} elseif (!$action_error) {
				debug("MUDOU nome: ". $nome);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`monitor`","lower(nome) = lower(\"$nome\")")) {

					// commit
					if ($MYSQL->queryUpdate("`monitor`",$sql_monitor,"idmonitor = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra que o monitor já está cadastrado...');
				}
			}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// monitor
	if ($MYSQL->dbCheck("`monitor`","idmonitor = \"$ID\"")) { // verifico se o equipamento existe
		$qry_monitor = $MYSQL->querySelect("monitor",NULL,"idmonitor = \"$ID\"");
		foreach ($qry_monitor as $tbl_monitor) {
			foreach($tbl_monitor as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	if (isset($nascimento)) {
		$nascimento = $STRING->toDate($nascimento, "YYYY-MM-DD", "DD/MM/YYYY");
	}

	if (isset($idtype_ctps)) {
		if ($MYSQL->dbCheck("type_ctps", "idtype_ctps = $idtype_ctps")) {
			$ctps_dataemissao = $STRING->toDate($MYSQL->getValue("type_ctps", "dataemissao", "idtype_ctps = $idtype_ctps"), "YYYY-MM-DD", "DD/MM/YYYY");
		}
	}

	if (isset($idtype_identidade)) {
		if ($MYSQL->dbCheck("type_identidade", "idtype_identidade = $idtype_identidade")) {
			$identidade_dataemissao = $STRING->toDate($MYSQL->getValue("type_identidade", "dataemissao", "idtype_identidade = $idtype_identidade"), "YYYY-MM-DD", "DD/MM/YYYY");
		}
	}
	
	if ($MYSQL->dbCheck("unidade_has_monitor", "idmonitor = $ID")) {
		$idunidade = $MYSQL->getValue("unidade_has_monitor", "idunidade", "idmonitor = $ID");
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Alteração efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>