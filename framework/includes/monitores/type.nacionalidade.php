<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_nacionalidade) ? $idtype_nacionalidade = $GET->idtype_nacionalidade : $idtype_nacionalidade = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);


	// LISTA TODOS OS federacaoS
	$qry_cbx_federacao = $MYSQL->querySelect("cbx_federacao","idcbx_federacao,federacao");
	if (is_array($qry_cbx_federacao)) {
		foreach($qry_cbx_federacao as $tbl_cbx_federacao) {
			$tmp_cbx_federacao["id"] = $tbl_cbx_federacao["idcbx_federacao"];
			$tmp_cbx_federacao["caption"] = $STRING->db2str($tbl_cbx_federacao["federacao"]);
			$cbx_federacao[] = $tmp_cbx_federacao;
		}
	}

	// LISTA TODOS OS federacaoS
	$qry_cbx_nacionalidade = $MYSQL->querySelect("cbx_nacionalidade","idcbx_nacionalidade,nacionalidade");
	if (is_array($qry_cbx_nacionalidade)) {
		foreach($qry_cbx_nacionalidade as $tbl_cbx_nacionalidade) {
			$tmp_cbx_nacionalidade["id"] = $tbl_cbx_nacionalidade["idcbx_nacionalidade"];
			$tmp_cbx_nacionalidade["caption"] = $STRING->db2str($tbl_cbx_nacionalidade["nacionalidade"]);
			$cbx_nacionalidade[] = $tmp_cbx_nacionalidade;
		}
	}

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_nacionalidade","idtype_nacionalidade = \"$idtype_nacionalidade\"")) { // verifico se a nacionalidade existe
		$qry_type_nacionalidade = $MYSQL->querySelect("type_nacionalidade",NULL,"idtype_nacionalidade = \"$idtype_nacionalidade\"");
		foreach ($qry_type_nacionalidade as $tbl_type_nacionalidade) {
			foreach($tbl_type_nacionalidade as $field=>$val) { //op: lista os campos da tabela
				$field = "nacionalidade_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
	
	$form->Select("nacionalidade_idcbx_nacionalidade", "Nacionalidade", $cbx_nacionalidade, "string", $nacionalidade_idcbx_nacionalidade, true, NULL, false, "toggle_nacionalidade(this.value);");
	$form->Select("nacionalidade_idcbx_federacao", "País de Origem", $cbx_federacao, "string", $nacionalidade_idcbx_federacao, true, NULL, false, NULL, true);
	
	$form->Null("\t <script type=\"text/javascript\"> \n");
	$form->Null("\t\t function toggle_nacionalidade(value) { \n");
	$form->Null("\t\t\t if (value > 1) { \n");
	$form->Null("\t\t\t\t document.getElementById(\"nacionalidade_idcbx_federacao\").disabled = false; \n");
	$form->Null("\t\t\t } else { \n");
	$form->Null("\t\t\t\t document.getElementById(\"nacionalidade_idcbx_federacao\").disabled = true; \n");
	$form->Null("\t\t\t } \n");
	$form->Null("\t\t } \n");
	$form->Null("\t\t toggle_nacionalidade('".$nacionalidade_idcbx_nacionalidade."'); \n");
	$form->Null("\t </script> \n");
	$form->Close();
?>