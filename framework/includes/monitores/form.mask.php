<!-- Load Mask Form -->
<script type="text/javascript">
//<![CDATA[

	var r = new Restrict("form");

    r.field.cpf = "\\d.-";
    r.mask.cpf = "###.###.###-##";

	r.onKeyRefuse = function(o, k){
		o.style.backgroundColor = "#E0E0E0";
	}
	r.onKeyAccept = function(o, k){
		if(k > 30)
			o.style.backgroundColor = "#FFFFFF";
	}
	r.start();

	MaskAjax_Active_Cep = false
	function active_mask_cep() {
		if (MaskAjax_Active_Cep == false) {
			var r = new Restrict("form");

			r.field.endereco_cep = "\\d-";
			r.mask.endereco_cep = "#####-###";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_Cep = true;
		}
	}

	MaskAjax_Active_Identidade = false
	function active_mask_identidade_numero() {
		if (MaskAjax_Active_Identidade == false) {
			var r = new Restrict("form");

			r.field.identidade_numero = "\\d-.";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_Identidade = true;
		}
	}

	MaskAjax_Active_TituloEleitor_Numero = false
	function active_mask_tituloeleitor_numero() {
		if (MaskAjax_Active_TituloEleitor_Numero == false) {
			var r = new Restrict("form");

			r.field.tituloeleitor_numero = "\\d";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_TituloEleitor_Numero = true;
		}
	}

	MaskAjax_Active_TituloEleitor_Zona = false
	function active_mask_tituloeleitor_zona() {
		if (MaskAjax_Active_TituloEleitor_Zona == false) {
			var r = new Restrict("form");

			r.field.tituloeleitor_zona = "\\d";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_TituloEleitor_Zona = true;
		}
	}

	MaskAjax_Active_TituloEleitor_Secao = false
	function active_mask_tituloeleitor_secao() {
		if (MaskAjax_Active_TituloEleitor_Secao == false) {
			var r = new Restrict("form");

			r.field.tituloeleitor_secao = "\\d";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_TituloEleitor_Secao = true;
		}
	}

	MaskAjax_Active_CTPS_Numero = false
	function active_mask_ctps_numero() {
		if (MaskAjax_Active_CTPS_Numero == false) {
			var r = new Restrict("form");

			r.field.ctps_numero = "\\d";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_CTPS_Numero = true;
		}
	}

	MaskAjax_Active_CTPS_Serie = false
	function active_mask_ctps_serie() {
		if (MaskAjax_Active_CTPS_Serie == false) {
			var r = new Restrict("form");

			r.field.ctps_serie = "\\d";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_CTPS_Serie = true;
		}
	}

//]]>
</script>