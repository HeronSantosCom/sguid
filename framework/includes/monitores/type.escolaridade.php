<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_escolaridade) ? $idtype_escolaridade = $GET->idtype_escolaridade : $idtype_escolaridade = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);
	
	// LISTA TODOS OS GRAU ESCOLAR
	$qry_cbx_grauescolar = $MYSQL->querySelect("cbx_grauescolar","idcbx_grauescolar,grauescolar");
	if (is_array($qry_cbx_grauescolar)) {
		foreach($qry_cbx_grauescolar as $tbl_cbx_grauescolar) {
			$tmp_cbx_grauescolar["id"] = $tbl_cbx_grauescolar["idcbx_grauescolar"];
			$tmp_cbx_grauescolar["caption"] = $STRING->db2str($tbl_cbx_grauescolar["grauescolar"]);
			$cbx_grauescolar[] = $tmp_cbx_grauescolar;
		}
	}

	// LISTA TODOS OS SERIE ESCOLAR
	$qry_cbx_serieescolar = $MYSQL->querySelect("cbx_serieescolar","idcbx_serieescolar,serieescolar");
	if (is_array($qry_cbx_serieescolar)) {
		foreach($qry_cbx_serieescolar as $tbl_cbx_serieescolar) {
			$tmp_cbx_serieescolar["id"] = $tbl_cbx_serieescolar["idcbx_serieescolar"];
			$tmp_cbx_serieescolar["caption"] = $STRING->db2str($tbl_cbx_serieescolar["serieescolar"]);
			$cbx_serieescolar[] = $tmp_cbx_serieescolar;
		}
	}

	// LISTA TODOS OS TIPO ESCOLA
	$qry_cbx_tipoescola = $MYSQL->querySelect("cbx_tipoescola","idcbx_tipoescola,tipoescola");
	if (is_array($qry_cbx_tipoescola)) {
		foreach($qry_cbx_tipoescola as $tbl_cbx_tipoescola) {
			$tmp_cbx_tipoescola["id"] = $tbl_cbx_tipoescola["idcbx_tipoescola"];
			$tmp_cbx_tipoescola["caption"] = $STRING->db2str($tbl_cbx_tipoescola["tipoescola"]);
			$cbx_tipoescola[] = $tmp_cbx_tipoescola;
		}
	}

	// LISTA TODOS OS TURNO ESCOLAR
	$qry_cbx_turnoescolar = $MYSQL->querySelect("cbx_turnoescolar","idcbx_turnoescolar,turnoescolar");
	if (is_array($qry_cbx_turnoescolar)) {
		foreach($qry_cbx_turnoescolar as $tbl_cbx_turnoescolar) {
			$tmp_cbx_turnoescolar["id"] = $tbl_cbx_turnoescolar["idcbx_turnoescolar"];
			$tmp_cbx_turnoescolar["caption"] = $STRING->db2str($tbl_cbx_turnoescolar["turnoescolar"]);
			$cbx_turnoescolar[] = $tmp_cbx_turnoescolar;
		}
	}

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_escolaridade","idtype_escolaridade = \"$idtype_escolaridade\"")) { // verifico se a escolaridade existe
		$qry_type_escolaridade = $MYSQL->querySelect("type_escolaridade",NULL,"idtype_escolaridade = \"$idtype_escolaridade\"");
		foreach ($qry_type_escolaridade as $tbl_type_escolaridade) {
			foreach($tbl_type_escolaridade as $field=>$val) { //op: lista os campos da tabela
				$field = "escolaridade_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	$form->Select("escolaridade_idcbx_grauescolar", "Grau Escolar", $cbx_grauescolar, "string", $escolaridade_idcbx_grauescolar, true);
	$form->Select("escolaridade_idcbx_serieescolar", "Série Escolar", $cbx_serieescolar, "string", $escolaridade_idcbx_serieescolar, true);
	$form->Select("escolaridade_idcbx_tipoescola", "Tipo da Instituição", $cbx_tipoescola, "string", $escolaridade_idcbx_tipoescola, true);
	$form->Input("text","escolaridade_instituicao","Instituição",true,$escolaridade_instituicao,255);
	$form->Input("text","escolaridade_curso","Curso",true,$escolaridade_curso,255);
	$form->Select("escolaridade_idcbx_turnoescolar", "Turno", $cbx_turnoescolar, "string", $escolaridade_idcbx_turnoescolar, true);

	$form->Close();
?>