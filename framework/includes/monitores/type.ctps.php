<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_ctps) ? $idtype_ctps = $GET->idtype_ctps : $idtype_ctps = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);
	
	// LISTA TODOS OS ESTADOS
	$qry_cbx_estado = $MYSQL->querySelect("cbx_estado","idcbx_estado,estado","idcbx_federacao = " . FEDERACAO);
	if (is_array($qry_cbx_estado)) {
		foreach($qry_cbx_estado as $tbl_cbx_estado) {
			$tmp_cbx_estado["id"] = $tbl_cbx_estado["idcbx_estado"];
			$tmp_cbx_estado["caption"] = $STRING->db2str($tbl_cbx_estado["estado"]);
			$cbx_estado[] = $tmp_cbx_estado;
		}
	}

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_ctps","idtype_ctps = \"$idtype_ctps\"")) { // verifico se a ctps existe
		$qry_type_ctps = $MYSQL->querySelect("type_ctps",NULL,"idtype_ctps = \"$idtype_ctps\"");
		foreach ($qry_type_ctps as $tbl_type_ctps) {
			foreach($tbl_type_ctps as $field=>$val) { //op: lista os campos da tabela
				$field = "ctps_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	$form->Input("text","ctps_numero","Número",true,$ctps_numero,10,false,"onfocus=\"active_mask_ctps_numero();\"");
	$form->Input("text","ctps_serie","Série",true,$ctps_serie,5,false,"onfocus=\"active_mask_ctps_serie();\"");
	$form->Select("ctps_idcbx_estado", "Estado", $cbx_estado, "string", $ctps_idcbx_estado, true);

	$form->Close();
?>