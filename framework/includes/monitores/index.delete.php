<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	// RESGATANDO E DECLARANDO VARIAVEIS
    isset($GET->list) ? $list=$GET->list : $list=NULL;
    $action_error = false;
    $not_found = false;
    $action_ok = false;

	// VERIFICA AUSENCIA DO ID
	if (isset($list)) {
		
		$aux_list = explode("|",$list); // desmembra lista de equipamentos
		if (is_array($aux_list)) {
			foreach($aux_list as $aux_id) {
				$ID = $STRING->deCrypt($aux_id);

				// CARREGA DADOS CADASTRADOS
				if ($MYSQL->dbCheck("`monitor`","idmonitor = \"$ID\"")) { // verifico se o equipamento existe
					$qry_monitor = $MYSQL->querySelect("`monitor`","idtype_endereco","idmonitor = \"$ID\"");
					foreach ($qry_monitor as $tbl_monitor) {
						foreach($tbl_monitor as $field=>$val) { //op: lista os campos da tabela
							$$field = $STRING->db2str($val); //op: atribui valores
						}
					}
				}

				// remove tituloeleitor
				if (!$action_error) {
					if ($MYSQL->dbCheck("unidade_has_monitor","idmonitor = \"$ID\"")) {
						if ($MYSQL->queryDelete("unidade_has_monitor","idmonitor = \"$ID\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove monitor
				if (!$action_error) {
					if ($MYSQL->queryDelete("`monitor`","idmonitor = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}
				}

				// remove endereco
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_endereco","idtype_endereco = \"$idtype_endereco\"")) {
						if ($MYSQL->queryDelete("type_endereco","idtype_endereco = \"$idtype_endereco\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove ctps
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_ctps","idtype_ctps = \"$idtype_ctps\"")) {
						if ($MYSQL->queryDelete("type_ctps","idtype_ctps = \"$idtype_ctps\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove escolaridade
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_escolaridade","idtype_escolaridade = \"$idtype_escolaridade\"")) {
						if ($MYSQL->queryDelete("type_escolaridade","idtype_escolaridade = \"$idtype_escolaridade\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove identidade
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_identidade","idtype_identidade = \"$idtype_identidade\"")) {
						if ($MYSQL->queryDelete("type_identidade","idtype_identidade = \"$idtype_identidade\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove nacionalidade
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_nacionalidade","idtype_nacionalidade = \"$idtype_nacionalidade\"")) {
						if ($MYSQL->queryDelete("type_nacionalidade","idtype_nacionalidade = \"$idtype_nacionalidade\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove tituloeleitor
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_tituloeleitor","idtype_tituloeleitor = \"$idtype_tituloeleitor\"")) {
						if ($MYSQL->queryDelete("type_tituloeleitor","idtype_tituloeleitor = \"$idtype_tituloeleitor\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}
			}
		} else {
			$not_found = true;
		}
	} else {
		$not_found = true;
	}
    
	if ($action_error) { // erro na operacao
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Erro ao efetuar operação...</div>";
	}
	
	if ($not_found) { // nao encontrado
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Os monitores selecionadas não foram encontrados...</div>";
	}

    if ($action_ok) { // operacao efetuada
		print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Monitor removida com sucesso...</div>";
		print "<script type=\"text/javascript\">";
		print "\trefresh_list();";
		print "</script>";
	}
	
?>
<script type="text/javascript">
	setTimeout(hide_delete_box,3000);
</script>