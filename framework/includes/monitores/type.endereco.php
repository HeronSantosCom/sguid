<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_endereco) ? $idtype_endereco = $GET->idtype_endereco : $idtype_endereco = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);
	$form->Fieldset("localizacao", "Localização");


	// LISTA TODOS OS ESTADOS
	$qry_cbx_estado = $MYSQL->querySelect("cbx_estado","idcbx_estado,estado","idcbx_federacao = " . FEDERACAO);
	if (is_array($qry_cbx_estado)) {
		foreach($qry_cbx_estado as $tbl_cbx_estado) {
			$tmp_cbx_estado["id"] = $tbl_cbx_estado["idcbx_estado"];
			$tmp_cbx_estado["caption"] = $STRING->db2str($tbl_cbx_estado["estado"]);
			$cbx_estado[] = $tmp_cbx_estado;
		}
	}

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_endereco","idtype_endereco = \"$idtype_endereco\"")) { // verifico se o equipamento existe
		$qry_type_endereco = $MYSQL->querySelect("type_endereco",NULL,"idtype_endereco = \"$idtype_endereco\"");
		foreach ($qry_type_endereco as $tbl_type_endereco) {
			foreach($tbl_type_endereco as $field=>$val) { //op: lista os campos da tabela
				$field = "endereco_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	if (isset($endereco_idcbx_cidade)) {
	    $endereco_idcbx_estado = $MYSQL->getValue("cbx_cidade", "idcbx_estado", "idcbx_cidade = $endereco_idcbx_cidade");
	} else {
	    $endereco_idcbx_estado = 0;
	}
	
	$form->Select("endereco_idcbx_estado", "Estado", $cbx_estado, "string", $endereco_idcbx_estado, true, NULL, false, "toggle_estado(this.value);");
	$form->Object("type_cidade"," ");
	$form->Input("text","endereco_logradouro","Logradouro",false,$endereco_logradouro,255);
	$form->Input("text","endereco_bairro","Bairro",false,$endereco_bairro,255);
	$form->Input("text","endereco_cep","CEP",false,$endereco_cep,9,false,"onfocus=\"active_mask_cep();\"");
	$form->EndFieldset();

	$form->Null("\t <script type=\"text/javascript\"> \n");
	$form->Null("\t\t function toggle_estado(value) { \n");
	$form->Null("\t\t\t load_form_cidade(value); \n");
	$form->Null("\t\t } \n");
	$form->Null("\t\t load_form_cidade('".$endereco_idcbx_estado."','".$endereco_idcbx_cidade."'); \n");
	$form->Null("\t </script> \n");

	$form->Close();
?>