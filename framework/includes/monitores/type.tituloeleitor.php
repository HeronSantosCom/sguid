<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_tituloeleitor) ? $idtype_tituloeleitor = $GET->idtype_tituloeleitor : $idtype_tituloeleitor = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_tituloeleitor","idtype_tituloeleitor = \"$idtype_tituloeleitor\"")) { // verifico se a tituloeleitor existe
		$qry_type_tituloeleitor = $MYSQL->querySelect("type_tituloeleitor",NULL,"idtype_tituloeleitor = \"$idtype_tituloeleitor\"");
		foreach ($qry_type_tituloeleitor as $tbl_type_tituloeleitor) {
			foreach($tbl_type_tituloeleitor as $field=>$val) { //op: lista os campos da tabela
				$field = "tituloeleitor_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	$form->Input("text","tituloeleitor_numero","Título de Eleitor",true,$tituloeleitor_numero,50,false,"onfocus=\"active_mask_tituloeleitor_numero();\"");
	$form->Input("text","tituloeleitor_zona","Zona Eleitoral",true,$tituloeleitor_zona,50,false,"onfocus=\"active_mask_tituloeleitor_zona();\"");
	$form->Input("text","tituloeleitor_secao","Seção Eleitoral",true,$tituloeleitor_secao,50,false,"onfocus=\"active_mask_tituloeleitor_secao();\"");

	$form->Close();
?>