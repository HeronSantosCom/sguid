<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Curso não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_type_curso = $MYSQL->getFields("type_curso");
	if (is_array($qry_type_curso)) {
		foreach($qry_type_curso as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_curso"])) {
		$old_curso = $STRING->str2db($STRING->deCrypt($_POST["old_curso"]));
	} else {
		$old_curso = NULL;
	}
	debug("old_curso - ". $old_curso);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($curso)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_type_curso[] = "curso = \"". $curso ."\"";
			$sql_type_curso[] = "descricao = \"". $descricao ."\"";
			// une a query
			$sql_type_curso = join(", ",$sql_type_curso);
			debug("POSTA: ". $sql_type_curso);

			// se nao tiver alterado e nao tiver erro
			if ($curso == $old_curso and !$action_error) {
				debug("CONTINUA curso: ". $old_curso);
				// commit
				if ($MYSQL->queryUpdate("`type_curso`",$sql_type_curso,"idtype_curso = \"$ID\"")) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// se tiver alterado e nao tiver erro
			} elseif (!$action_error) {
				debug("MUDOU curso: ". $curso);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`type_curso`","lower(curso) = lower(\"$curso\")")) {

					// commit
					if ($MYSQL->queryUpdate("`type_curso`",$sql_type_curso,"idtype_curso = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra que o curso já está cadastrado...');
				}
			}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// type_curso
	if ($MYSQL->dbCheck("`type_curso`","idtype_curso = \"$ID\"")) { // verifico se o equipamento existe
		$qry_type_curso = $MYSQL->querySelect("type_curso",NULL,"idtype_curso = \"$ID\"");
		foreach ($qry_type_curso as $tbl_type_curso) {
			foreach($tbl_type_curso as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Alteração efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>