<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_type_curso = $MYSQL->getFields("type_curso");
	if (is_array($qry_type_curso)) {
		foreach($qry_type_curso as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_curso"])) {
		$old_curso = $STRING->str2db($STRING->deCrypt($_POST["old_curso"]));
	} else {
		$old_curso = NULL;
	}
	debug("old_curso - ". $old_curso);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($curso)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_type_curso[] = "curso = \"". $curso ."\"";
			$sql_type_curso[] = "descricao = \"". $descricao ."\"";
			// une a query
			$sql_type_curso = join(", ",$sql_type_curso);
			debug("POSTA: ". $sql_type_curso);


			// se nao existe registro
			if (!$MYSQL->dbCheck("`type_curso`","lower(curso) = lower(\"$curso\")")) {

				// commit
				if ($ID = $MYSQL->queryInsert("`type_curso`",$sql_type_curso)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que o curso já está cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		//CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...');
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>