<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

	// LISTA TODOS OS CURSOS
	$qry_type_curso = $MYSQL->querySelect("type_curso","idtype_curso,curso");
	if (is_array($qry_type_curso)) {
		foreach($qry_type_curso as $tbl_type_curso) {
			$tmp_type_curso["id"] = $tbl_type_curso["idtype_curso"];
			$tmp_type_curso["caption"] = $STRING->db2str($tbl_type_curso["curso"]);
			$type_curso[] = $tmp_type_curso;
		}
	}
	
	// LISTA TODOS OS MODULOS
	$qry_cbx_modulo = $MYSQL->querySelect("cbx_modulo","idcbx_modulo,modulo");
	if (is_array($qry_cbx_modulo)) {
		foreach($qry_cbx_modulo as $tbl_cbx_modulo) {
			$tmp_cbx_modulo["id"] = $tbl_cbx_modulo["idcbx_modulo"];
			$tmp_cbx_modulo["caption"] = $STRING->db2str($tbl_cbx_modulo["modulo"]);
			$cbx_modulo[] = $tmp_cbx_modulo;
		}
	}

		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);

	$form->Fieldset("basic", "Relacionamento");
	$form->Select("idtype_curso", "Curso", $type_curso, "string", $idtype_curso, true, NULL, false, "toggle_curso(this.value);");
	$form->Select("idcbx_modulo", "Módulo", $cbx_modulo, "string", $idcbx_modulo, true);
	$form->EndFieldset();

	$form->Fieldset("modulo_has_curso", "Módulos Associados");
	$form->Object("curso"," ");
	$form->EndFieldset();

	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>
<script type="text/javascript">

	function load_form_modulo(idtype_curso) {
		//alert("Curso: " + idtype_curso + ")
		ajaxpage("<?= INCLUDE_PATH_PAST ?>curso.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_curso=" + idtype_curso, "curso", false);
	}

	function toggle_curso(value) {
		load_form_modulo(value);
	}

<?php
	print "\t load_form_modulo(\"".$idtype_curso."\"); \n";
?>
</script>