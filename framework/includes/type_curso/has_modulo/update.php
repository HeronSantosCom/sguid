<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Curso não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_cbx_modulo_has_type_curso = $MYSQL->getFields("cbx_modulo_has_type_curso");
	if (is_array($qry_cbx_modulo_has_type_curso)) {
		foreach($qry_cbx_modulo_has_type_curso as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idcbx_modulo) or empty($idtype_curso)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_cbx_modulo_has_type_curso[] = "idcbx_modulo = \"". $idcbx_modulo ."\"";
			$sql_cbx_modulo_has_type_curso[] = "idtype_curso = \"". $idtype_curso ."\"";
			// une a query
			$sql_cbx_modulo_has_type_curso = join(", ",$sql_cbx_modulo_has_type_curso);
			debug("POSTA: ". $sql_cbx_modulo_has_type_curso);

				debug("MUDOU curso: ". $curso);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`cbx_modulo_has_type_curso`","idcbx_modulo = $idcbx_modulo and idtype_curso = $idtype_curso")) {

					// commit
					if ($MYSQL->queryUpdate("`cbx_modulo_has_type_curso`",$sql_cbx_modulo_has_type_curso,"idcbx_modulo_has_type_curso = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra que já existe esta associação...');
				}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// cbx_modulo_has_type_curso
	if ($MYSQL->dbCheck("`cbx_modulo_has_type_curso`","idcbx_modulo_has_type_curso = \"$ID\"")) { // verifico se o equipamento existe
		$qry_cbx_modulo_has_type_curso = $MYSQL->querySelect("cbx_modulo_has_type_curso",NULL,"idcbx_modulo_has_type_curso = \"$ID\"");
		foreach ($qry_cbx_modulo_has_type_curso as $tbl_cbx_modulo_has_type_curso) {
			foreach($tbl_cbx_modulo_has_type_curso as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar associação!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Associação efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>