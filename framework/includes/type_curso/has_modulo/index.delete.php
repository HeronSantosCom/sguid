<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

	// RESGATANDO E DECLARANDO VARIAVEIS
    isset($GET->list) ? $list=$GET->list : $list=NULL;
    $action_error = false;
    $not_found = false;
    $action_ok = false;

	// VERIFICA AUSENCIA DO ID
	if (isset($list)) {
		
		$aux_list = explode("|",$list); // desmembra lista de equipamentos
		if (is_array($aux_list)) {
			foreach($aux_list as $aux_id) {
				$ID = $STRING->deCrypt($aux_id);

				// CARREGA DADOS CADASTRADOS
				/*if ($MYSQL->dbCheck("`type_curso`","idtype_curso = \"$ID\"")) { // verifico se o equipamento existe
					$qry_type_curso = $MYSQL->querySelect("`type_curso`","idtype_curso","idtype_curso = \"$ID\"");
					foreach ($qry_type_curso as $tbl_type_curso) {
						foreach($tbl_type_curso as $field=>$val) { //op: lista os campos da tabela
							$$field = $STRING->db2str($val); //op: atribui valores
						}
					}
				})*/

				// remove cbx_modulo_has_type_curso
				if (!$action_error) {
					if ($MYSQL->queryDelete("`cbx_modulo_has_type_curso`","idcbx_modulo_has_type_curso = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}
				}
			}
		} else {
			$not_found = true;
		}
	} else {
		$not_found = true;
	}
    
	if ($action_error) { // erro na operacao
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Erro ao efetuar operação...</div>";
	}
	
	if ($not_found) { // nao encontrado
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">As associações selecionadas não foram encontrados...</div>";
	}

    if ($action_ok) { // operacao efetuada
		print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Associação removida com sucesso...</div>";
		print "<script type=\"text/javascript\">";
		print "\trefresh_list();";
		print "</script>";
	}
	
?>
<script type="text/javascript">
	setTimeout(hide_delete_box,3000);
</script>