<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_cbx_modulo = $MYSQL->getFields("cbx_modulo");
	if (is_array($qry_cbx_modulo)) {
		foreach($qry_cbx_modulo as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_modulo"])) {
		$old_modulo = $STRING->str2db($STRING->deCrypt($_POST["old_modulo"]));
	} else {
		$old_modulo = NULL;
	}
	debug("old_modulo - ". $old_modulo);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($modulo)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_cbx_modulo[] = "modulo = \"". $modulo ."\"";
			$sql_cbx_modulo[] = "descricao = \"". $descricao ."\"";
			// une a query
			$sql_cbx_modulo = join(", ",$sql_cbx_modulo);
			debug("POSTA: ". $sql_cbx_modulo);


			// se nao existe registro
			if (!$MYSQL->dbCheck("`cbx_modulo`","lower(modulo) = lower(\"$modulo\")")) {

				// commit
				if ($ID = $MYSQL->queryInsert("`cbx_modulo`",$sql_cbx_modulo)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que o modulo já está cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		//CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...');
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>