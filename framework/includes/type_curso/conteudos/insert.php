<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_cbx_conteudo = $MYSQL->getFields("cbx_conteudo");
	if (is_array($qry_cbx_conteudo)) {
		foreach($qry_cbx_conteudo as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_conteudo"])) {
		$old_conteudo = $STRING->str2db($STRING->deCrypt($_POST["old_conteudo"]));
	} else {
		$old_conteudo = NULL;
	}
	debug("old_conteudo - ". $old_conteudo);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($conteudo) or empty($cargahoraria)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_cbx_conteudo[] = "conteudo = \"". $conteudo ."\"";
			$sql_cbx_conteudo[] = "cargahoraria = \"". $cargahoraria ."\"";
			$sql_cbx_conteudo[] = "descricao = \"". $descricao ."\"";
			// une a query
			$sql_cbx_conteudo = join(", ",$sql_cbx_conteudo);
			debug("POSTA: ". $sql_cbx_conteudo);


			// se nao existe registro
			if (!$MYSQL->dbCheck("`cbx_conteudo`","lower(conteudo) = lower(\"$conteudo\")")) {

				// commit
				if ($ID = $MYSQL->queryInsert("`cbx_conteudo`",$sql_cbx_conteudo)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que o conteudo já está cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		//CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...');
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>