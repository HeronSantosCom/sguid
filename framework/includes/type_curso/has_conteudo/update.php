<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Conteudo não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_cbx_modulo_has_cbx_conteudo = $MYSQL->getFields("cbx_modulo_has_cbx_conteudo");
	if (is_array($qry_cbx_modulo_has_cbx_conteudo)) {
		foreach($qry_cbx_modulo_has_cbx_conteudo as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idcbx_modulo) or empty($idcbx_conteudo)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_cbx_modulo_has_cbx_conteudo[] = "idcbx_modulo = \"". $idcbx_modulo ."\"";
			$sql_cbx_modulo_has_cbx_conteudo[] = "idcbx_conteudo = \"". $idcbx_conteudo ."\"";
			// une a query
			$sql_cbx_modulo_has_cbx_conteudo = join(", ",$sql_cbx_modulo_has_cbx_conteudo);
			debug("POSTA: ". $sql_cbx_modulo_has_cbx_conteudo);

				debug("MUDOU conteudo: ". $conteudo);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`cbx_modulo_has_cbx_conteudo`","idcbx_modulo = $idcbx_modulo and idcbx_conteudo = $idcbx_conteudo")) {

					// commit
					if ($MYSQL->queryUpdate("`cbx_modulo_has_cbx_conteudo`",$sql_cbx_modulo_has_cbx_conteudo,"idcbx_modulo_has_cbx_conteudo = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra que já existe esta associação...');
				}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// cbx_modulo_has_cbx_conteudo
	if ($MYSQL->dbCheck("`cbx_modulo_has_cbx_conteudo`","idcbx_modulo_has_cbx_conteudo = \"$ID\"")) { // verifico se o equipamento existe
		$qry_cbx_modulo_has_cbx_conteudo = $MYSQL->querySelect("cbx_modulo_has_cbx_conteudo",NULL,"idcbx_modulo_has_cbx_conteudo = \"$ID\"");
		foreach ($qry_cbx_modulo_has_cbx_conteudo as $tbl_cbx_modulo_has_cbx_conteudo) {
			foreach($tbl_cbx_modulo_has_cbx_conteudo as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar associação!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Associação efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>