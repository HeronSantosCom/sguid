<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

?>
<?php

		// variaveis da tabela
		$qry_cdr = $MYSQL->getFields("cbx_modulo_has_cbx_conteudo");
		if (is_array($qry_cdr)) {
			foreach($qry_cdr as $field) { //op: lista os campos da tabela
				$$field = NULL;
			}
		}

		$action_error = false;
		$xCount = 1;

	    // declara variaveis
	    if (isset($GET->where)) {
	    	$where = $GET->where;
			$where = explode(";", $where);

			foreach ($where as $aux) {
				//print ($aux . "<br>");
				$where_2 = explode("|", $aux);
				if (!empty($where_2[1])) {
					$field = $where_2[0];
					$$field = $where_2[1];
					debug("$field: " . $$field);
				}
			}
		}

		if (!is_null($idcbx_modulo)) {
			$qry_where[] = "idcbx_modulo = $idcbx_modulo";
		}

        // monta where da query
		if (isset($qry_where)) {
			if (is_array($qry_where)) {
				$qry_where = join(" AND ",$qry_where);
			}
		}

?>
<select id="fifochkid" name="fifochkid" size="5" style="display: none;"></select>
<table align="center" id="list-view" cellpadding="0" cellspacing="0">

	<thead>
		<td style="display: none;"><input type="checkbox" class="checkbox" name="chkall" id="chkall" onclick="toggle_all_checkbox('fifochkid', 'text');"></td>
		<td style="width: 2%; text-align: center;"><a href="javascript:void(0);" onclick="toggle_all_checkbox('fifochkid', 'text', 'chkall');"><img src="<?= getImage("checkall.gif") ?>" align="absmiddle"></a></td>
		<td>Módulo / Conteúdo</td>
	</thead>
	
	<?php

		if(!$action_error) {

			if (isset($GET->page)) {
				if ($GET->page > 0) {
					$page = $GET->page;
				} else {
					$page = 1;
				}
			} else {
				$page = 1;
			}
			debug ("Page: $page");

			if (isset($GET->limit)) {
				$limit = $GET->limit;
			$limit_init = ($page - 1) * $limit;
				$qry_limit = "limit $limit_init, $limit";
			} else {
			$limit = 0;
				$qry_limit = NULL;
			}
			debug ("Limit: $qry_limit");

			$count_cbx_modulo_has_cbx_conteudo = $MYSQL->dbCount("`cbx_modulo_has_cbx_conteudo`",$qry_where);
			$pagination = ceil($count_cbx_modulo_has_cbx_conteudo/$limit);

			$module_anterior = NULL;

			// exibe a listagem
			$qry_cbx_modulo_has_cbx_conteudo = $MYSQL->querySelect("`cbx_modulo_has_cbx_conteudo`","idcbx_modulo_has_cbx_conteudo, idcbx_conteudo, idcbx_modulo",$qry_where,"idcbx_modulo asc, idcbx_conteudo asc",$qry_limit);
			if (is_array($qry_cbx_modulo_has_cbx_conteudo)) {
				foreach($qry_cbx_modulo_has_cbx_conteudo as $tbl_cbx_modulo_has_cbx_conteudo) {

					foreach($tbl_cbx_modulo_has_cbx_conteudo as $field=>$val) { //op: lista os campos da tabela
						$$field = $STRING->db2str($val); //op: atribui valores
						debug("$field - ". $$field);
					} //op

					if (isset($idcbx_modulo)) {
						if ($MYSQL->dbCheck("cbx_modulo", "idcbx_modulo = $idcbx_modulo")) {
							$idcbx_modulo = $STRING->db2str($MYSQL->getValue("cbx_modulo", "modulo" ,"idcbx_modulo = $idcbx_modulo"));
						} else {
							$idcbx_modulo = " - ";
						}
					} else {
						$idcbx_modulo = " - ";
					}

					if (isset($idcbx_conteudo)) {
						if ($MYSQL->dbCheck("cbx_conteudo", "idcbx_conteudo = $idcbx_conteudo")) {
							$idcbx_conteudo = $STRING->db2str($MYSQL->getValue("cbx_conteudo", "conteudo" ,"idcbx_conteudo = $idcbx_conteudo"));
						} else {
							$idcbx_conteudo = " - ";
						}
					} else {
						$idcbx_conteudo = " - ";
					}

					if (isset($GET->return)) { // indica botao de retorno
						$link_update = "ondblclick=\"getOpen('". $STRING->deCrypt($GET->path) . "|INCLUDE/" . $STRING->crypTo("update.php") . "&return=" . $GET->return . "&id=" . $STRING->crypTo($idcbx_modulo_has_cbx_conteudo) ."','container');\"";
					} else {
						$link_update = NULL;
					}

					if ($module_anterior != $idcbx_modulo) {
	?>
	<tbody>
		<td></td>
		<td><b><?= $idcbx_modulo ?></b></td>
	</tbody>
	<?php
						$module_anterior = $idcbx_modulo;
					}
	?>
	<tbody id="tr_<?= $xCount ?>" <?= $link_update ?>>
		<td class="checkbox">
			<input type="checkbox" class="checkbox" name="chk" id="chk_<?= $xCount ?>" value="<?= $STRING->crypTo($idcbx_modulo_has_cbx_conteudo) ?>" onclick="toggle_checkbox('<?= $xCount ?>', 'fifochkid', 'text', this.id);">
		</td>
		<td onclick="toggle_checkbox('<?= $xCount ?>', 'fifochkid', 'text');"><?= $idcbx_conteudo ?></td>
	</tbody>
	<?php
					$xCount++;
				}
			} else {
				$xCount++;
?>
	<tbody>
		<td colspan="2">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Nenhum registro encontrado...</div>
		</td>
	</tbody>
<?php
			}

		} else {
			$xCount++;
?>
	<tbody>
		<td colspan="2">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Erro ao efetuar filtragem...</div>
		</td>
	</tbody>
<?php
		}

		if ($xCount <= 12) {
			for ($yCount = $xCount; $yCount <= 12; $yCount++) {
				echo "<tbody id=\"tr_". $yCount ."\"><td colspan=\"2\">&nbsp;</td></tbody>";
			}
		}
?>
</table>
<script type="text/javascript">
	var page_actualy = parseInt(<?= $page ?>);
	var page_total = parseInt(<?= $pagination ?>);
	var row_total = parseInt(<?= $xCount ?>);

	if (page_total <= 1) {
		document.getElementById('pagination-first').disabled = true;
		document.getElementById('pagination-previous').disabled = true;

		document.getElementById('pagination-page').disabled = true;
		document.getElementById('pagination-go').disabled = true;

		document.getElementById('pagination-next').disabled = true;
		document.getElementById('pagination-last').disabled = true;
	} else {
		if (page_actualy == 1) {
			document.getElementById('pagination-first').disabled = true;
			document.getElementById('pagination-previous').disabled = true;
		} else {
			document.getElementById('pagination-first').disabled = false;
			document.getElementById('pagination-previous').disabled = false;
		}

		document.getElementById('pagination-page').disabled = false;
		document.getElementById('pagination-go').disabled = false;

		if (page_actualy == page_total) {
			document.getElementById('pagination-next').disabled = true;
			document.getElementById('pagination-last').disabled = true;
		} else {
			document.getElementById('pagination-next').disabled = false;
			document.getElementById('pagination-last').disabled = false;
		}
	}

	document.getElementById('page_actualy').value = page_actualy.toString();
	document.getElementById('page_total').value = page_total.toString();
	document.getElementById('row_total').value = row_total.toString();
	document.getElementById('pagination-page-actualy').innerHTML = page_actualy.toString();
	document.getElementById('pagination-page-total').innerHTML = page_total.toString();
</script>