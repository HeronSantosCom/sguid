<?php

	/**
	* @autdor Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

    isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
    isset($GET->idcbx_modulo) ? $idcbx_modulo = $GET->idcbx_modulo : $idcbx_modulo = 0;
    $error = false; // define erro

    $qry_where[] = "idcbx_conteudo in(select idcbx_conteudo from cbx_modulo_has_cbx_conteudo where idcbx_modulo = $idcbx_modulo)";
    $qry_where = join(" AND ",$qry_where);
?>
<table align="center" id="list-view" cellpadding="0" cellspacing="0">
	<thead>
		<td>Conteúdo</td>
		<td style="width: 20%;">Carga Horária</td>
	</thead>

<?php
    $xCount = 1;
    if (isset($qry_where)) {
	//$qry_monitor = $MYSQL->querySelect("monitor","date(calldate) as calldateDate, sum(billsec) as billsecSum, count(calldate) as calldateCount, avg(billsec) as billsecAvg",$qry_where,"","group by calldateDate");
	$qry_monitor = $MYSQL->querySelect("cbx_conteudo","idcbx_conteudo, conteudo, cargahoraria",$qry_where,"conteudo asc");
	if (is_array($qry_monitor)) {
	    foreach($qry_monitor as $tbl_monitor) {

		foreach($tbl_monitor as $field=>$val) { //op: lista os campos da tabela
			$$field = $STRING->db2str($val); //op: atribui valores
		} //op

?>
	<tbody id="tr_<?= $xCount ?>">
		<td><?= $conteudo ?></td>
		<td><?= $cargahoraria ?> h</td>
	</tbody>
<?php
		$xCount++;
	    }
	} else {
	    $xCount++;
?>
	<tbody>
		<td colspan="2">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Nenhum conteúdo foi associado...</div>
		</td>
	</tbody>
<?php
	}
    }

    if ($xCount <= 5) {
	for ($yCount = $xCount; $yCount <= 5; $yCount++) {
	    echo "<tbody id=\"tr_". $yCount ."\"><td colspan=\"2\">&nbsp;</td></tbody>";
	}
    }
?>
</table>