<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/
	// LISTA TODOS OS MODULOS
	$qry_cbx_modulo = $MYSQL->querySelect("cbx_modulo","idcbx_modulo,modulo");
	if (is_array($qry_cbx_modulo)) {
		foreach($qry_cbx_modulo as $tbl_cbx_modulo) {
			$tmp_cbx_modulo["id"] = $tbl_cbx_modulo["idcbx_modulo"];
			$tmp_cbx_modulo["caption"] = $STRING->db2str($tbl_cbx_modulo["modulo"]);
			$cbx_modulo[] = $tmp_cbx_modulo;
		}
	}

	// LISTA TODOS OS CONTEUDOS
	$qry_cbx_conteudo = $MYSQL->querySelect("cbx_conteudo","idcbx_conteudo,conteudo");
	if (is_array($qry_cbx_conteudo)) {
		foreach($qry_cbx_conteudo as $tbl_cbx_conteudo) {
			$tmp_cbx_conteudo["id"] = $tbl_cbx_conteudo["idcbx_conteudo"];
			$tmp_cbx_conteudo["caption"] = $STRING->db2str($tbl_cbx_conteudo["conteudo"]);
			$cbx_conteudo[] = $tmp_cbx_conteudo;
		}
	}

		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);

	$form->Fieldset("basic", "Relacionamento");
	$form->Select("idcbx_modulo", "Módulo", $cbx_modulo, "string", $idcbx_modulo, true, NULL, false, "toggle_modulo(this.value);");
	$form->Select("idcbx_conteudo", "Conteúdo", $cbx_conteudo, "string", $idcbx_conteudo, true);
	$form->EndFieldset();

	$form->Fieldset("modulo_has_conteudo", "Conteúdos Associados");
	$form->Object("modulo"," ");
	$form->EndFieldset();

	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>
<script type="text/javascript">

	function load_form_modulo(idcbx_modulo) {
		//alert("Módulo: " + idcbx_modulo + ")
		ajaxpage("<?= INCLUDE_PATH_PAST ?>modulo.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idcbx_modulo=" + idcbx_modulo, "modulo", false);
	}

	function toggle_modulo(value) {
		load_form_modulo(value);
	}

<?php
	print "\t load_form_modulo(\"".$idcbx_modulo."\"); \n";
?>
</script>