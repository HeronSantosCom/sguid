<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_worktime = $MYSQL->getFields("worktime");
	if (is_array($qry_worktime)) {
		foreach($qry_worktime as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_dayname"])) {
		$old_dayname = $STRING->str2db($STRING->deCrypt($_POST["old_dayname"]));
	} else {
		$old_dayname = NULL;
	}
	debug("old_dayname - ". $old_dayname);

	// resgata dados antigos
	if(isset($_POST["old_idunidade"])) {
		$old_idunidade = $STRING->str2db($STRING->deCrypt($_POST["old_idunidade"]));
	} else {
		$old_idunidade = NULL;
	}
	debug("old_idunidade - ". $old_idunidade);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idunidade) or empty($dayweek) or empty($dayname) or empty($start) or empty($end)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_worktime[] = "idunidade = \"". $idunidade ."\"";
			$sql_worktime[] = "dayweek = \"". $dayweek ."\"";
			$sql_worktime[] = "dayname = \"". $dayname ."\"";
			$sql_worktime[] = "start = \"". $start ."\"";
			$sql_worktime[] = "end = \"". $end ."\"";
			// une a query
			$sql_worktime = join(", ",$sql_worktime);
			debug("POSTA: ". $sql_worktime);


			// se nao existe registro
			if (!$MYSQL->dbCheck("`worktime`","lower(dayname) = lower(\"$dayname\") and idunidade = $idunidade")) {

				// commit
				if ($ID = $MYSQL->queryInsert("`worktime`",$sql_worktime)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra já estar cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar cadastro!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		//CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...');
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>