<!-- Load Mask Form -->
<script type="text/javascript">
//<![CDATA[

	var r = new Restrict("form");

	r.field.start = "\\d:";
	r.mask.start = "##:##:##";

	r.field.end = "\\d:";
	r.mask.end = "##:##:##";

	r.onKeyRefuse = function(o, k){
		o.style.backgroundColor = "#E0E0E0";
	}
	r.onKeyAccept = function(o, k){
		if(k > 30)
			o.style.backgroundColor = "#FFFFFF";
	}
	r.start();

//]]>
</script>