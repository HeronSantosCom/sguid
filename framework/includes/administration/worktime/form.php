<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

    // LISTA TODOS OS SEXOS
    $qry_unidade = $MYSQL->querySelect("`unidade`","idunidade, nome");
    if (is_array($qry_unidade)) {
	    foreach($qry_unidade as $tbl_unidade) {
		    $tmp_unidade["id"] = $STRING->db2str($tbl_unidade["idunidade"]);
		    $tmp_unidade["caption"] = $STRING->db2str($tbl_unidade["nome"]);
		    $unidade[] = $tmp_unidade;
	    }
    } else {
	    $unidade = NULL;
    }
		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);
	$form->Input("hidden","old_dayname",NULL,NULL,$STRING->crypTo($dayname),255);
	$form->Input("hidden","old_idunidade",NULL,NULL,$STRING->crypTo($idunidade),255);

	$form->Fieldset("basic", "Básico");
	$form->Select("idunidade", "Unidade", $unidade, "string", $idunidade, true);
	$form->Select("dayweek", "Dia da Semana", $TYPE_DAYWEEK, "string", $dayweek, true);
	$form->Input("text","dayname","Nome",true,$dayname,255);
	$form->Input("text", "start", "Horário de Início", true, $start);
	$form->Input("text", "end", "Horário de Termino", true, $end);
	$form->EndFieldset();

	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>