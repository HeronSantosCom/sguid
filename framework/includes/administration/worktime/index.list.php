<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

?>
<?php

		// variaveis da tabela
		$qry_worktime = $MYSQL->getFields("worktime");
		if (is_array($qry_worktime)) {
			foreach($qry_worktime as $field) { //op: lista os campos da tabela
				$$field = NULL;
			}
		}

		$action_error = false;
		$xCount = 1;

	    // declara variaveis
	    if (isset($GET->where)) {
	    	$where = $GET->where;
			$where = explode(";", $where);

			foreach ($where as $aux) {
				//print ($aux . "<br>");
				$where_2 = explode("|", $aux);
				if (!empty($where_2[1])) {
					$field = $where_2[0];
					$$field = $where_2[1];
					debug("$field: " . $$field);
				}
			}
		}

		if (!is_null($idunidade)) {
			$qry_where[] = "idunidade = $idunidade";
		}

        // monta where da query
		if (isset($qry_where)) {
			if (is_array($qry_where)) {
				$qry_where = join(" AND ",$qry_where);
			}
		}

?>
<select id="fifochkid" name="fifochkid" size="5" style="display: none;"></select>
<table align="center" id="list-view" cellpadding="0" cellspacing="0">

	<thead>
		<td style="display: none;"><input type="checkbox" class="checkbox" name="chkall" id="chkall" onclick="toggle_all_checkbox('fifochkid', 'text');"></td>
		<td style="width: 2%; text-align: center;"><a href="javascript:void(0);" onclick="toggle_all_checkbox('fifochkid', 'text', 'chkall');"><img src="<?= getImage("checkall.gif") ?>" align="absmiddle"></a></td>
		<td style="width: 30%;">Unidade</td>
		<td style="width: 10%;">Dia da Semana</td>
		<td>Nome</td>
		<td style="width: 10%;">Início</td>
		<td style="width: 10%;">Término</td>
	</thead>
	
	<?php

		if(!$action_error) {

			if (isset($GET->page)) {
				if ($GET->page > 0) {
					$page = $GET->page;
				} else {
					$page = 1;
				}
			} else {
				$page = 1;
			}
			debug ("Page: $page");

			if (isset($GET->limit)) {
				$limit = $GET->limit;
			$limit_init = ($page - 1) * $limit;
				$qry_limit = "limit $limit_init, $limit";
			} else {
			$limit = 0;
				$qry_limit = NULL;
			}
			debug ("Limit: $qry_limit");

			$count_worktime = $MYSQL->dbCount("`worktime`",$qry_where);
			$pagination = ceil($count_worktime/$limit);
			$xCount = 1;

			// exibe a listagem
			$qry_worktime = $MYSQL->querySelect("`worktime`","idworktime, idunidade, dayweek, dayname, start, end",$qry_where,"idunidade asc, dayweek asc",$qry_limit);
			if (is_array($qry_worktime)) {
				foreach($qry_worktime as $tbl_worktime) {

					foreach($tbl_worktime as $field=>$val) { //op: lista os campos da tabela
						$$field = $STRING->db2str($val); //op: atribui valores
						debug("$field - ". $$field);
					} //op

					if (isset($idunidade)) {
						if ($MYSQL->dbCheck("unidade", "idunidade = $idunidade")) {
							$idunidade = $STRING->db2str($MYSQL->getValue("unidade", "nome" ,"idunidade = $idunidade"));
						} else {
							$idunidade = " - ";
						}
					} else {
						$idunidade = " - ";
					}

					foreach($TYPE_DAYWEEK as $type_dayweek) {
						if ($type_dayweek["id"] == $dayweek) {
							$dayweek = $type_dayweek["caption"];
						}
					}

					if (isset($GET->return)) { // indica botao de retorno
						$link_update = "ondblclick=\"getOpen('". $STRING->deCrypt($GET->path) . "|INCLUDE/" . $STRING->crypTo("update.php") . "&return=" . $GET->return . "&id=" . $STRING->crypTo($idworktime) ."','container');\"";
					} else {
						$link_update = NULL;
					}
	?>
	<tbody id="tr_<?= $xCount ?>" <?= $link_update ?>>
		<td class="checkbox">
			<input type="checkbox" class="checkbox" name="chk" id="chk_<?= $xCount ?>" value="<?= $STRING->crypTo($idworktime) ?>" onclick="toggle_checkbox('<?= $xCount ?>', 'fifochkid', 'text', this.id);">
		</td>
		<td onclick="toggle_checkbox('<?= $xCount ?>', 'fifochkid', 'text');"><?= $idunidade ?></td>
		<td><?= $dayweek ?></td>
		<td><?= $dayname ?></td>
		<td><?= $start ?></td>
		<td><?= $end ?></td>
	</tbody>
	<?php
					$xCount++;
				}
			} else {
				$xCount++;
?>
	<tbody>
		<td colspan="6">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Nenhum registro encontrado...</div>
		</td>
	</tbody>
<?php
			}

		} else {
			$xCount++;
?>
	<tbody>
		<td colspan="6">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Erro ao efetuar filtragem...</div>
		</td>
	</tbody>
<?php
		}

		if ($xCount <= 12) {
			for ($yCount = $xCount; $yCount <= 12; $yCount++) {
				echo "<tbody id=\"tr_". $yCount ."\"><td colspan=\"6\">&nbsp;</td></tbody>";
			}
		}
?>
</table>
<script type="text/javascript">
	var page_actualy = parseInt(<?= $page ?>);
	var page_total = parseInt(<?= $pagination ?>);
	var row_total = parseInt(<?= $xCount ?>);

	if (page_total <= 1) {
		document.getElementById('pagination-first').disabled = true;
		document.getElementById('pagination-previous').disabled = true;

		document.getElementById('pagination-page').disabled = true;
		document.getElementById('pagination-go').disabled = true;

		document.getElementById('pagination-next').disabled = true;
		document.getElementById('pagination-last').disabled = true;
	} else {
		if (page_actualy == 1) {
			document.getElementById('pagination-first').disabled = true;
			document.getElementById('pagination-previous').disabled = true;
		} else {
			document.getElementById('pagination-first').disabled = false;
			document.getElementById('pagination-previous').disabled = false;
		}

		document.getElementById('pagination-page').disabled = false;
		document.getElementById('pagination-go').disabled = false;

		if (page_actualy == page_total) {
			document.getElementById('pagination-next').disabled = true;
			document.getElementById('pagination-last').disabled = true;
		} else {
			document.getElementById('pagination-next').disabled = false;
			document.getElementById('pagination-last').disabled = false;
		}
	}

	document.getElementById('page_actualy').value = page_actualy.toString();
	document.getElementById('page_total').value = page_total.toString();
	document.getElementById('row_total').value = row_total.toString();
	document.getElementById('pagination-page-actualy').innerHTML = page_actualy.toString();
	document.getElementById('pagination-page-total').innerHTML = page_total.toString();
</script>