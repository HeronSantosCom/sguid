<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/
	// LISTA TODOS OS NIVEIS DE ACESSO
	if (USER_LEVELACCESS == 1) {
	    $qry_cbx_levelaccess = $MYSQL->querySelect("cbx_levelaccess","idcbx_levelaccess,levelaccess","idcbx_levelaccess");
	} else {
	    $qry_cbx_levelaccess = $MYSQL->querySelect("cbx_levelaccess","idcbx_levelaccess,levelaccess","idcbx_levelaccess > 1");
	}
	if (is_array($qry_cbx_levelaccess)) {
		foreach($qry_cbx_levelaccess as $tbl_cbx_levelaccess) {
			$tmp_cbx_levelaccess["id"] = $tbl_cbx_levelaccess["idcbx_levelaccess"];
			$tmp_cbx_levelaccess["caption"] = $STRING->db2str($tbl_cbx_levelaccess["levelaccess"]);
			$cbx_levelaccess[] = $tmp_cbx_levelaccess;
		}
	}
		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);
	$form->Input("hidden","old_username",NULL,NULL,$STRING->crypTo($username),255);

	$form->Fieldset("basic", "Básico");
	$form->Input("text","name","Nome",true,$name,255);
	$form->Input("text","username","Usuário",true,$username,50);
	$form->Input("password","password","Senha",false,NULL,50);
	$form->Input("password","conf_password","Confirmar Senha",false,NULL,50);
	$form->Select("idcbx_levelaccess", "Nível de Acesso", $cbx_levelaccess, "string", $idcbx_levelaccess, true);
	$form->EndFieldset();
	
	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>