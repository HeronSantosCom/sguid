<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_useraccount = $MYSQL->getFields("useraccount");
	if (is_array($qry_useraccount)) {
		foreach($qry_useraccount as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata o usuario antigo
	if(isset($_POST["old_username"])) {
		$old_username = $STRING->str2db($STRING->deCrypt($_POST["old_username"]));
	} else {
		$old_username = NULL;
	}

	// resgata a confirmacao da senha
	if(isset($_POST["conf_password"])) {
		$conf_password = $STRING->str2db($_POST["conf_password"]);
	} else {
		$conf_password = NULL;
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idcbx_levelaccess) or empty($name) or empty($username)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// verifica se existe ramal associado
		} elseif ($password != $conf_password) {
			CreateDialog("alert","Erro",'Senhas não conferem...');

		// senao
		} else {

			//monta value do query
			$sql_useraccount[] = "idcbx_levelaccess = \"". $idcbx_levelaccess ."\"";
			$sql_useraccount[] = "name = \"". $name ."\"";
			$sql_useraccount[] = "username = \"". $STRING->crypTo($username) ."\"";
			if (!is_null($password) and !is_null($conf_password)) {
				$sql_useraccount[] = "password = \"". $STRING->crypTo($password) ."\"";
			}
			// une a query
			$sql_useraccount = join(", ",$sql_useraccount);
			debug("POSTA: ". $sql_useraccount);

			// se nao existe registro
			if (!$MYSQL->dbCheck("`useraccount`","lower(username) = lower(\"$username\")")) {

				// commit
				if ($ID = $MYSQL->queryInsert("`useraccount`",$sql_useraccount)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra já estar cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar cadastro!\nEntre em contato com o administrador do sistema...',REFERRER);
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>