<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_emprego) ? $idtype_emprego = $GET->idtype_emprego : $idtype_emprego = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);
	
	// LISTA TODOS OS situacaoprofissionalS
	$qry_cbx_situacaoprofissional = $MYSQL->querySelect("cbx_situacaoprofissional","idcbx_situacaoprofissional,situacao");
	if (is_array($qry_cbx_situacaoprofissional)) {
		foreach($qry_cbx_situacaoprofissional as $tbl_cbx_situacaoprofissional) {
			$tmp_cbx_situacaoprofissional["id"] = $tbl_cbx_situacaoprofissional["idcbx_situacaoprofissional"];
			$tmp_cbx_situacaoprofissional["caption"] = $STRING->db2str($tbl_cbx_situacaoprofissional["situacao"]);
			$cbx_situacaoprofissional[] = $tmp_cbx_situacaoprofissional;
		}
	}

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_emprego","idtype_emprego = \"$idtype_emprego\"")) { // verifico se a emprego existe
		$qry_type_emprego = $MYSQL->querySelect("type_emprego",NULL,"idtype_emprego = \"$idtype_emprego\"");
		foreach ($qry_type_emprego as $tbl_type_emprego) {
			foreach($tbl_type_emprego as $field=>$val) { //op: lista os campos da tabela
				$field = "emprego_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	$form->Select("emprego_idcbx_situacaoprofissional", "Situação Profissional", $cbx_situacaoprofissional, "string", $emprego_idcbx_situacaoprofissional, true);
	$form->Input("text","emprego_instituicao","Instituição",false,$emprego_instituicao,255);
	$form->Input("text","emprego_ocupacao","Ocupação",false,$emprego_ocupacao,255);
	$form->Input("text","emprego_remuneracao","Remuneração",false,$emprego_remuneracao,9,false,"onfocus=\"active_mask_emprego_remuneracao();\"");

	$form->Close();
?>