<!-- Load Mask Form -->
<script type="text/javascript">
//<![CDATA[

	var r = new Restrict("form");

	/*r.field.start = "\\d:";
	r.mask.start = "##:##:##";

	r.field.end = "\\d:";
	r.mask.end = "##:##:##";*/

	r.onKeyRefuse = function(o, k){
		o.style.backgroundColor = "#E0E0E0";
	}
	r.onKeyAccept = function(o, k){
		if(k > 30)
			o.style.backgroundColor = "#FFFFFF";
	}
	r.start();

	MaskAjax_Active_Cep = false
	function active_mask_cep() {
		if (MaskAjax_Active_Cep == false) {
			var r = new Restrict("form");

			r.field.endereco_cep = "\\d-";
			r.mask.endereco_cep = "#####-###";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_Cep = true;
		}
	}

	MaskAjax_Active_Identidade = false
	function active_mask_identidade_numero() {
		if (MaskAjax_Active_Identidade == false) {
			var r = new Restrict("form");

			r.field.identidade_numero = "\\d-.";

			r.onKeyRefuse = function(o, k){
				o.style.backgroundColor = "#E0E0E0";
			}
			r.onKeyAccept = function(o, k){
				if(k > 30)
					o.style.backgroundColor = "#FFFFFF";
			}
			r.start();

			MaskAjax_Active_Identidade = true;
		}
	}

	MaskAjax_Active_Emprego_Remuneracao = false
	function active_mask_emprego_remuneracao() {
		if (MaskAjax_Active_Emprego_Remuneracao == false) {
			//var r = new Restrict("form");
			var emprego_remuneracao = document.getElementById("emprego_remuneracao");
			formatCurrency(emprego_remuneracao, 2, "", ".");
			//r.field.emprego_remuneracao = "\\d";

			//r.onKeyRefuse = function(o, k){
			//	o.style.backgroundColor = "#E0E0E0";
			//}
			//r.onKeyAccept = function(o, k){
			//	if(k > 30)
			//		o.style.backgroundColor = "#FFFFFF";
			//}
			//r.start();

			MaskAjax_Active_Emprego_Remuneracao = true;
		}
	}

//]]>
</script>