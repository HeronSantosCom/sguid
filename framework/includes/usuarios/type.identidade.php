<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idtype_identidade) ? $idtype_identidade = $GET->idtype_identidade : $idtype_identidade = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);

	// CARREGA DADOS CADASTRADOS
	if ($MYSQL->dbCheck("type_identidade","idtype_identidade = \"$idtype_identidade\"")) { // verifico se a identidade existe
		$qry_type_identidade = $MYSQL->querySelect("type_identidade",NULL,"idtype_identidade = \"$idtype_identidade\"");
		foreach ($qry_type_identidade as $tbl_type_identidade) {
			foreach($tbl_type_identidade as $field=>$val) { //op: lista os campos da tabela
				$field = "identidade_" . $field;
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	$form->Input("text","identidade_numero","Identidade",true,$identidade_numero,10,false,"onfocus=\"active_mask_identidade_numero();\"");
	$form->Input("text","identidade_orgaoemissor","Orgão Emissor",true,$identidade_orgaoemissor,10);

	$form->Close();
?>