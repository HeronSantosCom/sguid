<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

    // LISTA TODOS OS SEXOS
    $qry_unidade = $MYSQL->querySelect("`unidade`","idunidade, nome");
    if (is_array($qry_unidade)) {
	    foreach($qry_unidade as $tbl_unidade) {
		    $tmp_unidade["id"] = $STRING->db2str($tbl_unidade["idunidade"]);
		    $tmp_unidade["caption"] = $STRING->db2str($tbl_unidade["nome"]);
		    $unidade[] = $tmp_unidade;
	    }
    } else {
	    $unidade = NULL;
    }

    // LISTA TODOS OS SEXOS
    $qry_cbx_sexo = $MYSQL->querySelect("`cbx_sexo`","idcbx_sexo, sexo");
    if (is_array($qry_cbx_sexo)) {
	    foreach($qry_cbx_sexo as $tbl_cbx_sexo) {
		    $tmp_cbx_sexo["id"] = $STRING->db2str($tbl_cbx_sexo["idcbx_sexo"]);
		    $tmp_cbx_sexo["caption"] = $STRING->db2str($tbl_cbx_sexo["sexo"]);
		    $cbx_sexo[] = $tmp_cbx_sexo;
	    }
    } else {
	    $cbx_sexo = NULL;
    }

    // LISTA TODOS OS ESTADO CIVIL
    $qry_cbx_estadocivil = $MYSQL->querySelect("`cbx_estadocivil`","idcbx_estadocivil, estadocivil");
    if (is_array($qry_cbx_estadocivil)) {
	    foreach($qry_cbx_estadocivil as $tbl_cbx_estadocivil) {
		    $tmp_cbx_estadocivil["id"] = $STRING->db2str($tbl_cbx_estadocivil["idcbx_estadocivil"]);
		    $tmp_cbx_estadocivil["caption"] = $STRING->db2str($tbl_cbx_estadocivil["estadocivil"]);
		    $cbx_estadocivil[] = $tmp_cbx_estadocivil;
	    }
    } else {
	    $cbx_estadocivil = NULL;
    }

    // LISTA TODOS OS ETNIA
    $qry_cbx_etnia = $MYSQL->querySelect("`cbx_etnia`","idcbx_etnia, etnia");
    if (is_array($qry_cbx_etnia)) {
	    foreach($qry_cbx_etnia as $tbl_cbx_etnia) {
		    $tmp_cbx_etnia["id"] = $STRING->db2str($tbl_cbx_etnia["idcbx_etnia"]);
		    $tmp_cbx_etnia["caption"] = $STRING->db2str($tbl_cbx_etnia["etnia"]);
		    $cbx_etnia[] = $tmp_cbx_etnia;
	    }
    } else {
	    $cbx_etnia = NULL;
    }
		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);
	$form->Input("hidden","old_nome",NULL,NULL,$STRING->crypTo($nome),255);
	$form->Input("hidden","idtype_nacionalidade",NULL,NULL,$idtype_nacionalidade,255);
	$form->Input("hidden","idtype_endereco",NULL,NULL,$idtype_endereco,255);
	$form->Input("hidden","idtype_identidade",NULL,NULL,$idtype_identidade,255);
	$form->Input("hidden","idtype_escolaridade",NULL,NULL,$idtype_escolaridade,255);
	$form->Input("hidden","idtype_emprego",NULL,NULL,$idtype_emprego,255);

	$form->Fieldset("basic", "Básico");
	$form->Select("idunidade", "Unidade", $unidade, "string", $idunidade, true);
	$form->Input("text","nome","Nome",true,$nome,255);
	$form->Input("date","nascimento","Data de Nascimento",true,$nascimento,10);
	$form->Select("idcbx_sexo", "Sexo", $cbx_sexo, "string", $idcbx_sexo, true);
	$form->Select("idcbx_estadocivil", "Estado Civil", $cbx_estadocivil, "string", $idcbx_estadocivil, true);
	$form->Select("idcbx_etnia", "Etnia Social", $cbx_etnia, "string", $idcbx_etnia, true);

	$form->Object("type_nacionalidade"," ");
	$form->EndFieldset();

	$form->Object("type_endereco"," ");

	$form->Fieldset("documents", "Documentação");
		$form->Input("text","cpf","CPF",true,$cpf,17);

		$form->Fieldset("identidade", "Identidade");
		$form->Object("type_identidade"," ");
		$form->Input("date","identidade_dataemissao","Data Emissão",true,$identidade_dataemissao,10);
		$form->EndFieldset();
	$form->EndFieldset();

	$form->Fieldset("escolaridade", "Escolaridade");
	$form->Object("type_escolaridade"," ");
	$form->EndFieldset();

	$form->Fieldset("emprego", "Situação Profissional");
	$form->Object("type_emprego"," ");
	$form->Input("date","emprego_admissao","Data de Admissao",true,$emprego_admissao,10);
	$form->EndFieldset();

	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>
<script type="text/javascript">

	function load_form_endereco(idtype_endereco) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.endereco.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_endereco=" + idtype_endereco, "type_endereco", false);
	}

	function load_form_nacionalidade(idtype_nacionalidade) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.nacionalidade.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_nacionalidade=" + idtype_nacionalidade, "type_nacionalidade", false);
	}

	function load_form_identidade(idtype_identidade) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.identidade.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_identidade=" + idtype_identidade, "type_identidade", false);
	}

	function load_form_escolaridade(idtype_escolaridade) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.escolaridade.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_escolaridade=" + idtype_escolaridade, "type_escolaridade", false);
	}

	function load_form_emprego(idtype_emprego) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.emprego.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_emprego=" + idtype_emprego, "type_emprego", false);
	}


	function load_form_cidade(idcbx_estado,idcbx_cidade) {
		//alert("Estado: " + idcbx_estado + ", Cidade: " + idcbx_cidade)
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.endereco.cidade.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&endereco_idcbx_cidade=" + idcbx_cidade + "&endereco_idcbx_estado=" + idcbx_estado, "type_cidade", false);
	}

<?php
	print "\t load_form_endereco(\"".$idtype_endereco."\"); \n";
	print "\t load_form_nacionalidade(\"".$idtype_nacionalidade."\"); \n";
	print "\t load_form_identidade(\"".$idtype_identidade."\"); \n";
	print "\t load_form_escolaridade(\"".$idtype_escolaridade."\"); \n";
	print "\t load_form_emprego(\"".$idtype_emprego."\"); \n";
?>
</script>