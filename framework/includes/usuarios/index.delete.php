<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	// RESGATANDO E DECLARANDO VARIAVEIS
    isset($GET->list) ? $list=$GET->list : $list=NULL;
    $action_error = false;
    $not_found = false;
    $action_ok = false;

	// VERIFICA AUSENCIA DO ID
	if (isset($list)) {
		
		$aux_list = explode("|",$list); // desmembra lista de equipamentos
		if (is_array($aux_list)) {
			foreach($aux_list as $aux_id) {
				$ID = $STRING->deCrypt($aux_id);

				// CARREGA DADOS CADASTRADOS
				if ($MYSQL->dbCheck("`usuario`","idusuario = \"$ID\"")) { // verifico se o equipamento existe
					$qry_usuario = $MYSQL->querySelect("`usuario`","idtype_endereco","idusuario = \"$ID\"");
					foreach ($qry_usuario as $tbl_usuario) {
						foreach($tbl_usuario as $field=>$val) { //op: lista os campos da tabela
							$$field = $STRING->db2str($val); //op: atribui valores
						}
					}
				}

				// remove usuario
				if (!$action_error) {
					if ($MYSQL->queryDelete("`usuario`","idusuario = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}
				}

				// remove usuario_has_turma
				if (!$action_error) {
					if ($MYSQL->dbCheck("usuario_has_turma","idusuario = \"$ID\"")) {
						if ($MYSQL->queryDelete("usuario_has_turma","idusuario = \"$ID\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove endereco
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_endereco","idtype_endereco = \"$idtype_endereco\"")) {
						if ($MYSQL->queryDelete("type_endereco","idtype_endereco = \"$idtype_endereco\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove ctps
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_emprego","idtype_emprego = \"$idtype_emprego\"")) {
						if ($MYSQL->queryDelete("type_emprego","idtype_emprego = \"$idtype_emprego\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove escolaridade
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_escolaridade","idtype_escolaridade = \"$idtype_escolaridade\"")) {
						if ($MYSQL->queryDelete("type_escolaridade","idtype_escolaridade = \"$idtype_escolaridade\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove identidade
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_identidade","idtype_identidade = \"$idtype_identidade\"")) {
						if ($MYSQL->queryDelete("type_identidade","idtype_identidade = \"$idtype_identidade\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove nacionalidade
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_nacionalidade","idtype_nacionalidade = \"$idtype_nacionalidade\"")) {
						if ($MYSQL->queryDelete("type_nacionalidade","idtype_nacionalidade = \"$idtype_nacionalidade\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}
			}
		} else {
			$not_found = true;
		}
	} else {
		$not_found = true;
	}
    
	if ($action_error) { // erro na operacao
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Erro ao efetuar operação...</div>";
	}
	
	if ($not_found) { // nao encontrado
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Os usuários selecionadas não foram encontrados...</div>";
	}

    if ($action_ok) { // operacao efetuada
		print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Usuário removido com sucesso...</div>";
		print "<script type=\"text/javascript\">";
		print "\trefresh_list();";
		print "</script>";
	}
	
?>
<script type="text/javascript">
	setTimeout(hide_delete_box,3000);
</script>