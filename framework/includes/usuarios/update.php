<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'usuario não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_usuario = $MYSQL->getFields("usuario");
	if (is_array($qry_usuario)) {
		foreach($qry_usuario as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_endereco = $MYSQL->getFields("type_endereco");
	if (is_array($qry_type_endereco)) {
		foreach($qry_type_endereco as $field) { //op: lista os campos da tabela
			$field = "endereco_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_emprego = $MYSQL->getFields("type_emprego");
	if (is_array($qry_type_emprego)) {
		foreach($qry_type_emprego as $field) { //op: lista os campos da tabela
			$field = "emprego_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_escolaridade = $MYSQL->getFields("type_escolaridade");
	if (is_array($qry_type_escolaridade)) {
		foreach($qry_type_escolaridade as $field) { //op: lista os campos da tabela
			$field = "escolaridade_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_identidade = $MYSQL->getFields("type_identidade");
	if (is_array($qry_type_identidade)) {
		foreach($qry_type_identidade as $field) { //op: lista os campos da tabela
			$field = "identidade_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_nacionalidade = $MYSQL->getFields("type_nacionalidade");
	if (is_array($qry_type_nacionalidade)) {
		foreach($qry_type_nacionalidade as $field) { //op: lista os campos da tabela
			$field = "nacionalidade_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_nome"])) {
		$old_nome = $STRING->str2db($STRING->deCrypt($_POST["old_nome"]));
	} else {
		$old_nome = NULL;
	}
	debug("old_nome - ". $old_nome);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($endereco_idcbx_cidade) or empty($endereco_logradouro) or empty($endereco_bairro) or empty($endereco_cep)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// verifica se a cidade foi especificada
		} elseif (empty($emprego_idcbx_situacaoprofissional)) {
			CreateDialog("alert","Erro",'Falta informações sobre o emprego...');

		// senao
		} elseif (empty($escolaridade_idcbx_grauescolar) or empty($escolaridade_idcbx_serieescolar) or empty($escolaridade_idcbx_tipoescola) or empty($escolaridade_instituicao) or empty($escolaridade_curso) or empty($escolaridade_idcbx_turnoescolar)) {
			CreateDialog("alert","Erro",'Falta informações sobre o escolaridade...');

		// senao
		} elseif (empty($identidade_numero) or empty($identidade_orgaoemissor) or empty($identidade_dataemissao)) {
			CreateDialog("alert","Erro",'Falta informações sobre o identidade......');

		// senao
		} elseif (!empty($nacionalidade_idcbx_nacionalidade) and ($nacionalidade_idcbx_nacionalidade > 1 and empty($nacionalidade_idcbx_federacao))) {
			CreateDialog("alert","Erro",'Falta informações sobre o nacionalidade...');

		// senao
		} elseif (empty($idunidade) or empty($nome) or empty($nascimento) or empty($idcbx_sexo) or empty($idcbx_estadocivil) or empty($idcbx_etnia) or empty($cpf)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_type_endereco[] = "idcbx_cidade = \"". $endereco_idcbx_cidade ."\"";
			$sql_type_endereco[] = "logradouro = \"". $endereco_logradouro ."\"";
			$sql_type_endereco[] = "bairro = \"". $endereco_bairro ."\"";
			$sql_type_endereco[] = "cep = \"". $endereco_cep ."\"";
			$sql_type_endereco = join(", ",$sql_type_endereco); // une a query
			debug("POSTA: ". $sql_type_endereco);
			$MYSQL->queryUpdate("`type_endereco`",$sql_type_endereco,"idtype_endereco = $idtype_endereco"); // commit

			//monta value do query
			$sql_type_emprego[] = "idcbx_situacaoprofissional = \"". $emprego_idcbx_situacaoprofissional ."\"";
			$sql_type_emprego[] = "instituicao = \"". $emprego_instituicao ."\"";
			$sql_type_emprego[] = "admissao = \"". $STRING->toDate($emprego_admissao, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_type_emprego[] = "ocupacao = \"". $emprego_ocupacao ."\"";
			$sql_type_emprego[] = "remuneracao = \"". $emprego_remuneracao ."\"";
			$sql_type_emprego = join(", ",$sql_type_emprego); // une a query
			debug("POSTA: ". $sql_type_emprego);
			$MYSQL->queryUpdate("`type_emprego`",$sql_type_emprego,"idtype_emprego = $idtype_emprego"); // commit

			//monta value do query
			$sql_type_escolaridade[] = "idcbx_grauescolar = \"". $escolaridade_idcbx_grauescolar ."\"";
			$sql_type_escolaridade[] = "idcbx_serieescolar = \"". $escolaridade_idcbx_serieescolar ."\"";
			$sql_type_escolaridade[] = "idcbx_tipoescola = \"". $escolaridade_idcbx_tipoescola ."\"";
			$sql_type_escolaridade[] = "instituicao = \"". $escolaridade_instituicao ."\"";
			$sql_type_escolaridade[] = "curso = \"". $escolaridade_curso ."\"";
			$sql_type_escolaridade[] = "idcbx_turnoescolar = \"". $escolaridade_idcbx_turnoescolar ."\"";
			$sql_type_escolaridade = join(", ",$sql_type_escolaridade); // une a query
			debug("POSTA: ". $sql_type_escolaridade);
			$MYSQL->queryUpdate("`type_escolaridade`",$sql_type_escolaridade,"idtype_escolaridade = $idtype_escolaridade"); // commit

			//monta value do query
			$sql_type_identidade[] = "numero = \"". $identidade_numero ."\"";
			$sql_type_identidade[] = "orgaoemissor = \"". $identidade_orgaoemissor ."\"";
			$sql_type_identidade[] = "dataemissao = \"". $STRING->toDate($identidade_dataemissao, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_type_identidade = join(", ",$sql_type_identidade); // une a query
			debug("POSTA: ". $sql_type_identidade);
			$MYSQL->queryUpdate("`type_identidade`",$sql_type_identidade,"idtype_identidade = $idtype_identidade"); // commit

			//monta value do query
			$sql_type_nacionalidade[] = "idcbx_nacionalidade = \"". $nacionalidade_idcbx_nacionalidade ."\"";
			$sql_type_nacionalidade[] = "idcbx_federacao = \"". ($nacionalidade_idcbx_nacionalidade>1?$nacionalidade_idcbx_federacao:FEDERACAO) ."\"";
			$sql_type_nacionalidade = join(", ",$sql_type_nacionalidade); // une a query
			debug("POSTA: ". $sql_type_nacionalidade);
			$MYSQL->queryUpdate("`type_nacionalidade`",$sql_type_nacionalidade,"idtype_nacionalidade = $idtype_nacionalidade"); // commit

			//monta value do query
			$sql_usuario[] = "idunidade = \"". $idunidade ."\"";
			$sql_usuario[] = "nome = \"". $nome ."\"";
			$sql_usuario[] = "nascimento = \"". $STRING->toDate($nascimento, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_usuario[] = "idcbx_sexo = \"". $idcbx_sexo ."\"";
			$sql_usuario[] = "idcbx_estadocivil = \"". $idcbx_estadocivil ."\"";
			$sql_usuario[] = "idcbx_etnia = \"". $idcbx_etnia ."\"";
			$sql_usuario[] = "idtype_nacionalidade = \"". $idtype_nacionalidade ."\"";
			$sql_usuario[] = "idtype_endereco = \"". $idtype_endereco ."\"";
			$sql_usuario[] = "idtype_identidade = \"". $idtype_identidade ."\"";
			$sql_usuario[] = "cpf = \"". $cpf ."\"";
			$sql_usuario[] = "idtype_emprego = \"". $idtype_emprego ."\"";
			$sql_usuario[] = "idtype_escolaridade = \"". $idtype_escolaridade ."\"";
			// une a query
			$sql_usuario = join(", ",$sql_usuario);
			debug("POSTA: ". $sql_usuario);

			// se nao tiver alterado e nao tiver erro
			if ($nome == $old_nome and !$action_error) {
				debug("CONTINUA nome: ". $old_nome);
				// commit
				if ($MYSQL->queryUpdate("`usuario`",$sql_usuario,"idusuario = \"$ID\"")) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// se tiver alterado e nao tiver erro
			} elseif (!$action_error) {
				debug("MUDOU nome: ". $nome);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`usuario`","lower(nome) = lower(\"$nome\")")) {

					// commit
					if ($MYSQL->queryUpdate("`usuario`",$sql_usuario,"idusuario = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra que o usuário já está cadastrado...');
				}
			}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// usuario
	if ($MYSQL->dbCheck("`usuario`","idusuario = \"$ID\"")) { // verifico se o equipamento existe
		$qry_usuario = $MYSQL->querySelect("usuario",NULL,"idusuario = \"$ID\"");
		foreach ($qry_usuario as $tbl_usuario) {
			foreach($tbl_usuario as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}

	if (isset($nascimento)) {
		$nascimento = $STRING->toDate($nascimento, "YYYY-MM-DD", "DD/MM/YYYY");
	}

	if (isset($idtype_emprego)) {
		if ($MYSQL->dbCheck("type_emprego", "idtype_emprego = $idtype_emprego")) {
			$emprego_admissao = $STRING->toDate($MYSQL->getValue("type_emprego", "admissao", "idtype_emprego = $idtype_emprego"), "YYYY-MM-DD", "DD/MM/YYYY");
		}
	}

	if (isset($idtype_identidade)) {
		if ($MYSQL->dbCheck("type_identidade", "idtype_identidade = $idtype_identidade")) {
			$identidade_dataemissao = $STRING->toDate($MYSQL->getValue("type_identidade", "dataemissao", "idtype_identidade = $idtype_identidade"), "YYYY-MM-DD", "DD/MM/YYYY");
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Alteração efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>