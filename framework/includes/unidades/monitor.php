<?php

	/**
	* @autdor Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

    isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
    isset($GET->idunidade) ? $idunidade = $GET->idunidade : $idunidade = 0;
    $error = false; // define erro

    $qry_where[] = "idmonitor in(select idmonitor from unidade_has_monitor where idunidade = $idunidade)";
    $qry_where = join(" AND ",$qry_where);
?>
<table align="center" id="list-view" cellpadding="0" cellspacing="0">
	<thead>
        <td>Nome</td>
        <td style="width: 10%;">Nascimento</td>
        <td style="width: 10%;">Admissão</td>
	</thead>

<?php
    $xCount = 1;
    if (isset($qry_where)) {
	//$qry_monitor = $MYSQL->querySelect("monitor","date(calldate) as calldateDate, sum(billsec) as billsecSum, count(calldate) as calldateCount, avg(billsec) as billsecAvg",$qry_where,"","group by calldateDate");
	$qry_monitor = $MYSQL->querySelect("monitor","idmonitor, nome, nascimento, admissao",$qry_where,"nome asc");
	if (is_array($qry_monitor)) {
	    foreach($qry_monitor as $tbl_monitor) {

		foreach($tbl_monitor as $field=>$val) { //op: lista os campos da tabela
			$$field = $STRING->db2str($val); //op: atribui valores
		} //op
		if (isset($nascimento)) {
		    $nascimento = $STRING->toDate($nascimento, "YYYY-MM-DD", "DD/MM/YYYY");
		}
		if (isset($admissao)) {
		    $admissao = $STRING->toDate($admissao, "YYYY-MM-DD", "DD/MM/YYYY");
		}

?>
	<tbody id="tr_<?= $xCount ?>">
        <td><?= $nome ?></td>
        <td><?= $nascimento ?></td>
        <td><?= $admissao ?></td>
	</tbody>
<?php
		$xCount++;
	    }
	} else {
	    $xCount++;
?>
	<tbody>
		<td colspan="4">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Nenhum monitor foi relatado...</div>
		</td>
	</tbody>
<?php
	}
    }

    if ($xCount <= 5) {
	for ($yCount = $xCount; $yCount <= 5; $yCount++) {
	    echo "<tbody id=\"tr_". $yCount ."\"><td colspan=\"4\">&nbsp;</td></tbody>";
	}
    }
?>
</table>