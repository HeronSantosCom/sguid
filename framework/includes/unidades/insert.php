<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_unidade = $MYSQL->getFields("unidade");
	if (is_array($qry_unidade)) {
		foreach($qry_unidade as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_endereco = $MYSQL->getFields("type_endereco");
	if (is_array($qry_type_endereco)) {
		foreach($qry_type_endereco as $field) { //op: lista os campos da tabela
			$field = "endereco_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_nome"])) {
		$old_nome = $STRING->str2db($STRING->deCrypt($_POST["old_nome"]));
	} else {
		$old_nome = NULL;
	}
	debug("old_nome - ". $old_nome);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($nome)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// verifica se a cidade foi especificada
		} elseif (empty($endereco_idcbx_cidade)) {
			CreateDialog("alert","Erro",'Cidade não foi especificada...');

		// senao
		} else {

			//monta value do query
			$sql_type_endereco[] = "idcbx_cidade = \"". $endereco_idcbx_cidade ."\"";
			$sql_type_endereco[] = "logradouro = \"". $endereco_logradouro ."\"";
			$sql_type_endereco[] = "bairro = \"". $endereco_bairro ."\"";
			$sql_type_endereco[] = "cep = \"". $endereco_cep ."\"";
			// une a query
			$sql_type_endereco = join(", ",$sql_type_endereco);
			debug("POSTA: ". $sql_type_endereco);

			// commit
			$idtype_endereco = $MYSQL->queryInsert("`type_endereco`",$sql_type_endereco);

			//monta value do query
			$sql_unidade[] = "idinstituicao = \"". $idinstituicao ."\"";
			$sql_unidade[] = "nome = \"". $nome ."\"";
			$sql_unidade[] = "idtype_endereco = \"". $idtype_endereco ."\"";
			// une a query
			$sql_unidade = join(", ",$sql_unidade);
			debug("POSTA: ". $sql_unidade);


			// se nao existe registro
			if (!$MYSQL->dbCheck("`unidade`","lower(nome) = lower(\"$nome\")")) {

				// commit
				if ($ID = $MYSQL->queryInsert("`unidade`",$sql_unidade)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que a unidade já está cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		//CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...');
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>