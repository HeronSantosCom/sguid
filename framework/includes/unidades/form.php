<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

    // LISTA TODOS AS CIDADES
    $qry_instituicao = $MYSQL->querySelect("instituicao","idinstituicao,nomefantasia");
    if (is_array($qry_instituicao)) {
	    foreach($qry_instituicao as $tbl_instituicao) {
		    $tmp_instituicao["id"] = $tbl_instituicao["idinstituicao"];
		    $tmp_instituicao["caption"] = $STRING->db2str($tbl_instituicao["nomefantasia"]);
		    $instituicao[] = $tmp_instituicao;
	    }
    }
		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);
	$form->Input("hidden","old_nome",NULL,NULL,$STRING->crypTo($nome),255);
	$form->Input("hidden","idtype_endereco",NULL,NULL,$idtype_endereco,255);

	$form->Fieldset("basic", "Básico");
	$form->Select("idinstituicao", "Instituição", $instituicao, "string", $idinstituicao, true, NULL, false, NULL);
	
	$form->Input("text","nome","Nome",true,$nome,255);
	$form->EndFieldset();

	$form->Object("type_endereco"," ");
	$form->Object("sala"," ");
	
	$form->Fieldset("unidade_has_monitor", "Monitores Associados");
	$form->Object("monitor"," ");
	$form->EndFieldset();
	
	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>
<script type="text/javascript">

	function load_form_endereco(idtype_endereco) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.endereco.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_endereco=" + idtype_endereco, "type_endereco", false);
	}

	function load_form_cidade(idcbx_estado,idcbx_cidade) {
		//alert("Estado: " + idcbx_estado + ", Cidade: " + idcbx_cidade)
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.endereco.cidade.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&endereco_idcbx_cidade=" + idcbx_cidade + "&endereco_idcbx_estado=" + idcbx_estado, "type_cidade", false);
	}

	function load_form_sala(idunidade,idsala) {
		//alert("Unidade: " + idunidade + ", Sala: " + idsala)
		ajaxpage("<?= INCLUDE_PATH_PAST ?>sala.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idunidade=" + idunidade + "&idsala=" + idsala, "sala", false);
	}

	function load_form_monitor(idunidade) {
		//alert("Unidade: " + idunidade + ")
		ajaxpage("<?= INCLUDE_PATH_PAST ?>monitor.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idunidade=" + idunidade, "monitor", false);
	}

<?php
	print "\t load_form_endereco(\"".$idtype_endereco."\"); \n";
	print "\t load_form_sala(\"".$idunidade."\"); \n";
	print "\t load_form_monitor(\"".$idunidade."\"); \n";
?>
</script>