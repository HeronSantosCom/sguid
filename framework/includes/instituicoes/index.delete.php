<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	// RESGATANDO E DECLARANDO VARIAVEIS
    isset($GET->list) ? $list=$GET->list : $list=NULL;
    $action_error = false;
    $not_found = false;
    $action_ok = false;

	// VERIFICA AUSENCIA DO ID
	if (isset($list)) {
		
		$aux_list = explode("|",$list); // desmembra lista de equipamentos
		if (is_array($aux_list)) {
			foreach($aux_list as $aux_id) {
				$ID = $STRING->deCrypt($aux_id);

				// CARREGA DADOS CADASTRADOS
				if ($MYSQL->dbCheck("`instituicao`","idinstituicao = \"$ID\"")) { // verifico se o equipamento existe
					$qry_instituicao = $MYSQL->querySelect("`instituicao`","idtype_endereco","idinstituicao = \"$ID\"");
					foreach ($qry_instituicao as $tbl_instituicao) {
						foreach($tbl_instituicao as $field=>$val) { //op: lista os campos da tabela
							$$field = $STRING->db2str($val); //op: atribui valores
						}
					}
				}

				// remove instituicao
				if (!$action_error) {
					if ($MYSQL->queryDelete("`instituicao`","idinstituicao = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}
				}

				// remove endereco
				if (!$action_error) {
					if ($MYSQL->dbCheck("type_endereco","idtype_endereco = \"$idtype_endereco\"")) {
						if ($MYSQL->queryDelete("type_endereco","idtype_endereco = \"$idtype_endereco\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}

				// remove relacionamento com unidade
				if (!$action_error) {
					if ($MYSQL->dbCheck("unidade","idinstituicao = \"$ID\"")) {
						if ($MYSQL->queryUpdate("unidade","idinstituicao = NULL","idinstituicao = \"$ID\"")) {
							$action_ok = true;
						} else {
							$action_error = true;
						}
					}
				}
			}
		} else {
			$not_found = true;
		}
	} else {
		$not_found = true;
	}
    
	if ($action_error) { // erro na operacao
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Erro ao efetuar operação...</div>";
	}
	
	if ($not_found) { // nao encontrado
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">As instituições selecionadas não foram encontrados...</div>";
	}

    if ($action_ok) { // operacao efetuada
		print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Instituição removida com sucesso...</div>";
		print "<script type=\"text/javascript\">";
		print "\trefresh_list();";
		print "</script>";
	}
	
?>
<script type="text/javascript">
	setTimeout(hide_delete_box,3000);
</script>