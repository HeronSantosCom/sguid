<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/
		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);
	$form->Input("hidden","old_nomefantasia",NULL,NULL,$STRING->crypTo($nomefantasia),255);
	$form->Input("hidden","idtype_endereco",NULL,NULL,$idtype_endereco,255);

	$form->Fieldset("basic", "Básico");
	$form->Input("text","nomefantasia","Nome Fantasia",true,$nomefantasia,100);
	$form->Input("text","razaosocial","Razão Social",false,$razaosocial,155);
	$form->Input("text","CNPJ","CNPJ",false,$CNPJ,17);
	$form->EndFieldset();

	$form->Object("type_endereco"," ");
	
	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>
<script type="text/javascript">

	function load_form_endereco(idtype_endereco) {
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.endereco.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idtype_endereco=" + idtype_endereco, "type_endereco", false);
	}

	function load_form_cidade(idcbx_estado,idcbx_cidade) {
		//alert("Estado: " + idcbx_estado + ", Cidade: " + idcbx_cidade)
		ajaxpage("<?= INCLUDE_PATH_PAST ?>type.endereco.cidade.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&endereco_idcbx_cidade=" + idcbx_cidade + "&endereco_idcbx_estado=" + idcbx_estado, "type_cidade", false);
	}

<?php
	print "\t load_form_endereco(\"".$idtype_endereco."\"); \n";
?>
</script>