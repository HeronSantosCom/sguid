<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->endereco_idcbx_cidade) ? $endereco_idcbx_cidade = $GET->endereco_idcbx_cidade : $endereco_idcbx_cidade = 0;
	isset($GET->endereco_idcbx_estado) ? $endereco_idcbx_estado = $GET->endereco_idcbx_estado : $endereco_idcbx_estado = 0;
	$error = false; // define erro

	// instancio novo formulatio
	$form = new Form(false);
	if ($endereco_idcbx_estado != 0) {

	    // LISTA TODOS AS CIDADES
	    $qry_cbx_cidade = $MYSQL->querySelect("cbx_cidade","idcbx_cidade,cidade","idcbx_estado = $endereco_idcbx_estado");
	    if (is_array($qry_cbx_cidade)) {
		    foreach($qry_cbx_cidade as $tbl_cbx_cidade) {
			    $tmp_cbx_cidade["id"] = $tbl_cbx_cidade["idcbx_cidade"];
			    $tmp_cbx_cidade["caption"] = $STRING->db2str($tbl_cbx_cidade["cidade"]);
			    $cbx_cidade[] = $tmp_cbx_cidade;
		    }
	    }

	    $form->Select("endereco_idcbx_cidade", "Cidade", $cbx_cidade, "string", $endereco_idcbx_cidade, true, NULL, false, NULL);

	} else {
	    $form->Select("endereco_idcbx_cidade", "Cidade", $cbx_cidade, "string", $endereco_idcbx_cidade, true, NULL, false, NULL, true);
	}
	$form->Close();
?>