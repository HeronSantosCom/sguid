<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Item não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_instituicao = $MYSQL->getFields("instituicao");
	if (is_array($qry_instituicao)) {
		foreach($qry_instituicao as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// variaveis da tabela
	$qry_type_endereco = $MYSQL->getFields("type_endereco");
	if (is_array($qry_type_endereco)) {
		foreach($qry_type_endereco as $field) { //op: lista os campos da tabela
			$field = "endereco_" . $field;
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

	// resgata dados antigos
	if(isset($_POST["old_nomefantasia"])) {
		$old_nomefantasia = $STRING->str2db($STRING->deCrypt($_POST["old_nomefantasia"]));
	} else {
		$old_nomefantasia = NULL;
	}
	debug("old_nomefantasia - ". $old_nomefantasia);

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($nomefantasia) or (!empty($CNPJ) and empty($razaosocial))) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// verifica se a cidade foi especificada
		} elseif (empty($endereco_idcbx_cidade)) {
			CreateDialog("alert","Erro",'Cidade não foi especificada...');

		// senao
		} else {

			//monta value do query
			$sql_type_endereco[] = "idcbx_cidade = \"". $endereco_idcbx_cidade ."\"";
			$sql_type_endereco[] = "logradouro = \"". $endereco_logradouro ."\"";
			$sql_type_endereco[] = "bairro = \"". $endereco_bairro ."\"";
			$sql_type_endereco[] = "cep = \"". $endereco_cep ."\"";
			// une a query
			$sql_type_endereco = join(", ",$sql_type_endereco);
			debug("POSTA: ". $sql_type_endereco);

			// commit
			$MYSQL->queryUpdate("`type_endereco`",$sql_type_endereco,"idtype_endereco = $idtype_endereco");

			//monta value do query
			$sql_instituicao[] = "nomefantasia = \"". $nomefantasia ."\"";
			$sql_instituicao[] = "razaosocial = \"". $razaosocial ."\"";
			$sql_instituicao[] = "CNPJ = \"". $CNPJ ."\"";
			$sql_instituicao[] = "idtype_endereco = \"". $idtype_endereco ."\"";
			// une a query
			$sql_instituicao = join(", ",$sql_instituicao);
			debug("POSTA: ". $sql_instituicao);

			// se nao tiver alterado e nao tiver erro
			if ($nomefantasia == $old_nomefantasia and !$action_error) {
				debug("CONTINUA nomefantasia: ". $old_nomefantasia);
				// commit
				if ($MYSQL->queryUpdate("`instituicao`",$sql_instituicao,"idinstituicao = \"$ID\"")) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// se tiver alterado e nao tiver erro
			} elseif (!$action_error) {
				debug("MUDOU nomefantasia: ". $nomefantasia);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`instituicao`","lower(nomefantasia) = lower(\"$nomefantasia\")")) {

					// commit
					if ($MYSQL->queryUpdate("`instituicao`",$sql_instituicao,"idinstituicao = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra já estar cadastrado...');
				}
			}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// instituicao
	if ($MYSQL->dbCheck("`instituicao`","idinstituicao = \"$ID\"")) { // verifico se o equipamento existe
		$qry_instituicao = $MYSQL->querySelect("instituicao",NULL,"idinstituicao = \"$ID\"");
		foreach ($qry_instituicao as $tbl_instituicao) {
			foreach($tbl_instituicao as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Alteração efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>