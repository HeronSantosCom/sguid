<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("insert.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_turma = $MYSQL->getFields("turma");
	if (is_array($qry_turma)) {
		foreach($qry_turma as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idunidade) or empty($idtype_curso) or empty($inicio)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {
			$idturma = $MYSQL->getValue("`turma`", "count(idturma)", "idunidade = $idunidade and YEAR(inicio) = YEAR(\"". $STRING->toDate($inicio, "DD/MM/YYYY", "YYYY-MM-DD") ."\")") + 1;
			$codigo = str_pad($idunidade, 3, '0', STR_PAD_LEFT) . "." . str_pad($idturma, 5, '0', STR_PAD_LEFT) . "/" . date("y");
			debug("CODIGO: ". $codigo);

			//monta value do query
			$sql_turma[] = "codigo = \"". $codigo ."\"";
			$sql_turma[] = "idunidade = \"". $idunidade ."\"";
			$sql_turma[] = "idtype_curso = \"". $idtype_curso ."\"";
			$sql_turma[] = "inicio = \"". $STRING->toDate($inicio, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_turma[] = "termino = \"". $STRING->toDate($inicio, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			// une a query
			$sql_turma = join(", ",$sql_turma);
			debug("POSTA: ". $sql_turma);


			// se nao existe registro
			if (!$MYSQL->dbCheck("`turma`","idunidade = $idunidade and idtype_curso = $idtype_curso and inicio = " . $STRING->toDate($inicio, "DD/MM/YYYY", "YYYY-MM-DD"))) {

				// commit
				if ($ID = $MYSQL->queryInsert("`turma`",$sql_turma)) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que a turma já está cadastrado...');
			}
		}
	}

?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...',REFERRER);
		//CreateDialog("alert","Concluído",'Cadastro efetuado com sucesso...');
		exit;
	}

?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>