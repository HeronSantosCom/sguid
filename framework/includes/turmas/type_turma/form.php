<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

    // LISTA TODOS OS UNIDADE
    $qry_unidade = $MYSQL->querySelect("`unidade`","idunidade, nome");
    if (is_array($qry_unidade)) {
	    foreach($qry_unidade as $tbl_unidade) {
		    $tmp_unidade["id"] = $STRING->db2str($tbl_unidade["idunidade"]);
		    $tmp_unidade["caption"] = $STRING->db2str($tbl_unidade["nome"]);
		    $unidade[] = $tmp_unidade;
	    }
    } else {
	    $unidade = NULL;
    }

	// LISTA TODOS OS CURSOS
	$qry_type_curso = $MYSQL->querySelect("type_curso","idtype_curso,curso");
	if (is_array($qry_type_curso)) {
		foreach($qry_type_curso as $tbl_type_curso) {
			$tmp_type_curso["id"] = $tbl_type_curso["idtype_curso"];
			$tmp_type_curso["caption"] = $STRING->db2str($tbl_type_curso["curso"]);
			$type_curso[] = $tmp_type_curso;
		}
	}

		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);

	$form->Fieldset("basic", "Básico");
	$form->Select("idunidade", "Unidade", $unidade, "string", $idunidade, true);
	$form->Select("idtype_curso", "Curso", $type_curso, "string", $idtype_curso, true);
	$form->Input("date", "inicio", "Data de Início", true, $inicio);
	//$form->Input("date", "termino", "Data de Termino", true, $termino);
	$form->EndFieldset();

	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>