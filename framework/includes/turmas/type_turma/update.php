<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Turma não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_turma = $MYSQL->getFields("turma");
	if (is_array($qry_turma)) {
		foreach($qry_turma as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idunidade) or empty($idtype_curso) or empty($inicio)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_turma[] = "idunidade = \"". $idunidade ."\"";
			$sql_turma[] = "idtype_curso = \"". $idtype_curso ."\"";
			$sql_turma[] = "inicio = \"". $STRING->toDate($inicio, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			$sql_turma[] = "termino = \"". $STRING->toDate($termino, "DD/MM/YYYY", "YYYY-MM-DD") ."\"";
			// une a query
			$sql_turma = join(", ",$sql_turma);
			debug("POSTA: ". $sql_turma);

			// se nao existe registro
			if (!$MYSQL->dbCheck("`turma`","idunidade = $idunidade and idtype_curso = $idtype_curso and inicio = " . $STRING->toDate($inicio, "DD/MM/YYYY", "YYYY-MM-DD"))) {

				// commit
				if ($MYSQL->queryUpdate("`turma`",$sql_turma,"idturma = \"$ID\"")) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que a turma já está cadastrada...');
			}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// turma
	if ($MYSQL->dbCheck("`turma`","idturma = \"$ID\"")) { // verifico se o equipamento existe
		$qry_turma = $MYSQL->querySelect("turma",NULL,"idturma = \"$ID\"");
		foreach ($qry_turma as $tbl_turma) {
			foreach($tbl_turma as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}

		if (isset($inicio)) {
			$inicio = $STRING->toDate($inicio, "YYYY-MM-DD", "DD/MM/YYYY");
		}
		if (isset($termino)) {
			$termino = $STRING->toDate($termino, "YYYY-MM-DD", "DD/MM/YYYY");
		}

	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Alteração efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>