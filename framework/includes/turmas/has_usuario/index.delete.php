<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

	// RESGATANDO E DECLARANDO VARIAVEIS
    isset($GET->list) ? $list=$GET->list : $list=NULL;
    $action_error = false;
    $not_found = false;
    $action_ok = false;

	// VERIFICA AUSENCIA DO ID
	if (isset($list)) {
		
		$aux_list = explode("|",$list); // desmembra lista de equipamentos
		if (is_array($aux_list)) {
			foreach($aux_list as $aux_id) {
				$ID = $STRING->deCrypt($aux_id);

				// CARREGA DADOS CADASTRADOS
				/*if ($MYSQL->dbCheck("`turma`","idturma = \"$ID\"")) { // verifico se o equipamento existe
					$qry_turma = $MYSQL->querySelect("`turma`","idturma","idturma = \"$ID\"");
					foreach ($qry_turma as $tbl_turma) {
						foreach($tbl_turma as $field=>$val) { //op: lista os campos da tabela
							$$field = $STRING->db2str($val); //op: atribui valores
						}
					}
				})*/

				// remove usuario_has_turma
				if (!$action_error) {
					if ($MYSQL->queryDelete("`usuario_has_turma`","idusuario_has_turma = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}
				}
			}
		} else {
			$not_found = true;
		}
	} else {
		$not_found = true;
	}
    
	if ($action_error) { // erro na operacao
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Erro ao efetuar operação...</div>";
	}
	
	if ($not_found) { // nao encontrado
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">As associações selecionadas não foram encontrados...</div>";
	}

    if ($action_ok) { // operacao efetuada
		print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Associação removida com sucesso...</div>";
		print "<script type=\"text/javascript\">";
		print "\trefresh_list();";
		print "</script>";
	}
	
?>
<script type="text/javascript">
	setTimeout(hide_delete_box,3000);
</script>