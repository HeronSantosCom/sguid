<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Associação não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_usuario_has_turma = $MYSQL->getFields("usuario_has_turma");
	if (is_array($qry_usuario_has_turma)) {
		foreach($qry_usuario_has_turma as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		// verifica campos nulos
		if (empty($idusuario) or empty($idturma) or empty($inicio) or empty($termino)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} else {

			//monta value do query
			$sql_usuario_has_turma[] = "idusuario = \"". $idusuario ."\"";
			$sql_usuario_has_turma[] = "idturma = \"". $idturma ."\"";
			// une a query
			$sql_usuario_has_turma = join(", ",$sql_usuario_has_turma);
			debug("POSTA: ". $sql_usuario_has_turma);

				debug("MUDOU conteudo: ". $conteudo);
				// se nao existe registro
				if (!$MYSQL->dbCheck("`usuario_has_turma`","idusuario = $idusuario and idturma = $idturma")) {

					// commit
					if ($MYSQL->queryUpdate("`usuario_has_turma`",$sql_usuario_has_turma,"idusuario_has_turma = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}

				// existe registro
				} else {
					CreateDialog("alert","Atenção",'As especificações demostra que já existe esta associação...');
					//exit;
				}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// usuario_has_turma
	if ($MYSQL->dbCheck("`usuario_has_turma`","idusuario_has_turma = \"$ID\"")) { // verifico se o equipamento existe
		$qry_usuario_has_turma = $MYSQL->querySelect("usuario_has_turma",NULL,"idusuario_has_turma = \"$ID\"");
		foreach ($qry_usuario_has_turma as $tbl_usuario_has_turma) {
			foreach($tbl_usuario_has_turma as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar associação!\nEntre em contato com o administrador do sistema...',REFERRER);
		//CreateDialog("alert","Erro",'Impossível efetuar alteração!\nEntre em contato com o administrador do sistema...');
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Associação efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>