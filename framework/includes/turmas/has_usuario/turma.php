<?php

	/**
	* @autdor Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

    isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
    isset($GET->idturma) ? $idturma = $GET->idturma : $idturma = 0;
    $error = false; // define erro

	$qry_where[] = "idturma = $idturma";
    $qry_where = join(" AND ",$qry_where);
?>
<table align="center" id="list-view" cellpadding="0" cellspacing="0">
	<thead>
		<td>Horário de Expediente</td>
		<td style="width: 20%;">Horário de Início</td>
		<td style="width: 20%;">Horário de Término</td>
	</thead>

<?php
    $xCount = 1;
    if (isset($qry_where)) {
	//$qry_monitor = $MYSQL->querySelect("monitor","date(calldate) as calldateDate, sum(billsec) as billsecSum, count(calldate) as calldateCount, avg(billsec) as billsecAvg",$qry_where,"","group by calldateDate");
	$qry_monitor = $MYSQL->querySelect("usuario_has_turma","idusuario", $qry_where);
	if (is_array($qry_monitor)) {
	    foreach($qry_monitor as $tbl_monitor) {

		foreach($tbl_monitor as $field=>$val) { //op: lista os campos da tabela
			$$field = $STRING->db2str($val); //op: atribui valores
		} //op

		if (isset($idusuario)) {
			if ($MYSQL->dbCheck("usuario", "idusuario = $idusuario")) {
				$nome = $STRING->db2str($MYSQL->getValue("usuario", "nome" ,"idusuario = $idusuario"));
				$nascimento = $STRING->db2str($MYSQL->getValue("usuario", "nascimento" ,"idusuario = $idusuario"));
				$inscricao = $STRING->db2str($MYSQL->getValue("usuario", "inscricao" ,"idusuario = $idusuario"));
				
				if (isset($nascimento)) {
					$nascimento = $STRING->toDate($nascimento, "YYYY-MM-DD", "DD/MM/YYYY");
				}
				if (isset($inscricao)) {
					$inscricao = $STRING->toDate($inscricao, "YYYY-MM-DD", "DD/MM/YYYY");
				}

			} else {
				$nome = " - ";
				$nascimento = " - ";
				$inscricao = " - ";
			}
		} else {
			$nome = " - ";
			$nascimento = " - ";
			$inscricao = " - ";
		}

?>
	<tbody id="tr_<?= $xCount ?>">
		<td><?= $nome ?></td>
		<td><?= $nascimento ?></td>
		<td><?= $inscricao ?></td>
	</tbody>
<?php
		$xCount++;
	    }
	} else {
	    $xCount++;
?>
	<tbody>
		<td colspan="3">
			<div id="boxmessage"><img src="<?= getImage("alert.png") ?>" align="absmiddle">Nenhum usuário foi associado...</div>
		</td>
	</tbody>
<?php
	}
    }

    if ($xCount <= 5) {
	for ($yCount = $xCount; $yCount <= 5; $yCount++) {
	    echo "<tbody id=\"tr_". $yCount ."\"><td colspan=\"3\">&nbsp;</td></tbody>";
	}
    }
?>
</table>