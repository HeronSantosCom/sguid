<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idturma) ? $idturma = $GET->idturma : $idturma = 0;
	isset($GET->idusuario) ? $idusuario = $GET->idusuario : $idusuario = 0;
	$error = false; // define erro

	if ($MYSQL->dbCheck("turma", "idturma = $idturma")) {
		$idunidade = $MYSQL->getValue("turma", "idunidade", "idturma = $idturma");
	} else {
		$idturma = 0;
	}

	// instancio novo formulatio
	$form = new Form(false);
	if ($idturma != 0) {

	    // LISTA TODOS OS usuario
	    $qry_usuario = $MYSQL->querySelect("usuario","idusuario, nome, nascimento, inscricao","idunidade = $idunidade","inscricao asc, nome asc");
	    if (is_array($qry_usuario)) {
		    foreach($qry_usuario as $tbl_usuario) {
			    $tmp_usuario["id"] = $tbl_usuario["idusuario"];
			    $tmp_usuario["caption"] = $STRING->toDate($STRING->db2str($tbl_usuario["inscricao"]), "YYYY-MM-DD", "DD/MM/YYYY") . " - " . $STRING->db2str($tbl_usuario["nome"]) . " (" . $STRING->toDate($STRING->db2str($tbl_usuario["nascimento"]), "YYYY-MM-DD", "DD/MM/YYYY") . ")";
			    $usuario[] = $tmp_usuario;
		    }
	    }
		
	    $form->Select("idusuario", "Usuário", $usuario, "string", $idusuario, true, NULL, false, NULL);

	} else {
	    $form->Select("idusuario", "Usuário", $usuario, "string", $idusuario, true, NULL, false, NULL, true);
	}
	$form->Close();
?>