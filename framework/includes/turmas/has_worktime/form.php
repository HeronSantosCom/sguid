<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

	// LISTA TODOS AS TURMAS
	$qry_turma = $MYSQL->querySelect("turma","idturma, codigo, idunidade, idtype_curso");
	if (is_array($qry_turma)) {
		foreach($qry_turma as $tbl_turma) {
			$unidade = $STRING->db2str($MYSQL->getValue("unidade", "nome", "idunidade = " . $tbl_turma["idunidade"]));
			$type_curso = $STRING->db2str($MYSQL->getValue("type_curso", "curso", "idtype_curso = " . $tbl_turma["idtype_curso"]));
			$caption = $STRING->db2str($tbl_turma["codigo"]) . " - " . $unidade . " - " . $type_curso;
			$tmp_turma["id"] = $tbl_turma["idturma"];
			$tmp_turma["caption"] = $caption;
			$turma[] = $tmp_turma;
		}
	}

		
?>

<?php

/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(true,NULL,"POST"," id=\"form\" ");
	$form->Input("hidden","action",NULL,NULL,md5("submit"),255);

	$form->Fieldset("basic", "Relacionamento");
	$form->Select("idturma", "Turma", $turma, "string", $idturma, true, NULL, false, "toggle_turma(this.value);");
	$form->Object("worktime"," ");
	$form->Input("text", "inicio", "Horário de Início", true, $inicio);
	$form->Input("text", "termino", "Horário de Termino", true, $termino);
	$form->EndFieldset();

	$form->Fieldset("turma_has_worktime", "Horários Associados");
	$form->Object("turma"," ");
	$form->EndFieldset();

	$form->Object("boxmessage", "<img src=\"".getImage("help.png")."\" align=\"absmiddle\">Os campos marcados com * são obrigatórios...");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Ok", "submit", NULL, false, true, "title=\"Ok\"");

	$form->OpenInclude("form.mask.php");
	$form->Close();

?>
<script type="text/javascript">

	function load_form_turma(idturma) {
		//alert("Turma: " + idturma)
		ajaxpage("<?= INCLUDE_PATH_PAST ?>turma.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idturma=" + idturma, "turma", false);
	}

	function toggle_turma(value) {
		load_form_turma(value);
		load_form_worktime(value);
	}

<?php
	print "\t load_form_turma(\"".$idturma."\"); \n";
?>

	function load_form_worktime(idturma,idworktime) {
		//alert("Turma: " + idturma + " Worktime: " + idworktime)
		ajaxpage("<?= INCLUDE_PATH_PAST ?>worktime.php?include=<?= $STRING->crypTo(INCLUDE_PATH_PAST) ?>&idturma=" + idturma + "&idworktime=" + idworktime, "worktime", false);
	}

<?php
	print "\t load_form_worktime(\"".$idturma."\",\"".$idworktime."\"); \n";
?>
</script>