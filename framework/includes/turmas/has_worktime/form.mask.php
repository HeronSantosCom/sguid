<!-- Load Mask Form -->
<script type="text/javascript">
//<![CDATA[

	var r = new Restrict("form");

	r.field.inicio = "\\d:";
	r.mask.inicio = "##:##:##";

	r.field.termino = "\\d:";
	r.mask.termino = "##:##:##";

	r.onKeyRefuse = function(o, k){
		o.style.backgroundColor = "#E0E0E0";
	}
	r.onKeyAccept = function(o, k){
		if(k > 30)
			o.style.backgroundColor = "#FFFFFF";
	}
	r.start();

//]]>
</script>