<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("update.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

?>

<?php

/**********************************************************
 * VERIFICA AUSENCIA DO ID
 **/
	if (!isset($ID)) {
		CreateDialog("alert","Erro",'Associação não encontrado!',REFERRER);
		exit;
	}
	
?>

<?php

/**********************************************************
 * VARIAVEIS
 **/
	// variaveis padrao
	isset($POST->action) ? $action = $POST->action : $action=NULL;
	$action_error = false;
	$action_ok = false;
	$error_forward = false;

	// variaveis da tabela
	$qry_worktime_has_turma = $MYSQL->getFields("worktime_has_turma");
	if (is_array($qry_worktime_has_turma)) {
		foreach($qry_worktime_has_turma as $field) { //op: lista os campos da tabela
			isset($POST->$field) ? $$field = $POST->$field : $$field = NULL;
			debug("$field - ". $$field);
		}
	}

?>

<?php

/**********************************************************
 * SUBMIT
 **/
	if ($action == md5("submit")) {

		if (!empty($idworktime)) {
			if ($MYSQL->dbCheck("worktime", "idworktime = $idworktime")) {
				$start = $MYSQL->getValue("worktime", "start", "idworktime = $idworktime");
				$end = $MYSQL->getValue("worktime", "end", "idworktime = $idworktime");
			} else {
				$start = NULL;
				$end = NULL;
			}
		}

		debug("start - ". $start);
		debug("end - ". $end);

		// verifica campos nulos
		if (empty($idworktime) or empty($idturma) or empty($inicio) or empty($termino)) {
			CreateDialog("alert","Erro",'Há campos obrigatórios que não foram preenchidos...\nVerifique os campos sinalizados com * ao lado...');

		// senao
		} elseif (($inicio < $start or $inicio > $end) or ($termino > $end or $termino < $start)) {
			CreateDialog("alert","Erro",'Horário especificado fora do expediente...');

		// senao
		} elseif (($inicio > $termino and $termino < $inicio)) {
			CreateDialog("alert","Erro",'Horário invertido...');

		// senao
		} else {

			//monta value do query
			$sql_worktime_has_turma[] = "idworktime = \"". $idworktime ."\"";
			$sql_worktime_has_turma[] = "idturma = \"". $idturma ."\"";
			$sql_worktime_has_turma[] = "inicio = \"". $inicio ."\"";
			$sql_worktime_has_turma[] = "termino = \"". $termino ."\"";
			// une a query
			$sql_worktime_has_turma = join(", ",$sql_worktime_has_turma);
			debug("POSTA: ". $sql_worktime_has_turma);

			// se nao existe registro
			if (!$MYSQL->dbCheck("`worktime_has_turma`","idworktime = $idworktime and idturma = $idturma and inicio = \"$inicio\" and termino = \"$termino\"")) {

				// commit
				if ($MYSQL->queryUpdate("`worktime_has_turma`",$sql_worktime_has_turma,"idworktime_has_turma = \"$ID\"")) {
					$action_ok = true;
				} else {
					$action_error = true;
				}

				if ($action_ok) {

					$cargahoraria = $MYSQL->getValue("cbx_conteudo", "SUM(cargahoraria)", "
									idcbx_conteudo in(select idcbx_conteudo from cbx_modulo_has_cbx_conteudo where
									idcbx_modulo in(select idcbx_modulo from cbx_modulo_has_type_curso where
									idtype_curso in(select idtype_curso from turma where idturma = $idturma)))");
					if ($cargahoraria < 1) {
						$cargahoraria = 0;
					}
					debug("carga horaria inicial: $cargahoraria");

					$day_inicio = $MYSQL->getValue("turma", "inicio", "idturma = $idturma");
					debug("data inicial: $day_inicio");

					if (isset($day_inicio)) {
						$day_inicio = $STRING->toDate($day_inicio, "YYYY-MM-DD", "DD/MM/YYYY");
					}
					debug("data convetida: $day_inicio");

					debug("----------------------");
					$cDia = 0;
					while ($cargahoraria > 0) {
						$day_termino = $STRING->sumDate($day_inicio, $cDia);
						$dayweek_atual = $STRING->getDayWeek($day_termino);

						$qry_monitor = $MYSQL->querySelect("worktime_has_turma","idworktime, inicio, termino", "idturma = $idturma");
						if (is_array($qry_monitor)) {
							foreach($qry_monitor as $tbl_monitor) {
								if ($MYSQL->dbCheck("worktime", "idworktime = " . $tbl_monitor["idworktime"])) {
									$dayweek = $MYSQL->getValue("worktime", "dayweek", "idworktime = " . $tbl_monitor["idworktime"]);
								} else {
									$dayweek = 0;
								}

								if ($dayweek == $dayweek_atual) {
									$nDiff = strtotime($tbl_monitor["termino"]) - strtotime($tbl_monitor["inicio"]);
									$nHour = round($nDiff / 3600, 1);
									$cargahoraria -= $nHour;

									debug("data executada: $dayweek_atual - $day_termino = $cargahoraria ($nHour)");
								}
							}
						} else {
							$day_termino = $STRING->toDate($day_inicio, "YYYY-MM-DD", "DD/MM/YYYY");
							$cargahoraria = 0;
						}

						$cDia++;
					}
					debug("----------------------");
					debug("data termino: $day_termino");
					if (isset($day_termino)) {
						$day_termino = $STRING->toDate($day_termino, "DD/MM/YYYY", "YYYY-MM-DD");
					}
					debug("data convetida: $day_termino");
					$MYSQL->queryUpdate("turma", "termino = \"$day_termino\"", "idturma = $idturma");
				}

			// existe registro
			} else {
				CreateDialog("alert","Atenção",'As especificações demostra que já existe esta associação...');
				//exit;
			}
		}
	}

?>

<?php

/**********************************************************
 * CARREGA DADOS CADASTRADOS
 **/
	// worktime_has_turma
	if ($MYSQL->dbCheck("`worktime_has_turma`","idworktime_has_turma = \"$ID\"")) { // verifico se o equipamento existe
		$qry_worktime_has_turma = $MYSQL->querySelect("worktime_has_turma",NULL,"idworktime_has_turma = \"$ID\"");
		foreach ($qry_worktime_has_turma as $tbl_worktime_has_turma) {
			foreach($tbl_worktime_has_turma as $field=>$val) { //op: lista os campos da tabela
				$$field = $STRING->db2str($val); //op: atribui valores
			}
		}
	}
	
?>

<?

/**********************************************************
 * STATUS
 **/
	if ($action_error) { // erro na operacao
		CreateDialog("alert","Erro",'Impossível efetuar associação!\nEntre em contato com o administrador do sistema...',REFERRER);
		exit;
	}
    if ($action_ok) { // operacao efetuada
		CreateDialog("alert","Concluído",'Associação efetuado com sucesso...',REFERRER);
		exit;
	}
	
?>

<?

/**********************************************************
 * FORMULARIO
 **/
	include(IMPORT_PATH_PAST . "form.php");

?>