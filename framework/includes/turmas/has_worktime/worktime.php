<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

	isset($GET->include) ? $include=$STRING->deCrypt($GET->include) : $include = NULL;
	isset($GET->idturma) ? $idturma = $GET->idturma : $idturma = 0;
	isset($GET->idworktime) ? $idworktime = $GET->idworktime : $idworktime = 0;
	$error = false; // define erro

	if ($MYSQL->dbCheck("turma", "idturma = $idturma")) {
		$idunidade = $MYSQL->getValue("turma", "idunidade", "idturma = $idturma");
	} else {
		$idturma = 0;
	}

	// instancio novo formulatio
	$form = new Form(false);
	if ($idturma != 0) {

	    // LISTA TODOS OS WORKTIME
	    $qry_worktime = $MYSQL->querySelect("worktime","idworktime, dayname, start, end","idunidade = $idunidade");
	    if (is_array($qry_worktime)) {
		    foreach($qry_worktime as $tbl_worktime) {
			    $tmp_worktime["id"] = $tbl_worktime["idworktime"];
			    $tmp_worktime["caption"] = $STRING->db2str($tbl_worktime["dayname"]) . " (" . $STRING->db2str($tbl_worktime["start"]) . " - " . $STRING->db2str($tbl_worktime["end"]) . ")";
			    $worktime[] = $tmp_worktime;
		    }
	    }

	    $form->Select("idworktime", "Horário de Expediente", $worktime, "string", $idworktime, true, NULL, false, NULL);

	} else {
	    $form->Select("idworktime", "Horário de Expediente", $worktime, "string", $idworktime, true, NULL, false, NULL, true);
	}
	$form->Close();
?>