<?php

	//op: bloqueador de acesso externo
	$url_check = $_SERVER["PHP_SELF"];
	if (eregi("index.php", "$url_check")) {
		header ("Location: /index.php");
	}

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

    $link_list = INCLUDE_PATH_PAST . "index.list.php?path=". $STRING->crypTo(SYSTEM_PATH_CONTAINER) ."&return=". LOADED_CONTAINER;
	$link_delete = INCLUDE_PATH_PAST . "index.delete.php?path=". $STRING->crypTo(SYSTEM_PATH_CONTAINER) ."&action=". md5("delete");
	$link_insert = SYSTEM_PATH_CONTAINER . "|INCLUDE/" . $STRING->crypTo("insert.php") . "&return=" . LOADED_CONTAINER;
	$link_update = SYSTEM_PATH_CONTAINER . "|INCLUDE/" . $STRING->crypTo("update.php") . "&return=" . LOADED_CONTAINER;

?>
<script type="text/javascript">
	// declarando variavel query
	var qry_where;

	// Carrega a lista
	function open_list(page, where, limit, orderby, orderby_order) {
		refresh_list();

		if (page == null || page == "")
			var page = 1;

		if (limit == null)
			var limit = 30;

		if (where != null)
			var qry_where = "&where=" + where;
		else
			var qry_where = "";

		if (orderby != null) {
			var qry_orderby = "&order=" + orderby;
			if (orderby_order != null) {
				qry_orderby = qry_orderby + "&by=" + orderby_order;
			}
		} else
			var qry_orderby = "";


		ajaxpage("<?= $link_list ?>" + qry_where + "&page=" + page + "&limit=" + limit, "ajaxbox");

		document.getElementById('pagination-page').value = page;
	}

	// Seleciona a pagina
	function set_page() {
		total_page = parseInt(document.getElementById('page_total').value);
		field_page = parseInt(document.getElementById('pagination-page').value);

		if (field_page > 0 && field_page <= total_page) {
			open_list(field_page);
		} else {
			alert("Página Inexistente");
		}
	}

	// Atualiza Lista
	function refresh_list() {
		var chk_elements_name = document.getElementsByName('chk');
		uncheckedAllChk(chk_elements_name, 'fifochkid');
	}

	function resultset_first() {
		open_list(1);
	}

	function resultset_previous() {
		var page_actualy = parseInt(document.getElementById('page_actualy').value);
		previous_page = page_actualy - 1;
		open_list(previous_page);
	}

	function resultset_next() {
		var page_actualy = parseInt(document.getElementById('page_actualy').value);
		next_page = page_actualy + 1;
		open_list(next_page);
	}

	function resultset_last() {
		var last_page = parseInt(document.getElementById('page_total').value);
		open_list(last_page);
	}

	function page_refresh() {
		var page_actualy = parseInt(document.getElementById('page_actualy').value);
		open_list(page_actualy);
	}

	//Filtro da Paginacao
	function page_filter() {
		var idturma = document.getElementById('idturma').value;

		if (idturma != "")
			idturma = ";idturma|" + idturma;
		else
			idturma = "";

		 qry_where = "0" + idturma;

		open_list(1, qry_where);
	}

	// Remove da Lista
	function delete_list() {
		document.getElementById('deletebox').style.display = "block";

		// declara lista e lista tamanho
		var list_obj = document.getElementById("fifochkid");
		var list_len = list_obj.length;

		if (list_len > 0) { // se conter item na LIST

			agree = confirm("Remover\nDeseja remover os itens selecionados?");
			if (agree) {

				var iCount = 0;
				var list_aux = null;

				while (iCount < list_len) { // analisa os itens na LIST

					list_value = list_obj.options[iCount].text;

					if (list_aux == null)
						list_aux = list_value;
					else
						list_aux = list_aux + "|" + list_value;

					iCount++; // incrementa contador
				}

				ajaxpage("<?= $link_delete ?>&list=" + list_aux, "deletebox");

			} else
				return false;

		} else { // se nao conter item na LIST
			alert("Nenhum item foi selecionado!"); // acrescentar na lista
		}
	}

	// Atualiza Item
	function update_item() {
		// declara lista e lista tamanho
		var list_obj = document.getElementById("fifochkid");
		var list_len = list_obj.length;

		if (list_len > 0) { // se conter item na LIST

			if (list_len < 2) {

				var iCount = 0;
				var list_aux = null;

				while (iCount < list_len) { // analisa os itens na LIST
					list_value = list_obj.options[iCount].text;

					if (list_aux == null)
						list_aux = list_value;
					else
						list_aux = list_aux + "|" + list_value;

					iCount++; // incrementa contador
				}

				getOpen("<?= $link_update ?>&id=" + list_aux,"container");

			} else
				alert("Atenção\nSó é possível editar um item por vez...");

		} else
			alert("Nenhum item foi selecionado!");
	}


	// Oculta Status do Delete
	function hide_delete_box() {
		ajaxpage("", "deletebox");
		document.getElementById('deletebox').style.display = "none";

		var row_total = parseInt(document.getElementById('row_total').value);
		var page_actualy = parseInt(document.getElementById('page_actualy').value);

		if (row_total == 1) {
			if (page_actualy > 1)
				var next_page = page_actualy - 1;
			else
				var next_page = 1;
		} else
			var next_page = page_actualy;

		open_list(next_page);
	}
</script>
<?php

/**********************************************************
 * DEVOLVE VALORES AO FORM
 **/

	// LISTA TODOS AS TURMAS
	$qry_turma = $MYSQL->querySelect("turma","idturma, codigo, idunidade, idtype_curso");
	if (is_array($qry_turma)) {
		foreach($qry_turma as $tbl_turma) {
			$unidade = $STRING->db2str($MYSQL->getValue("unidade", "nome", "idunidade = " . $tbl_turma["idunidade"]));
			$type_curso = $STRING->db2str($MYSQL->getValue("type_curso", "curso", "idtype_curso = " . $tbl_turma["idtype_curso"]));
			$caption = $STRING->db2str($tbl_turma["codigo"]) . " - " . $unidade . " - " . $type_curso;
			$tmp_turma["id"] = $tbl_turma["idturma"];
			$tmp_turma["caption"] = $caption;
			$turma[] = $tmp_turma;
		}
	}


/**********************************************************
 * IMPRIME FORMULARIO
 **/
	$form = new Form(false,NULL,"POST");
	$form->Null("<form id=\"form\" action=\"javascript:void(0);\">");
	$form->Fieldset("basic", "Básico");

	$form->Select("idturma", "Turma", $turma, "string", $idturma, false, NULL, false, NULL, false, "tabindex=\"1\"");

	$form->Div("button", "style=\"float: right;\"");
	$form->Button("confirm", "<img src=\"".getImage("accept.png")."\" align=\"absmiddle\"> Filtrar", "submit", "page_filter()", false, false, "title=\"Filtrar Relatório\"");
	$form->EndDiv();

	$form->EndFieldset();
	$form->Null("</form>");

	$form->Close();

?>
<br clear="all">

<input type="hidden" name="page_actualy" id="page_actualy" value="">
<input type="hidden" name="page_total" id="page_total" value="">
<input type="hidden" name="row_total" id="row_total" value="">

<div id="deletebox"></div>

<table style="float: right;">
	<tr>
		<td>Página&nbsp;</td>
		<td><div id="pagination-page-actualy"></div></td>
		<td>&nbsp;de&nbsp;</td>
		<td><div id="pagination-page-total"></div></td>
		<td>&nbsp;&nbsp;&nbsp;</td>
		<td><button title="Atualizar Tabela" type="button" id="page_refresh" onclick="page_refresh()"><img src="<?= getImage("arrow_refresh.png") ?>" align="absmiddle"></button></td>
	</tr>
</table>

<table id="pagination">
	<tr>
		<td><button title="Primeira" type="button" id="pagination-first" onclick="resultset_first()" disabled><img src="<?= getImage("resultset_first.png") ?>" align="absmiddle"></button></td>
		<td><button title="Anterior" type="button" id="pagination-previous" onclick="resultset_previous()" disabled><img src="<?= getImage("resultset_previous.png") ?>" align="absmiddle"></button></td>
		<td><button title="Próxima" type="button" id="pagination-next" onclick="resultset_next()" disabled><img src="<?= getImage("resultset_next.png") ?>" align="absmiddle"></button></td>
		<td><button title="Última" type="button" id="pagination-last" onclick="resultset_last()" disabled><img src="<?= getImage("resultset_last.png") ?>" align="absmiddle"></button></td>
		<form action="javascript:void(0);">
			<td>&nbsp;</td>
			<td><input type="text" name="pagination-page" id="pagination-page" disabled></td>
			<td>&nbsp;</td>
			<td><button type="submit" title="Ir para página..." id="pagination-go" onclick="set_page();" disabled><img src="<?= getImage("page_go.png") ?>" alt="Ir" align="absmiddle"> Ir</button></td>
			<td>&nbsp;</td>
		</form>
	</tr>
</table>

<div id="ajaxbox"></div>

<table>
	<tr>
		<td><button title="Inserir..." type="button" id="pagination-insert" onclick="getOpen('<?= $link_insert ?>', 'container');"><img src="<?= getImage("insert.png") ?>" alt="Adicionar" align="absmiddle"> Adicionar</button></td>
		<td><button title="Alterar..." type="button" id="pagination-update" onclick="update_item();"><img src="<?= getImage("update.png") ?>" alt="Alterar" align="absmiddle"> Alterar</button></td>
		<td><button title="Remover Itens..." id="pagination-delete" onclick="delete_list();"><img src="<?= getImage("delete.png") ?>" alt="Deletar" align="absmiddle"> Deletar</button></td>
		<td>&nbsp;&nbsp;&nbsp;</td>
	</tr>
</table>

<script type="text/javascript">
	open_list();
</script>