<?php

	/**
	* @author Heron Santos
	* @copyright 2008
	*/

	// carrega configuracoes
    include("../../../../core/configurations.php");

	// RESGATANDO E DECLARANDO VARIAVEIS
    isset($GET->list) ? $list=$GET->list : $list=NULL;
    $action_error = false;
    $not_found = false;
    $action_ok = false;

	// VERIFICA AUSENCIA DO ID
	if (isset($list)) {
		
		$aux_list = explode("|",$list); // desmembra lista de equipamentos
		if (is_array($aux_list)) {
			foreach($aux_list as $aux_id) {
				$ID = $STRING->deCrypt($aux_id);

				// CARREGA DADOS CADASTRADOS
				if ($MYSQL->dbCheck("`worktime_has_turma`","idworktime_has_turma = \"$ID\"")) { // verifico se o equipamento existe
					$qry_turma = $MYSQL->querySelect("`worktime_has_turma`","idturma","idworktime_has_turma = \"$ID\"");
					foreach ($qry_turma as $tbl_turma) {
						foreach($tbl_turma as $field=>$val) { //op: lista os campos da tabela
							$$field = $STRING->db2str($val); //op: atribui valores
						}
					}
				}

				// remove worktime_has_turma
				if (!$action_error) {
					if ($MYSQL->queryDelete("`worktime_has_turma`","idworktime_has_turma = \"$ID\"")) {
						$action_ok = true;
					} else {
						$action_error = true;
					}
				}

				// atualiza grade de horarios
				if (!$action_error and $idturma) {

					$cargahoraria = $MYSQL->getValue("cbx_conteudo", "SUM(cargahoraria)", "
									idcbx_conteudo in(select idcbx_conteudo from cbx_modulo_has_cbx_conteudo where
									idcbx_modulo in(select idcbx_modulo from cbx_modulo_has_type_curso where
									idtype_curso in(select idtype_curso from turma where idturma = $idturma)))");
					if ($cargahoraria < 1) {
						$cargahoraria = 0;
					}
					debug("carga horaria inicial: $cargahoraria");

					$day_inicio = $MYSQL->getValue("turma", "inicio", "idturma = $idturma");
					debug("data inicial: $day_inicio");

					if (isset($day_inicio)) {
						$day_inicio = $STRING->toDate($day_inicio, "YYYY-MM-DD", "DD/MM/YYYY");
					}
					debug("data convetida: $day_inicio");

					debug("----------------------");
					$cDia = 0;
					while ($cargahoraria > 0) {
						$day_termino = $STRING->sumDate($day_inicio, $cDia);
						$dayweek_atual = $STRING->getDayWeek($day_termino);

						$qry_monitor = $MYSQL->querySelect("worktime_has_turma","idworktime, inicio, termino", "idturma = $idturma");
						if (is_array($qry_monitor)) {
							foreach($qry_monitor as $tbl_monitor) {
								if ($MYSQL->dbCheck("worktime", "idworktime = " . $tbl_monitor["idworktime"])) {
									$dayweek = $MYSQL->getValue("worktime", "dayweek", "idworktime = " . $tbl_monitor["idworktime"]);
								} else {
									$dayweek = 0;
								}

								if ($dayweek == $dayweek_atual) {
									$nDiff = strtotime($tbl_monitor["termino"]) - strtotime($tbl_monitor["inicio"]);
									$nHour = round($nDiff / 3600, 1);
									$cargahoraria -= $nHour;

									debug("data executada: $dayweek_atual - $day_termino = $cargahoraria ($nHour)");
								}
							}
						} else {
							$day_termino = $STRING->toDate($day_inicio, "YYYY-MM-DD", "DD/MM/YYYY");
							$cargahoraria = 0;
						}

						$cDia++;
					}
					debug("----------------------");
					debug("data termino: $day_termino");
					if (isset($day_termino)) {
						$day_termino = $STRING->toDate($day_termino, "DD/MM/YYYY", "YYYY-MM-DD");
					}
					debug("data convetida: $day_termino");
					$MYSQL->queryUpdate("turma", "termino = \"$day_termino\"", "idturma = $idturma");
				}

			}
		} else {
			$not_found = true;
		}
	} else {
		$not_found = true;
	}
    
	if ($action_error) { // erro na operacao
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">Erro ao efetuar operação...</div>";
	}
	
	if ($not_found) { // nao encontrado
		print "<div id=\"boxmessage\"><img src=\"".getImage("error.png")."\" align=\"absmiddle\">As associações selecionadas não foram encontrados...</div>";
	}

    if ($action_ok) { // operacao efetuada
		print "<div id=\"boxmessage\"><img src=\"".getImage("accept.png")."\" align=\"absmiddle\">Associação removida com sucesso...</div>";
		print "<script type=\"text/javascript\">";
		print "\trefresh_list();";
		print "</script>";
	}
	
?>
<script type="text/javascript">
	//setTimeout(hide_delete_box,3000);
</script>